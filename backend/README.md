# 同路

## 一站式旅行规划系统后端部分

这里是同路旅游聚合系统的后端部分，如需了解请阅读对应的章节~

## 开发组件介绍 :small_airplane:

- 使用 `Spring Boot` 完成基本 Web 程序搭建
- 使用 `Spring Data JPA` 完成持久层编写
- 使用 `Spring Security` 完成账号系统，安全保护等
- 使用 `MapStruct` 进行不同实体对象间的自动转换
- 使用 `Swagger 3` 进行 `Restful API` 的交互式文档记录
- 使用 `Jackson` 完成对象与请求体的转换
- 使用 `auth0` 进行 `JWT` 验证

简而言之就是 Spring 全家桶 ，但是这可不是乱选的啊  :joy: ，在试用了 `MyBatis` `Shiro` 等框架之后，发现 Spring 集成组件甚至在 IDE 层面都有全面的支持，开发非常方便，一句 XML 或 SQL
都不用写 :kissing_closed_eyes: 。

## 开发进度 :zap:

### 准备工作

- [x] 学习 `Spring` 全家桶基本使用
- [x] 搭建基本框架
- [x] 设计组件级数据库模式
- [x] 设计系统级数据库模式
- [x] 完成实体对象的构建
- [x] 完成 `JPA` 关系映射配置
- [x] 完善 `Repository` 持久层查询方法

### 系统实现

- [x] 设计账户系统
- [x] 学习 `Spring Security` 与安全验证相关文档
- [x] 自定义实现 `Spring Security` 完成 `Jwt` 验证
- [x] 完成登录、注册、登出的 `Controller`
- [x] 单元测试账户系统
- [x] 修正测试 `Bug`
- [ ] 与前端联调账户系统
- [ ] 修正联调 `Bug`

### 未来工作

- [ ] 设计 `Docker`虚拟化部署方案
- [ ] 建立 `CI / CD` 工作流
- [ ] 设计并实现商家与景点商品子系统
- [ ] 设计并实现日历行程子系统
- [ ] 设计并实现地理天气子系统
- [ ] 设计并实现论坛交流子系统
- [ ] 设计并实现智能推荐子系统
- [ ] ······

## 部署方法 :satellite:

### 数据库

首先确保数据库环境，请安装 `MySQL 8.0.22` 及以上版本，然后在 `root `用户执行以下操作：

```mysql
# 创建应用专用用户
create user 'tourist'@'localhost' identified by 'your password';

# 创建专用数据库
create database tourist;

# 授权在该数据库上的所有权限
grant all privileges on tourist.* to 'tourist'@'localhost';

# 授予脚本创建触发器的权限 （每次服务器重新启动可能均需配置）
set global log_bin_trust_function_creators = 1;
```

### 构建与运行

构建与运行需要 `gradle` 环境，仓库内已经上传了嵌入式的 `gradle-wrapper` 与 `gradlew` 。

使用以下命令进行构建

```bash
gradle build 
```

使用以下命令执行所有单元测试（目前还不能确保全部执行的正确性）

```bash
gradle test 
```

使用以下命令运行

```bash
# 普通测试
bootRun
# 带有错误堆栈输出
bootRun --stacktrace
```

使用 Docker 的虚拟化部署方法目前还在开发中。

## 开发事宜 :computer:

### 环境与配置事项

- 使用`Idea Ultimate` 版本进行开发
- 安装 `JPA Buddy`扩展以加快持久层编码
- 运行时可能需要传入专用账户的密码
- 更改 `src/main/resource/application.yaml` 中的键值以按需更改配置
- 请勿随意改动 `build.gradle` 脚本中的依赖配置

### `API` 访问文档查看

此外可以在部署完成后访问 `localhost:8080/api-test` 来查看项目提供的所有 `API` 规范。





