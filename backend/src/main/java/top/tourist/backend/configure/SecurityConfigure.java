package top.tourist.backend.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.header.Header;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import top.tourist.backend.configure.security.JwtSignInConfigure;
import top.tourist.backend.configure.security.JwtVerifyConfigure;
import top.tourist.backend.configure.security.OptionsConfigure;
import top.tourist.backend.service.auth.handler.JwtSuccessHandler;
import top.tourist.backend.service.auth.handler.SessionClearLogoutHandler;
import top.tourist.backend.service.auth.provider.JwtSignInProvider;
import top.tourist.backend.service.auth.provider.JwtVerifyProvider;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        prePostEnabled = true
)
public class SecurityConfigure extends WebSecurityConfigurerAdapter {

    private final JwtSuccessHandler jwtSuccessHandler;
    private final JwtSignInProvider jwtSignInProvider;
    private final JwtVerifyProvider jwtVerifyProvider;
    private final SessionClearLogoutHandler sessionClearLogoutHandler;

    public SecurityConfigure(JwtSuccessHandler jwtSuccessHandler,
                             JwtSignInProvider jwtSignInProvider,
                             JwtVerifyProvider jwtVerifyProvider,
                             SessionClearLogoutHandler sessionClearLogoutHandler) {
        this.jwtSuccessHandler = jwtSuccessHandler;
        this.jwtSignInProvider = jwtSignInProvider;
        this.jwtVerifyProvider = jwtVerifyProvider;
        this.sessionClearLogoutHandler = sessionClearLogoutHandler;
    }

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((authorize) ->
                                authorize
                                        // 设置无需认证的页面
                                        .mvcMatchers("/index").permitAll()
                                        .mvcMatchers("/signup").permitAll()
                                        .mvcMatchers("/signin").permitAll()
                                        .mvcMatchers("/api-docs/**", "/api-test/**").permitAll()
                                        .mvcMatchers("/swagger-ui/**").permitAll()
                                        .mvcMatchers("/shopping/**").permitAll()
                                        // 设置需要认证的页面
                                        .mvcMatchers("/greeting/**").hasRole("ADMIN")
                                        .mvcMatchers("/admin/**").hasRole("ADMIN")
                                        .anyRequest().authenticated()
                        // 其他控制器页面可以使用注解配置
                )
                // 设置跨域支持
                .cors(Customizer.withDefaults())
                // 关闭 CSRF 保护
                .csrf().disable()
                // 关闭默认 session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // 关闭浏览器内置 form 登录
                .formLogin().disable()
                // 设置支持跨域与 Jwt 验证的请求头
                .headers().addHeaderWriter(new StaticHeadersWriter(Arrays.asList(
                        new Header("Access-Control-Allow-Origin", "*"),
                        new Header("Access-Control-Expose-Headers", "Authorization")
                )))
                .and()
                // 设置请求认证
                .apply(new JwtSignInConfigure<>()).loginSuccessHandler(jwtSuccessHandler)
                .and()
                // 设置 Verify 处理链路
                .apply(new JwtVerifyConfigure<>()).tokenValidSuccessHandler(jwtSuccessHandler).permissiveRequestUrls("/logout")
                .and()
                // 拦截 OPTIONS 请求
                .apply(new OptionsConfigure<>())
                .and()
                // 设置 Logout 默认处理链路
                .logout()
                .logoutUrl("/logout")
                .addLogoutHandler(sessionClearLogoutHandler)
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 向 AuthenticationManager 中添加自定义的 Provider
        // 其验证到支持的 Token 封装类型后就会被调用
        auth
                .authenticationProvider(jwtSignInProvider)
                .authenticationProvider(jwtVerifyProvider)
                .eraseCredentials(false);
    }

}
