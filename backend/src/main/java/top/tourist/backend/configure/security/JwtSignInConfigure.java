package top.tourist.backend.configure.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import top.tourist.backend.filter.JwtLoginFilter;
import top.tourist.backend.service.auth.handler.JwtFailedHandler;

public class JwtSignInConfigure<T extends JwtSignInConfigure<T, B>, B extends HttpSecurityBuilder<B>> extends AbstractHttpConfigurer<T, B> {

    private final JwtLoginFilter jwtLoginFilter;

    public JwtSignInConfigure() {
        this.jwtLoginFilter = new JwtLoginFilter();
    }

    @Override
    public void configure(B builder) throws Exception {
        jwtLoginFilter.setAuthenticationManager(builder.getSharedObject(AuthenticationManager.class));
        jwtLoginFilter.setAuthenticationFailureHandler(new JwtFailedHandler());
        JwtLoginFilter filter = postProcess(jwtLoginFilter);
        builder.addFilterAfter(filter, LogoutFilter.class);
    }

    public JwtSignInConfigure<T, B> loginSuccessHandler(AuthenticationSuccessHandler handler) {
        jwtLoginFilter.setAuthenticationSuccessHandler(handler);
        return this;
    }

}
