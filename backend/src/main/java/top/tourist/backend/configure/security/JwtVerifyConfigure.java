package top.tourist.backend.configure.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import top.tourist.backend.filter.JwtVerifyFilter;
import top.tourist.backend.service.auth.handler.JwtFailedHandler;

public class JwtVerifyConfigure<T extends JwtSignInConfigure<T, B>, B extends HttpSecurityBuilder<B>> extends AbstractHttpConfigurer<T, B> {
    private final JwtVerifyFilter jwtVerifyFilter;

    public JwtVerifyConfigure() {
        this.jwtVerifyFilter = new JwtVerifyFilter();
    }

    @Override
    public void configure(B builder) throws Exception {
        jwtVerifyFilter.setAuthenticationManager(builder.getSharedObject(AuthenticationManager.class));
        jwtVerifyFilter.setFailureHandler(new JwtFailedHandler());
        JwtVerifyFilter filter = postProcess(jwtVerifyFilter);
        builder.addFilterBefore(filter, LogoutFilter.class);
    }

    public JwtVerifyConfigure<T, B> permissiveRequestUrls(String... urls) {
        jwtVerifyFilter.setPermissiveUrl(urls);
        return this;
    }

    public JwtVerifyConfigure<T, B> tokenValidSuccessHandler(AuthenticationSuccessHandler successHandler) {
        jwtVerifyFilter.setSuccessHandler(successHandler);
        return this;
    }

}
