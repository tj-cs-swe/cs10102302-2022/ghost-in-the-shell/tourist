package top.tourist.backend.configure.security;

import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import top.tourist.backend.filter.JwtVerifyFilter;
import top.tourist.backend.filter.OptionRequestFilter;

public class OptionsConfigure<T extends OptionsConfigure<T, B>, B extends HttpSecurityBuilder<B>> extends AbstractHttpConfigurer<T, B> {
    private final OptionRequestFilter optionsFilter;

    public OptionsConfigure() {
        optionsFilter = new OptionRequestFilter();
    }

    @Override
    public void configure(B builder) {
        OptionRequestFilter filter = postProcess(optionsFilter);
        builder.addFilterBefore(filter, JwtVerifyFilter.class);
    }
}
