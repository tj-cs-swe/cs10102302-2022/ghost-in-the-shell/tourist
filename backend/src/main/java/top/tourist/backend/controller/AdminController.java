package top.tourist.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.FirmProfile;
import top.tourist.backend.model.enumerate.EAccountRole;
import top.tourist.backend.service.account.JpaAccountService;
import top.tourist.backend.service.firm.JpaFirmService;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class AdminController {
    @Resource
    private JpaFirmService jpaFirmService;

    @Resource
    private JpaAccountService jpaAccountService;

    /**
     * return example: [
     * {
     * "name": "汪汪队",
     * "location": "汪汪省汪汪市汪汪学校",
     * "website": "https://wangwangdui.com",
     * "contact_name": "汪汪",
     * "contact_num": "13866668888",
     * "bank_name": "中国工商银行",
     * "bank_account": "6354345654",
     * "intro": "汪汪队靠卖零食赚大钱",
     * "state": "checking"
     * },
     * {
     * "name": "哈哈队",
     * "location": "哈哈省哈哈市哈哈学校",
     * "website": "https://hahadui.com",
     * "contact_name": "哈哈",
     * "contact_num": "13866668889",
     * "bank_name": "中国工商银行",
     * "bank_account": "6354345655",
     * "intro": "哈哈队靠卖零食赚大钱",
     * "state": "exit_checking"
     * }
     * ]
     */
    @GetMapping("/admin/getaduit")
    public Result<List<Map<String, String>>> getAllAudit() {
        List<FirmProfile> searchedList = jpaFirmService.getAllFirmInfo();

        List<Map<String, String>> resultList = new ArrayList<>();

        for (FirmProfile firmProfile : searchedList) {
            resultList.add(convertFirmToMap(firmProfile));
        }

        return Result.resultFactory(EResult.SUCCESS, resultList);
    }

    private Map<String, String> convertFirmToMap(FirmProfile firm) {
        Map<String, String> returnMap = new HashMap<>();

        returnMap.put("name", firm.getName());
        returnMap.put("location", firm.getLocation());
        returnMap.put("website", firm.getWebsite());
        returnMap.put("contact_name", firm.getContactName());
        returnMap.put("contact_num", firm.getContactTel());
        returnMap.put("bank_name", firm.getBankName());
        returnMap.put("bank_account", firm.getBankAccount());
        returnMap.put("intro", firm.getIntro());

        returnMap.put("state", jpaFirmService.shortToString(firm.getAuditStatus()));

        return returnMap;
    }

    @PutMapping("/admin/changeaduit")
    public Result<Integer> changeAudit(@RequestParam String name, @RequestParam String state) {
        FirmProfile firmProfile = jpaFirmService.findByName(name);
        if (firmProfile == null) {
            return Result.resultFactory(EResult.DATA_NULL, -1);
        }

        try {
            Short newStatusShort = jpaFirmService.stringToShort(state);
            firmProfile.setAuditStatus(newStatusShort);
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();

            return Result.resultFactory(EResult.BAD_REQUEST, -1);
        }

        jpaFirmService.updateFirm(firmProfile);

        return Result.resultFactory(EResult.SUCCESS, 0);
    }


    @GetMapping("/admin/getUsers")
    public Result<List<Map<String, String>>> getAllUser() {
        List<Account> accountList = jpaAccountService.findAccountsByRoles(EAccountRole.USER);

        List<Map<String, String>> resultList = new ArrayList<>();

        for (Account account : accountList) {
            resultList.add(convertUserToMap(account));
        }

        return Result.resultFactory(EResult.SUCCESS, resultList);
    }

    private Map<String, String> convertUserToMap(Account userAccount) {
        Map<String, String> returnMap = new HashMap<>();

        returnMap.put("id", userAccount.getId().toString());
        returnMap.put("name", userAccount.getName());
        returnMap.put("status", userAccount.isDisableStatus() ? "1" : "0");

        return returnMap;
    }

    @PutMapping("/admin/putUserid")
    public Result<Integer> putUserid(@RequestParam String id, @RequestParam String status) {
        long userId;
        try {
            userId = Long.parseLong(id);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return Result.resultFactory(EResult.BAD_REQUEST, -1);
        }

        Account account = jpaAccountService.findAccountById(userId);
        if (account == null || account.getRoles() != EAccountRole.USER) {
            return Result.resultFactory(EResult.DATA_NULL, -1);
        }

        account.setDisableStatus("1".equals(status));

        jpaAccountService.updateAccount(account);
        return Result.resultFactory(EResult.SUCCESS, 0);
    }

    @DeleteMapping("/admin/deleteUser")
    public Result<Integer> deleteUser(@RequestParam String id) {
        long userId;
        try {
            userId = Long.parseLong(id);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.resultFactory(EResult.BAD_REQUEST, -1);
        }

        Account account = jpaAccountService.findAccountById(userId);
        if (account == null || account.getRoles() != EAccountRole.USER) {
            return Result.resultFactory(EResult.DATA_NULL, -1);
        }

        jpaAccountService.deleteAccount(account);
        return Result.resultFactory(EResult.SUCCESS, 0);
    }
}
