package top.tourist.backend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import top.tourist.backend.model.dto.AccountDto;
import top.tourist.backend.model.vo.SignUpVo;
import top.tourist.backend.service.auth.AuthControllerService;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@Tag(name = "认证", description = "用户授权部分：成功后会在响应头中添加 Authorization : Bearer .... 形式的验证秘钥")
public class AuthController {

    private final AuthControllerService authService;

    public AuthController(AuthControllerService authService) {
        this.authService = authService;
    }

    @Operation(
            summary = "用户统一注册",
            tags = {"认证"},
            description = "公开访问，返回 Jwt 秘钥与用户基本信息"
    )
    @PostMapping(value = "/signup")
    public Result<AccountDto> SignUp(@RequestBody SignUpVo signUpVo, HttpServletResponse response) {
        if (signUpVo == null || response == null) {
            return Result.resultFactory(EResult.BAD_REQUEST, null);
        }
        try {
            Result<AccountDto> result = authService.signUpService(signUpVo);
            if (result.getData() == null) {
                HttpUtils.reviseResponseFormat(response, result.getCode());
                return result;
            }
            log.debug("Account : " + result.getData().getName() + " sign up successfully");
            HttpUtils.setTokenHeader(response, result.getData().getJwt());
            result.getData().setJwt("");
            HttpUtils.reviseResponseFormat(response, result.getCode());
            return result;
        } catch (AuthenticationException e) {
            HttpUtils.reviseResponseFormat(response, EResult.REQUEST_REJECT.getCode());
            return Result.resultFactory(EResult.REQUEST_REJECT, null);
        } catch (Exception e) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @Operation(
            summary = "用户统一登录",
            tags = {"认证"},
            description = "公开访问，返回 Jwt 秘钥与用户基本信息"
    )
    @PostMapping(value = "/signin")
    public Result<AccountDto> SignIn(HttpServletResponse response) {
        if (response == null) {
            return Result.resultFactory(EResult.BAD_REQUEST, null);
        }
        if (response.getStatus() == HttpServletResponse.SC_OK) {
            try {
                String name = SecurityContextHolder.getContext().getAuthentication().getName();

                AccountDto accountDto = authService.signInService(name);
                if (accountDto.isDisableStatus()) {
                    log.debug("Account : " + accountDto.getName() + "was disabled.");
                    return Result.resultFactory(EResult.REQUEST_REJECT, null);
                }

                log.debug("Account : " + accountDto.getName() + "sign in successfully");
                HttpUtils.setTokenHeader(response, accountDto.getJwt());
                accountDto.setJwt("");
                return Result.resultFactory(EResult.SUCCESS, accountDto);
            } catch (LockedException e) {
                HttpUtils.reviseResponseFormat(response, EResult.DATA_DUPLICATE.getCode());
                return Result.resultFactory(EResult.DATA_DUPLICATE, null);
            } catch (AuthenticationException e) {
                HttpUtils.reviseResponseFormat(response, EResult.AUTH_FAIL.getCode());
                return Result.resultFactory(EResult.AUTH_FAIL, null);
            } catch (Exception e) {
                HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
                return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
            } finally {
                SecurityContextHolder.clearContext();
            }
        } else {
            HttpUtils.reviseResponseFormat(response, EResult.AUTH_FAIL.getCode());
            return Result.resultFactory(EResult.AUTH_FAIL, null);
        }
    }

    @Operation(
            summary = "用户统一登出",
            tags = {"认证"},
            description = "验证访问，需要带有 jwt 请求头"
    )
    @PostMapping(value = "/logout")
    public String Logout(HttpServletResponse response) {
        return null;
    }

}
