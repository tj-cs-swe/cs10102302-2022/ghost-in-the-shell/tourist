package top.tourist.backend.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.ObjectNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import top.tourist.backend.model.dto.CalendarTodoDto;
import top.tourist.backend.model.vo.CalendarTodoVo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.service.calendar.CalendarService;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/calendar")
@Tag(name = "行程", description = "用户通过日历记录操作自己的形成信息")
public class CalendarController {

    private final CalendarService calendarService;

    public CalendarController(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/findAll")
    public Result<List<CalendarTodoDto>> FindAll(HttpServletResponse response) {
        try {
            long accountId = GetAccountIdFromAuth();
            List<CalendarTodoDto> todoList = calendarService.FindAllTodosForUser(accountId);
            Result<List<CalendarTodoDto>> result = Result.resultFactory(EResult.SUCCESS, todoList);
            HttpUtils.reviseResponseFormat(response, result.getCode());
            return result;
        } catch (Exception e) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/add")
    public Result<Long> Add(@RequestBody CalendarTodoVo todoVo, HttpServletResponse response) {
        try {
            long accountId = GetAccountIdFromAuth();
            Long todoId = calendarService.AddTodoForUser(accountId, todoVo);
            HttpUtils.reviseResponseFormat(response, EResult.SUCCESS.getCode());
            return Result.resultFactory(EResult.SUCCESS, todoId);
        } catch (IllegalArgumentException e) {
            HttpUtils.reviseResponseFormat(response, EResult.BAD_REQUEST.getCode());
            return Result.resultFactory(EResult.BAD_REQUEST, null);
        } catch (Exception e) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping(value = "/modify")
    public Result<String> Modify(@RequestBody CalendarTodoVo todoVo, HttpServletResponse response) {
        try {
            CalendarTodoDto todo = todoVo.toDto();
            calendarService.UpdateTodoById(todo);
            return HttpUtils.reviseStringResponseSuccess(response);
        } catch (IllegalArgumentException e) {
            return HttpUtils.reviseStringResponseFail(response, EResult.BAD_REQUEST);
        } catch (ObjectNotFoundException e) {
            return HttpUtils.reviseStringResponseFail(response, EResult.DATA_NULL);
        } catch (Exception e) {
            return HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @DeleteMapping(value = "/delete")
    public Result<String> Delete(@RequestParam String id, HttpServletResponse response) {
        try {
            Long longId = Long.parseLong(id);
            calendarService.DeleteTodoById(longId);
            return HttpUtils.reviseStringResponseSuccess(response);
        } catch (ObjectNotFoundException e) {
            return HttpUtils.reviseStringResponseFail(response, EResult.DATA_NULL);
        } catch (Exception e) {
            return HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
        }
    }

    private long GetAccountIdFromAuth() {
        JwtVerifyToken token;
        try {
            token = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
            return token.getTokenVo().getAccountId();
        } catch (Exception e) {
            throw new AuthenticationCredentialsNotFoundException("");
        }
    }
}
