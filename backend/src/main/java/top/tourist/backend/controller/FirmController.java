package top.tourist.backend.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import top.tourist.backend.model.dto.FirmProfileDto;
import top.tourist.backend.service.firm.JpaFirmService;
import top.tourist.backend.utils.result.Result;

import javax.annotation.Resource;

@RestController
public class FirmController {

    @Resource
    private JpaFirmService jpaFirmService;

    @GetMapping(value = "/credit/getStatus")
    public Result<String> getStatus()
    {
        Result<String> result =new Result<>();
        result.setData(jpaFirmService.getStatus());
        return result;
    }

    @GetMapping(value = "/credit/getInfo")
    public Result<FirmProfileDto> getInfo()
    {
        Result<FirmProfileDto> result = new Result<>();
        result.setData(jpaFirmService.getInfo());
        return result;
    }

    @PutMapping(value = "/credit/changeInfo")
    public Result<Integer> changeInfo(@RequestBody FirmProfileDto newFirm)
    {
        Result<Integer> result = new Result<>();
        result.setData(jpaFirmService.changeInfo(newFirm));
        return result;
    }

    @PutMapping(value = "/credit/rechecking")
    public Result<Integer> rechecking(@RequestBody FirmProfileDto newFirm)
    {
        Result<Integer> result = new Result<>();
        result.setData(jpaFirmService.rechecking(newFirm));
        return result;
    }

    @PutMapping(value = "/credit/signContract")
    public Result<Integer> signContract()
    {
        Result<Integer> result = new Result<>();
        result.setData(jpaFirmService.signContract());
        return result;
    }

    @PutMapping(value = "/credit/applyExit")
    public Result<Integer> applyExit(String reason)
    {
        Result<Integer> result = new Result<>();
        result.setData(jpaFirmService.applyExit(reason));
        return result;
    }

    @PutMapping(value = "/credit/applySettleAgain")
    public Result<Integer> applySettleAgain()
    {
        Result<Integer> result = new Result<>();
        result.setData(jpaFirmService.applySettleAgain());
        return result;
    }
}
