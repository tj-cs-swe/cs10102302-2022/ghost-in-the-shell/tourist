package top.tourist.backend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import top.tourist.backend.model.entity.util.Greeting;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@Tag(name = "打招呼", description = "连接鉴权测试")
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Operation(
            summary = "连接鉴权测试",
            tags = {"打招呼"}
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200", description = "连接成功"
                    ),
                    @ApiResponse(
                            responseCode = "401", description = "连接失败，未能校验身份"
                    ),
                    @ApiResponse(
                            responseCode = "403", description = "连接失败，未能校验身份"
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "服务器处理错误"
                    )
            }
    )
    @GetMapping("/greeting/{name}")
    public Greeting greeting(@PathVariable(name = "name", required = true) String name, HttpServletResponse response) {

        Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        if (auth == null) {
            HttpUtils.reviseResponseFormat(response, EResult.AUTH_FAIL.getCode());
            return null;
        }
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
}
