package top.tourist.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import top.tourist.backend.model.vo.ChangeLabel;
import top.tourist.backend.model.vo.LabelWithRole;
import top.tourist.backend.model.vo.TagsVo;
import top.tourist.backend.service.label.TagsService;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
public class LabelController {

    private final TagsService tagsService;

    public LabelController(TagsService tagsService) {
        this.tagsService = tagsService;
    }

    @PreAuthorize("hasAnyRole('USER', 'FIRM')")
    @PutMapping(value = {"/profile/saveLabel", "/bus/saveLabel"})
    public Result<Integer> SaveLabel(@RequestBody List<TagsVo> voList, HttpServletResponse response) {
        try {
            tagsService.UpdateTags(voList);
            return Result.resultFactory(EResult.SUCCESS, 0);
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, -1);
        }
    }

    @PreAuthorize("hasAnyRole('USER', 'FIRM')")
    @GetMapping(value = {"/profile/getLabel", "/bus/getLabel"})
    public Result<List<TagsVo>> GetLabel(HttpServletResponse response) {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.GetTags());
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

//    @PreAuthorize("hasAnyRole('USER', 'FIRM')")
//    @GetMapping(value = {"/profile/getAllUserLabel", "/bus/getAllBusLabel"})
//    public Result<List<TagsVo>> GetAdminLabel(HttpServletResponse response) {
//        try {
//            return Result.resultFactory(EResult.SUCCESS, tagsService.GetAdminTags());
//        } catch (Exception e) {
//            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
//            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
//        }
//    }

    // 用户获取所有用户标签
    @PreAuthorize("hasAnyRole('USER')")
    @GetMapping(value = {"/profile/getAllUserLabel"})
    public Result<List<String>> UserGetAllUserLabels(HttpServletResponse response) {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.GetAllLabelsByRoles("USER"));
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    // 商户获取所有商户标签
    @PreAuthorize("hasAnyRole('FIRM')")
    @GetMapping(value = {"/bus/getAllBusLabel"})
    public Result<List<String>> FirmGetAllFirmLabels(HttpServletResponse response) {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.GetAllLabelsByRoles("FIRM"));
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    // 管理员获取所有标签
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/admin/getLabel")
    public Result<List<LabelWithRole>> AdminGetLabel(HttpServletResponse response)
    {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.adminGetAllLabels());
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    // 管理员添加新的标签
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/admin/addLabel")
    public Result<Integer> AdminAddLabel(@RequestBody LabelWithRole roleLabel, HttpServletResponse response)
    {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.adminAddLabel(roleLabel));
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, -1);
        }
    }

    // 管理员删除若干标签
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/admin/deleteLabels")
    public Result<Integer> AdminDeleteLabels(@RequestBody List<LabelWithRole> list, HttpServletResponse response)
    {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.adminDeleteLabel(list));
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, -1);
        }
    }

    // 管理员修改标签类别
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/admin/changeLabel")
    public Result<Integer> AdminAddLabel(@RequestBody ChangeLabel changeLabel, HttpServletResponse response)
    {
        try {
            return Result.resultFactory(EResult.SUCCESS, tagsService.adminChangeLabelRoles(changeLabel));
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, -1);
        }
    }
}
