package top.tourist.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import top.tourist.backend.model.vo.TicketInfoVo;
import top.tourist.backend.model.vo.TicketsNumVo;
import top.tourist.backend.service.commerce.OrderService;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "ticket")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "buy")
    public Result<String> buyTicket(@RequestBody TicketsNumVo ticketsNumVo, HttpServletResponse response) {
        try {
            orderService.RequestBuyTickets(ticketsNumVo);
            return Result.success(EResult.SUCCESS.getMsg());
        } catch (IllegalArgumentException e) {
            HttpUtils.reviseResponseFormat(response, EResult.BAD_REQUEST.getCode());
            return Result.resultFactory(EResult.BAD_REQUEST, e.getMessage());
        } catch (UnsupportedOperationException e) {
            HttpUtils.reviseResponseFormat(response, EResult.REQUEST_REJECT.getCode());
            return Result.resultFactory(EResult.REQUEST_REJECT, e.getMessage());
        } catch (Exception e) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
            return Result.resultFactory(EResult.UNKNOWN_ERROR, EResult.UNKNOWN_ERROR.getMsg());
        }
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "findAll")
    public Result<List<TicketInfoVo>> findAllTickets(HttpServletResponse response) {
        try {
            return Result.resultFactory(EResult.SUCCESS, orderService.GetAllTicketsForUser());
        } catch (Exception ignored) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
            return Result.failed(EResult.UNKNOWN_ERROR);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "refund")
    public Result<String> refundTickets(@RequestBody TicketsNumVo ticketsNumVo, HttpServletResponse response) {
        try {
            Double refundPrice = orderService.RefundOrderPartial(ticketsNumVo);
            return Result.resultFactory(EResult.SUCCESS, refundPrice.toString());
        } catch (IllegalArgumentException e) {
            HttpUtils.reviseResponseFormat(response, EResult.BAD_REQUEST.getCode());
            return Result.resultFactory(EResult.BAD_REQUEST, e.getMessage());
        } catch (UnsupportedOperationException e) {
            HttpUtils.reviseResponseFormat(response, EResult.REQUEST_REJECT.getCode());
            return Result.resultFactory(EResult.REQUEST_REJECT, e.getMessage());
        } catch (Exception e) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
            return Result.resultFactory(EResult.UNKNOWN_ERROR, EResult.UNKNOWN_ERROR.getMsg());
        }
    }

}
