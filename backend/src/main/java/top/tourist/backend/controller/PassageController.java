package top.tourist.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import top.tourist.backend.model.vo.CreatedArticle;
import top.tourist.backend.model.vo.CreatedComment;
import top.tourist.backend.model.vo.Passage;
import top.tourist.backend.model.vo.PassageWithComment;
import top.tourist.backend.service.passage.PassageService;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
public class PassageController {

    @Resource
    private PassageService passageService;

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(value = {"/passage/findAll"})
    public Result<List<Passage>> FindALLPassage(HttpServletResponse response) {
        try {
            return Result.resultFactory(EResult.SUCCESS, passageService.FindALLPassage());
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(value = {"/passage/getContent"})
    public Result<PassageWithComment> FindPassageComment(@RequestParam String id, HttpServletResponse response)
    {
        try {
            return Result.resultFactory(EResult.SUCCESS, passageService.FindPassageComment(Long.parseLong(id)));
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping (value = {"/passage/add"})
    public Result<String> UserCreateArticle(@RequestBody CreatedArticle createdArticle, HttpServletResponse response)
    {
        try {
            passageService.UserCreateArticle(createdArticle);
            return Result.resultFactory(EResult.SUCCESS, "Add Success");
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = {"/passage/addComment"})
    public Result<String> UserCreateComment(@RequestBody CreatedComment createdComment, HttpServletResponse response)
    {
        try {
            passageService.UserCreateComment(createdComment);
            return Result.resultFactory(EResult.SUCCESS, "Add Success");
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @DeleteMapping (value = {"/passage/delete"})
    public Result<String> UserDeleteArticle(@RequestParam String id, HttpServletResponse response)
    {
        try {
            passageService.UserDeleteArticle(Long.parseLong(id));
            return Result.resultFactory(EResult.SUCCESS, "success");
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @DeleteMapping(value = {"/passage/delComment"})
    public Result<String> UserDeleteComment(@RequestParam String id, HttpServletResponse response)
    {
        try {
            passageService.UserDeleteComment(Long.parseLong(id));
            return Result.resultFactory(EResult.SUCCESS, "success");
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.UNKNOWN_ERROR, null);
        }
    }
}

