package top.tourist.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import top.tourist.backend.model.entity.commerce.Product;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.service.product.JpaProductService;
import top.tourist.backend.utils.data.SnowFlakeUtils;
import top.tourist.backend.utils.general.TimeUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class ProductOprController {

    private final SnowFlakeUtils idGen = new SnowFlakeUtils(1, 0);

    @Resource
    private JpaProductService jpaProductService;

    // 在测试该API之前需要注册FIRM，并将该用户添加入firm_profile表中
    @PostMapping("/product/add")
    public Result<String> addProduct(@RequestBody Map<String, Object> newProductJson) {
        // 解析传来的json
        String name = (String) newProductJson.get("name");
        String address = (String) newProductJson.get("address");

        Object priceObj = newProductJson.get("price");

        Integer remain = (Integer) newProductJson.get("remain");
        String intro = (String) newProductJson.get("intro");

        // 获取商家id
        JwtVerifyToken jwtVerifyToken =  (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();

        Long firm_id = jwtVerifyToken.getTokenVo().getAccountId();


        Product product = new Product();

        product.setId(idGen.NextId());

        product.setFirmId(firm_id);
        product.setName(name);

        product.setPrice(parsePrice(priceObj));


        product.setAddress(address);
        product.setRemain(remain);

        product.setCreateTime(TimeUtils.getTime());
        product.setUpdateTime(TimeUtils.getTime());

        if (intro != null) {
            product.setIntro(intro);
        }

        boolean insertSuccess = jpaProductService.insertProduct(product);
        Result<String> result;
        if (insertSuccess) {
            result = Result.resultFactory(EResult.SUCCESS, "Add success");
        }
        else {
            result = Result.resultFactory(EResult.DATA_NULL, "Invalid data");
        }

        return result;
    }

    @DeleteMapping("/product/delete")
    public Result<Integer> deleteProduct(@RequestParam String name) {
        List<Product> productList = jpaProductService.findProductByName(name);

        int cnt = 0;
        for (Product product : productList) {
            if (jpaProductService.deleteProduct(product)) {
                ++cnt;
            }
        }

        return Result.resultFactory(EResult.SUCCESS, cnt);
    }

    @PostMapping("/product/modify")
    public Result<String> modifyProduct(@RequestBody Map<String, Object> incomeJson) {
        // 解析传来的json
        String old_name = (String) incomeJson.get("oldname");
        String new_name = (String) incomeJson.get("name");
        String address = (String) incomeJson.get("address");

        Object priceObj = incomeJson.get("price");

        Integer remain = (Integer) incomeJson.get("remain");
        String intro = (String) incomeJson.get("intro");

        List<Product> productList = jpaProductService.findProductByName(old_name);
        if (productList.isEmpty()) {
            return Result.resultFactory(EResult.DATA_NULL, "No such product");
        }

        if (productList.size() != 1) {
            return Result.resultFactory(EResult.DATA_DUPLICATE, "More than one matched product");
        }

        Product product = productList.get(0);

        if (new_name != null) {
            product.setName(new_name);
        }

        if (address != null) {
            product.setAddress(address);
        }

        if (priceObj != null) {
            product.setPrice(parsePrice(priceObj));
        }

        if (remain != null) {
            product.setRemain(remain);
        }

        if (intro != null) {
            product.setIntro(intro);
        }

        product.setUpdateTime(LocalDateTime.now());

        if (jpaProductService.updateProduct(product)) {
            return Result.resultFactory(EResult.SUCCESS, "success");
        }
        else {
            return Result.resultFactory(EResult.UNKNOWN_ERROR, "unknown error");
        }
    }

    @GetMapping("/product/findAll")
    public List<Map<String, Object>> findAllProduct() {
        List<Product> products = jpaProductService.findAllProduct();
        List<Map<String, Object>> returnMapList = new ArrayList<>();

        for (Product product : products) {
            returnMapList.add(convertToMap(product));
        }

        return returnMapList;
    }

    private Map<String, Object> convertToMap(Product product) {
        Map<String, Object> returnMap = new HashMap<>();

        returnMap.put("id", product.getId());
        returnMap.put("name", product.getName());
        returnMap.put("address", product.getAddress());
        returnMap.put("price", product.getPrice());
        returnMap.put("remain", product.getRemain());
        returnMap.put("intro", product.getIntro());

        return returnMap;
    }

    private Double parsePrice(Object incomePrice) {
        if (incomePrice instanceof Double) {
            return (Double) incomePrice;
        }
        else {
            return ((Integer)incomePrice).doubleValue();
        }
    }

    @GetMapping("/shopping/search")
    private Result<List<Map<String, Object>>> searchProduct(@RequestParam String name) {
        List<Product> searchedProducts = jpaProductService.findProductsByNameContains(name);

        List<Map<String, Object>> resultList = new ArrayList<>();

        for (Product product : searchedProducts) {
            resultList.add(convertToMap(product));
        }

        return Result.resultFactory(EResult.SUCCESS, resultList);
    }

    @GetMapping("/shopping/getproducts")
    private Result<List<Map<String, Object>>> getRecommendProducts() {
        List<Product> recommendProducts = jpaProductService.getRecommendProducts();

        List<Map<String, Object>> resultList = new ArrayList<>();

        for (Product product : recommendProducts) {
            resultList.add(convertToMap(product));
        }

        return Result.resultFactory(EResult.SUCCESS, resultList);
    }
}
