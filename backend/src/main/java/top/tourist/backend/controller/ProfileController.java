package top.tourist.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import top.tourist.backend.model.dto.UserProfileDto;
import top.tourist.backend.model.vo.UserProfileVo;
import top.tourist.backend.service.profile.UserProfileService;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping(value = "/user")
public class ProfileController {

    private final UserProfileService userProfileService;

    public ProfileController(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping(value = "/saveMessage")
    public Result<Integer> UpdateUserProfile(@RequestBody UserProfileVo vo, HttpServletResponse response) {
        try {
            if (userProfileService.UpdateUserProfile(vo) > 0) {
                return Result.resultFactory(EResult.SUCCESS, 0);
            }
            throw new IllegalArgumentException();
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.BAD_REQUEST);
            return Result.resultFactory(EResult.BAD_REQUEST, -1);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/getMessage")
    public Result<UserProfileDto> GetUserProfile(HttpServletResponse response) {
        try {
            return userProfileService.GetUserProfile();
        } catch (Exception e) {
            HttpUtils.reviseStringResponseFail(response, EResult.UNKNOWN_ERROR);
            return Result.resultFactory(EResult.BAD_REQUEST, null);
        }
    }

}
