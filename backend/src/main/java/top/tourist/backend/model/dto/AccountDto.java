package top.tourist.backend.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.tourist.backend.model.enumerate.EAccountRole;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = " 账户响应体 返回账户的详细信息")
public class AccountDto implements Serializable {

    @Schema(description = "用户ID", implementation = Long.class)
    private Long id;

    @Schema(description = "用户名", implementation = String.class)
    private String name;

    @Schema(description = "用户角色", implementation = EAccountRole.class)
    private EAccountRole roles;

    @Schema(description = "用户封禁状态", implementation = boolean.class)
    private boolean disableStatus;

    @Schema(description = "登录时间", implementation = LocalDateTime.class)
    private LocalDateTime signinTime;

    @Schema(description = "登录状态验证串(置于 Authorization 请求头中)", implementation = String.class)
    private String jwt;
}
