package top.tourist.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import top.tourist.backend.model.enumerate.ECalendarState;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class CalendarTodoDto implements Serializable {
    private final Long id;
    private final String name;
    private final String location;
    private ECalendarState state;
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;
    private final boolean privateControl;
    private final boolean allDay;
}
