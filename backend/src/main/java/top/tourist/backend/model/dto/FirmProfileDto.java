package top.tourist.backend.model.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class FirmProfileDto implements Serializable {
    private final Long id;
    private final String name;
    private final String avatarURl;
    private final String certURL;
    private final Short level;
    private final Short auditStatus;
    private final String intro;
    private final String location;
    private final String website;
    private final String contactName;
    private final String contactTel;
    private final String bankName;
    private final String bankAccount;
    private final Double turnover;
}
