package top.tourist.backend.model.dto;


import lombok.Data;
import top.tourist.backend.model.entity.account.UserProfile;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class UserProfileDto implements Serializable {
    private String nickname;
    private Integer articles;
    private Integer comments;
    private String motto;
    private String email;
    private LocalDateTime createTime;

    public UserProfileDto(UserProfile profile, Integer articles, Integer comments) {
        this.articles = articles;
        this.comments = comments;
        this.nickname = profile.getNickname();
        this.motto = profile.getMotto();
        this.email = profile.getEmail();
        this.createTime = profile.getCreateTime();
    }
}
