package top.tourist.backend.model.dto.mapper;

import org.mapstruct.*;
import top.tourist.backend.model.dto.AccountDto;
import top.tourist.backend.model.entity.account.Account;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AccountMapper {
    Account toAccount(AccountDto accountDto);

    AccountDto toAccountDto(Account account);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateFromDto(AccountDto accountDto, @MappingTarget Account account);
}
