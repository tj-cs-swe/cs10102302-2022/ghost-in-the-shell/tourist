package top.tourist.backend.model.dto.mapper;

import org.mapstruct.*;
import top.tourist.backend.model.dto.CalendarTodoDto;
import top.tourist.backend.model.entity.calendar.CalendarTodo;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CalendarTodoMapper {
    CalendarTodo ToCalendarTodo(CalendarTodoDto calendarTodoDto);

    CalendarTodoDto ToDto(CalendarTodo calendarTodo);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateFromDto(CalendarTodoDto calendarTodoDto, @MappingTarget CalendarTodo calendarTodo);
}
