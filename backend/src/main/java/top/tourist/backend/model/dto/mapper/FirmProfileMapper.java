package top.tourist.backend.model.dto.mapper;

import org.mapstruct.*;
import top.tourist.backend.model.dto.FirmProfileDto;
import top.tourist.backend.model.entity.account.FirmProfile;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface FirmProfileMapper {
    FirmProfile ToFirmProfile(FirmProfileDto firmProfileDto);

    FirmProfileDto ToDto(FirmProfile firmProfile);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateFromDto(FirmProfileDto firmProfileDto, @MappingTarget FirmProfile firmProfile);
}
