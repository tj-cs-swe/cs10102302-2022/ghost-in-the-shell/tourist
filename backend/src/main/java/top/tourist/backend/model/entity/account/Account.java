package top.tourist.backend.model.entity.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import top.tourist.backend.model.entity.forum.Bulletin;
import top.tourist.backend.model.entity.notice.AccountNotice;
import top.tourist.backend.model.entity.tag.AccountTagsRel;
import top.tourist.backend.model.entity.util.ImageStore;
import top.tourist.backend.model.enumerate.EAccountRole;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(name = "account", indexes = {
        @Index(name = "index_auth_name", columnList = "name")
})
@EntityListeners(AuditingEntityListener.class)
public class Account implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 255, nullable = false, unique = true)
    private String name;

    @Column(name = "password", length = 511, nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "roles", nullable = false)
    private EAccountRole roles;

    @Column(name = "status", nullable = false)
    private boolean disableStatus;

    @CreatedDate
    @Column(name = "signup_time", nullable = false)
    private LocalDateTime signupTime;

    @Column(name = "signin_time", nullable = false)
    private LocalDateTime signinTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    @Column(name = "disable_time")
    private LocalDateTime disableTime;

    public Account(Long idIn, String nameIn, String passwordIn, EAccountRole rolesIn,
                   boolean disableStatusIn, LocalDateTime signinTimeIn) {
        this.id = idIn;
        this.name = nameIn;
        this.password = passwordIn;
        this.roles = rolesIn;
        this.disableStatus = disableStatusIn;
        this.signinTime = signinTimeIn;
        this.disableTime = null;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", status=" + disableStatus +
                ", signupTime=" + signupTime +
                ", signinTime=" + signinTime +
                ", updateTime=" + updateTime +
                ", disableTime=" + disableTime +
                '}';
    }

    @OneToOne(
            optional = true,
            targetEntity = UserProfile.class,
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            mappedBy = "userAccount"
    )
    private UserProfile userProfile;

    @OneToOne(
            optional = true,
            targetEntity = FirmProfile.class,
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            mappedBy = "firmAccount"
    )
    private FirmProfile firmProfile;

    @OneToOne(
            optional = true,
            targetEntity = AuthSession.class,
            cascade = {CascadeType.REMOVE},
            mappedBy = "account"
    )
    private AuthSession authSession;

    @OneToMany(
            targetEntity = AccountTagsRel.class,
            mappedBy = "account",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST}
    )
    private Set<AccountTagsRel> accountTagsRel;

    @OneToMany(
            targetEntity = AccountNotice.class,
            mappedBy = "sendAccount",
            cascade = {CascadeType.REMOVE}
    )
    private Set<AccountNotice> senderNotice;

    @OneToMany(
            targetEntity = AccountNotice.class,
            mappedBy = "receiveAccount",
            cascade = {CascadeType.REMOVE}
    )
    private Set<AccountNotice> receiverNotice;

    @OneToMany(
            targetEntity = Bulletin.class,
            mappedBy = "adminAccount",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Bulletin> bulletinSet;

    @OneToMany(
            targetEntity = ImageStore.class,
            mappedBy = "uploadAccount",
            cascade = {CascadeType.REMOVE}
    )
    private Set<ImageStore> imageStore;

}
