package top.tourist.backend.model.entity.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(name = "auth_session")
@EntityListeners(AuditingEntityListener.class)
@Transactional
public class AuthSession {
    @Id
    private Long id;

    @Column(name = "account_id", nullable = false, unique = true)
    private Long accountId;

    @Column(name = "secret", nullable = false)
    private String secret;

    @Column(name = "expire_time", nullable = false)
    private LocalDateTime expireTime;

    @Column(name = "close_status", nullable = false)
    private boolean closeStatus;

    @OneToOne(
            optional = false,
            targetEntity = Account.class,
            fetch = FetchType.EAGER
    )
    @JoinColumn(name = "account_id", referencedColumnName = "id", updatable = false, insertable = false,
            foreignKey = @ForeignKey(name = "fk_auth_session_account_id"))
    private Account account;

    public AuthSession(Long id, Long accountId, String secret, LocalDateTime expireTime, boolean closeStatus) {
        this.id = id;
        this.accountId = accountId;
        this.secret = secret;
        this.expireTime = expireTime;
        this.closeStatus = closeStatus;
    }

//    public boolean IsInvalid() {
//        return this.expireTime.after(TimeUtils.getTime()) || closeStatus;
//    }
}
