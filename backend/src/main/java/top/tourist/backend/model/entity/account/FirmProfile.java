package top.tourist.backend.model.entity.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.model.entity.commerce.Product;
import top.tourist.backend.model.entity.notice.IssueFirmRequest;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "firm_profile", indexes = {
        @Index(name = "index_firm_name", columnList = "name")
})
public class FirmProfile implements Serializable {
    @Id
    private Long id;

    @Column(name = "name", nullable = false, length = 255)
    private String name;

    @Column(name = "avatar_url", length = 1023)
    private String avatarURl;

    @Column(name = "cert_url", length = 1023)
    private String certURL;

    @Column(name = "level", nullable = false)
    private Short level;

    @Column(name = "audit_status", nullable = false)
    private Short auditStatus;

    @Column(name = "intro")
    private String intro;

    @Column(name = "location", length = 255)
    private String location;

    @Column(name = "website", length = 1023)
    private String website;

    @Column(name = "contact_name", length = 63)
    private String contactName;

    @Column(name = "contact_num", length = 127)
    private String contactTel;

    @Column(name = "bank_name", length = 127)
    private String bankName;

    @Column(name = "bank_account", length = 127)
    private String bankAccount;

    @Column(name = "turnover")
    private Double turnover;

    public FirmProfile(Long idIn, String nameIn) {
        this.id = idIn;
        this.name = nameIn;
        this.avatarURl = "";
        this.certURL = "";
        this.level = (short) 0;
        this.auditStatus = (short) 0;
        this.intro = "";
        this.location = "";
        this.website = "";
        this.contactName = "";
        this.contactTel = "";
        this.bankName = "";
        this.bankAccount = "";
        this.turnover = 0.0;
    }

    @OneToOne(
            targetEntity = Account.class,
            optional = true
    )
    @JoinColumn(name = "id", referencedColumnName = "id", updatable = false, insertable = false,
            foreignKey = @ForeignKey(name = "fk_firm_profile_auth_id"))
    private Account firmAccount;

    @OneToMany(
            targetEntity = IssueFirmRequest.class,
            mappedBy = "firm",
            cascade = {CascadeType.REMOVE}
    )
    private Set<IssueFirmRequest> issue;

    @OneToMany(
            targetEntity = Product.class,
            mappedBy = "firm",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Product> products;

}
