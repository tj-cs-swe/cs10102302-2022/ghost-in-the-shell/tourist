package top.tourist.backend.model.entity.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.calendar.CalendarTodo;
import top.tourist.backend.model.entity.commerce.UserOrder;
import top.tourist.backend.model.entity.forum.Article;
import top.tourist.backend.model.entity.forum.Comment;
import top.tourist.backend.model.entity.notice.IssueUserRequest;
import top.tourist.backend.utils.general.TimeUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(name = "user_profile", indexes = {
        @Index(name = "index_profile_nickname", columnList = "nickname" )
})
public class UserProfile implements Serializable {

    @Id
    private Long id;

    @Column(name = "nickname", nullable = false, length = 255)
    private String nickname;

    @Column(name = "level", nullable = false)
    private Short level = 0;

    @Column(name = "avatar_url", length = 1023)
    private String avatarUrl;

    @Column(name = "email", length = 127)
    private String email;

    @Column(name = "motto", length = 255)
    private String motto;

    @Column(name = "create_time", nullable = false)
    @CreatedDate
    private LocalDateTime createTime;

    @Column(name = "update_time", nullable = false)
    @LastModifiedDate
    private LocalDateTime updateTime;

    public UserProfile(Long idIn, String nicknameIn, Short levelIn, String avatarUrlIn, String emailIn, String mottoIn) {
        this.id = idIn;
        this.nickname = nicknameIn;
        this.level = levelIn;
        this.avatarUrl = avatarUrlIn;
        this.email = emailIn;
        this.motto = mottoIn;
        this.createTime = TimeUtils.getTime();
        this.updateTime = TimeUtils.getTime();
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", level=" + level +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", email='" + email + '\'' +
                ", motto='" + motto + '\'' +
                '}';
    }

    @OneToOne(
            optional = true,
            targetEntity = Account.class
    )
    @JoinColumn(name = "id", referencedColumnName = "id", updatable = false, insertable = false,
            foreignKey = @ForeignKey(name = "fk_user_profile_auth_id"))
    private Account userAccount;

    @OneToMany(
            targetEntity = Article.class,
            mappedBy = "userProfile",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Article> articles;

    @OneToMany(
            targetEntity = CalendarTodo.class,
            mappedBy = "todoOwner",
            cascade = {CascadeType.REMOVE}
    )
    private Set<CalendarTodo> todos;

    @OneToMany(
            targetEntity = Comment.class,
            mappedBy = "commentOwner",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Comment> comments;

    @OneToMany(
            targetEntity = IssueUserRequest.class,
            mappedBy = "issueOwner",
            cascade = {CascadeType.REMOVE}
    )
    private Set<IssueUserRequest> issue;

    @OneToMany(
            targetEntity = UserOrder.class,
            mappedBy = "user",
            cascade = {CascadeType.REMOVE}
    )
    private Set<UserOrder> orders;

}
