package top.tourist.backend.model.entity.calendar;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.enumerate.ECalendarState;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(name = "calendar_todo", indexes = {
        @Index(name = "index_todo_user_id", columnList = "user_id")
})
public class CalendarTodo implements Serializable {
    @Id
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "name", length = 511, nullable = false)
    private String name;

    @Column(name = "location")
    private String location;

    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private ECalendarState state = ECalendarState.BUSY;

    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;

    @Column(name = "private_control", nullable = false)
    private boolean privateControl = false;

    @Column(name = "all_day", nullable = false)
    private boolean allDay = false;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    public CalendarTodo(
            Long idIn, Long userIdIn, String nameIn, String locationIn,
            ECalendarState stateIn, LocalDateTime startTimeIn, LocalDateTime endTimeIn,
            boolean isPrivateIn, boolean isAllDayIn) {
        this.id = idIn;
        this.userId = userIdIn;
        this.name = nameIn;
        this.location = locationIn;
        this.state = stateIn;
        this.startTime = startTimeIn;
        this.endTime = endTimeIn;
        this.privateControl = isPrivateIn;
        this.allDay = isAllDayIn;
    }

    @ManyToOne(
            targetEntity = UserProfile.class,
            optional = false
    )
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_calendar_issue_auth_id"))
    private UserProfile todoOwner;
}
