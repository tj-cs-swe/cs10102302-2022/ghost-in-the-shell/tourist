package top.tourist.backend.model.entity.commerce;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.model.entity.account.FirmProfile;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product", indexes = {
        @Index(name = "index_product_name", columnList = "name")
})
public class Product {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "firm_id", nullable = false)
    private Long firmId;

    @Column(name = "name", length = 255, nullable = false)
    private String name;

    @Column(name = "price")
    private Double price;

    @Column(name = "remain", nullable = false)
    private Integer remain;

    @Column(name = "intro")
    private String intro;

    @Column(name = "address", length = 255, nullable = false)
    private String address;

    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    @ManyToOne(
            targetEntity = FirmProfile.class,
            optional = false
    )
    @JoinColumn(name = "firm_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_product_firm_id"))
    private FirmProfile firm;

    @OneToMany(
            targetEntity = UserOrder.class,
            mappedBy = "product",
            cascade = {CascadeType.REMOVE}
    )
    private Set<UserOrder> order;

    @OneToMany(
            targetEntity = ProductImage.class,
            mappedBy = "product",
            cascade = {CascadeType.REMOVE}
    )
    private Set<ProductImage> productImages;

}
