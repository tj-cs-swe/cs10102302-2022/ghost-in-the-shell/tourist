package top.tourist.backend.model.entity.commerce;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.model.entity.util.ImageStore;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_image")
public class ProductImage implements Serializable {
    @Id
    @Column(name = "image_id")
    private Long imageId;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "serial")
    private Integer serial;

    @ManyToOne(
            targetEntity = ImageStore.class,
            optional = false
    )
    @JoinColumn(name = "image_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_image_store_id"))
    private ImageStore imageStore;

    @ManyToOne(
            targetEntity = Product.class,
            optional = false
    )
    @JoinColumn(name = "product_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_image_product_id"))
    private Product product;


}
