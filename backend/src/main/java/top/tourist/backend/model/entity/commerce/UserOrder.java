package top.tourist.backend.model.entity.commerce;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.enumerate.EOrderStatus;
import top.tourist.backend.utils.general.TimeUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_order")
public class UserOrder implements Serializable {
    @Id
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "product_id", nullable = false)
    private Long productId;

    @Column(name = "product_num", nullable = false)
    private Integer productNum;

    @Column(name = "price", nullable = false)
    private Double price;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private EOrderStatus status;

    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    @Column(name = "coupon", length = 1023)
    private String coupon;

    @Column(name = "expire_time")
    private LocalDateTime expireTime;

    public UserOrder(Long idIn, Long userIdIn, Long productIdIn, Integer productNumIn, Double priceIn) {
        this.id = idIn;
        this.userId = userIdIn;
        this.productId = productIdIn;
        this.productNum = productNumIn;
        this.price = priceIn;
        this.createTime = TimeUtils.getTime();
        this.updateTime = TimeUtils.getTime();
        this.status = EOrderStatus.CHECKING;
        this.coupon = null;
        this.expireTime = null;
    }

    @ManyToOne(
            targetEntity = UserProfile.class,
            optional = false
    )
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_order_user_id"))
    private UserProfile user;

    @ManyToOne(
            targetEntity = Product.class,
            optional = false
    )
    @JoinColumn(name = "product_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_order_product_id"))
    private Product product;

}
