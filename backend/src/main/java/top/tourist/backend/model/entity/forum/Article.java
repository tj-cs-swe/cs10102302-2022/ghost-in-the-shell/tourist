package top.tourist.backend.model.entity.forum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.entity.tag.ArticleTagsRel;
import top.tourist.backend.model.vo.CreatedArticle;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "article")
public class Article implements Serializable {
    @Id
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long user_id;

    @Column(name=  "topic_id")
    private Long topic_id;

    @Column(name = "title", length = 255, nullable = false)
    private String title;

    @Column(name = "cont")
    private String cont;

    @Column(name = "views")
    private Long views;

    @Column(name = "likes")
    private Long likes;

    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    @ManyToOne(
            targetEntity = UserProfile.class,
            optional = false
    )
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, nullable = false, insertable = false,
            foreignKey = @ForeignKey(name = "fk_article_user_id"))
    private UserProfile userProfile;


    @ManyToOne(
            targetEntity = Topic.class,
            optional = false
    )
    @JoinColumn(name = "topic_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_article_topic_id"))
    private Topic topic;

    @OneToMany(
            targetEntity = ArticleTagsRel.class,
            mappedBy = "article",
            cascade = {CascadeType.REMOVE}
    )
    private Set<ArticleTagsRel> tagsRel;

    @OneToMany(
            targetEntity = Comment.class,
            mappedBy = "article",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Comment> comments;

    public Article(CreatedArticle ca, Long aid, Long uid)
    {
        this.id = aid;
        this.user_id = uid;
        this.title = ca.getTitle();
        this.cont = ca.getContent();
        this.views = 0L;
        this.likes = 0L;
        this.createTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
    }
}
