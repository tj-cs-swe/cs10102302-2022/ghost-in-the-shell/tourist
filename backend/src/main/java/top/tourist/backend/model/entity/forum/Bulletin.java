package top.tourist.backend.model.entity.forum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.Account;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bulletin")
public class Bulletin implements Serializable {
    @Id
    private Long id;

    @Column(name = "admin_id", nullable = false)
    private Long adminId;

    @Column(name = "title", length = 255, nullable = false)
    private String title;

    @Column(name = "cont")
    private String cont;

    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

    @ManyToOne(
            targetEntity = Account.class,
            optional = false
    )
    @JoinColumn(name = "admin_id", referencedColumnName = "id", insertable = false, updatable = false, nullable = false,
        foreignKey = @ForeignKey(name = "fk_bulletin_admin_id"))
    private Account adminAccount;
}
