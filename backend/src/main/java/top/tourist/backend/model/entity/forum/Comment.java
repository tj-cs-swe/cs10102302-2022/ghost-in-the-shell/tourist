package top.tourist.backend.model.entity.forum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.vo.CreatedComment;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "comment", indexes = {
        @Index(name = "index_comment_article_id", columnList = "article_id")
})
public class Comment implements Serializable {
    @Id
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "article_id", nullable = false)
    private Long articleId;

    @Column(name = "likes")
    private Long likes;

    @Column(name = "cont", length = 255,nullable = false)
    private String cont;

    @Column(name = "parent_id")
    private Long parentId;

    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private LocalDateTime updateTime;

    @ManyToOne(
            targetEntity = UserProfile.class,
            optional = false
    )
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_comment_user_id"))
    private UserProfile commentOwner;

    @ManyToOne(
            targetEntity = Article.class,
            optional = false
    )
    @JoinColumn(name = "article_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_comment_article_id"))
    private Article article;

    @ManyToOne(
            targetEntity = Comment.class,
            optional = false
    )
    @JoinColumn(name = "parent_id", referencedColumnName = "id", updatable = false, insertable = false,
            foreignKey = @ForeignKey(name = "fk_comment_parent_id"))
    private Comment parentComment;

    @OneToMany(
            targetEntity = Comment.class,
            mappedBy = "parentComment",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Comment> childComment;

    public Comment(CreatedComment cc, Long cid, Long uid)
    {
        this.id = cid;
        this.userId = uid;
        this.articleId = cc.getArticleId();
        this.cont = cc.getContent();
        this.likes = 0L;
        this.createTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
    }
}
