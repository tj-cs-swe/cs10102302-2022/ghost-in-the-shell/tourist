package top.tourist.backend.model.entity.forum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "topic")
public class Topic implements Serializable {
    @Id
    private Long id;

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "admin_id")
    private Long adminId;

    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "click_cnt")
    private Long clickCnt;

    @OneToMany(
            targetEntity = Article.class,
            mappedBy = "topic",
            cascade = {CascadeType.REMOVE}
    )
    private Set<Article> articles;

}
