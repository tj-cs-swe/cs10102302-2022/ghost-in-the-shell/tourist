package top.tourist.backend.model.entity.notice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import top.tourist.backend.model.entity.account.Account;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account_notice", indexes = {
        @Index(name = "index_notice_receiver_id", columnList = "receiver_id")
})
public class AccountNotice implements Serializable {
    @Id
    private Long id;

    @Column(name = "sender_id", nullable = false)
    private Long senderId;

    @Column(name = "receiver_id", nullable = false)
    private Long receiverId;

    @Column(name = "title", nullable = false, length = 255)
    private String title;

    @Column(name = "cont")
    private String cont;

    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "expire_time")
    private Date expireTime;

    @ManyToOne(
            targetEntity = Account.class,
            optional = false
    )
    @JoinColumn(name = "sender_id", referencedColumnName = "id", nullable = false, insertable = false,
            updatable = false, foreignKey = @ForeignKey(name = "fk_notice_sender_id"))
    private Account sendAccount;

    @ManyToOne(
            targetEntity = Account.class,
            optional = false
    )
    @JoinColumn(name = "receiver_id", referencedColumnName = "id", nullable = false, insertable = false,
            updatable = false, foreignKey = @ForeignKey(name = "fk_notice_receiver_id"))
    private Account receiveAccount;


}
