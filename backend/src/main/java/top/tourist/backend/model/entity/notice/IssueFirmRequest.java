package top.tourist.backend.model.entity.notice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.FirmProfile;
import top.tourist.backend.model.enumerate.EFirmRequest;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "issue_firm_request")
public class IssueFirmRequest implements Serializable {
    @Id
    private Long id;

    @Column(name = "firm_id", nullable = false)
    private Long firmId;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private EFirmRequest type;

    @Column(name = "message", length = 1023)
    private String message;

    @Column(name = "audit", nullable = false)
    private Short audit;

    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

    @ManyToOne(
            targetEntity = FirmProfile.class,
            optional = false
    )
    @JoinColumn(name = "firm_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_issue_firm_id"))
    private FirmProfile firm;

    public IssueFirmRequest(Long idIn, Long firmIdIn, EFirmRequest typeIn, String msgIn) {
        this.id = idIn;
        this.firmId = firmIdIn;
        this.type = typeIn;
        this.message = msgIn;
        this.audit = 0;
        this.createTime = new Date(0);
        this.updateTime = new Date(0);
    }
}
