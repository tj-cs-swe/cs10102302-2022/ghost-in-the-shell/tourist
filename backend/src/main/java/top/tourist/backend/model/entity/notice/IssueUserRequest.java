package top.tourist.backend.model.entity.notice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.enumerate.EFirmRequest;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "issue_user_request")
public class IssueUserRequest implements Serializable {
    @Id
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private EFirmRequest type;

    @Column(name = "message", length = 1023)
    private String message;

    @Column(name = "audit", nullable = false)
    private Short audit;

    @CreatedDate
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time", nullable = false)
    private Date updateTime;

    @ManyToOne(
            targetEntity = UserProfile.class,
            optional = false
    )
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, insertable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_issue_user_id"))
    private UserProfile issueOwner;

}
