package top.tourist.backend.model.entity.tag;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.unionkey.TagsRelUnionKey;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@NoArgsConstructor
@Table(name = "account_tags_rel")
@IdClass(TagsRelUnionKey.class)
public class AccountTagsRel implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Id
    @Column(name = "tag_id", nullable = false)
    private Long tagId;

    @Column(name = "is_admin_add", nullable = false)
    private boolean isAdminAdd;

    public AccountTagsRel(Long accountIdIn, Long tagIdIn) {
        this.id = accountIdIn;
        this.tagId = tagIdIn;
        this.isAdminAdd = false;
    }

    public AccountTagsRel(Long accountIdIn, Long tagIdIn, boolean isAdminAddIn) {
        this.id = accountIdIn;
        this.tagId = tagIdIn;
        this.isAdminAdd = isAdminAddIn;
    }

    @ManyToOne(
            targetEntity = Account.class,
            optional = false
    )
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_tags_rel_auth_id"))
    private Account account;

    @ManyToOne(
            targetEntity = Tags.class,
            optional = false
    )

    @JoinColumn(name = "tag_id", referencedColumnName = "id", insertable = false, updatable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_tags_rel_tag_id"))
    private Tags tags;

}
