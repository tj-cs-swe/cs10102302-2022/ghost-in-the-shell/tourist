package top.tourist.backend.model.entity.tag;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.model.entity.forum.Article;
import top.tourist.backend.model.unionkey.ArticleTagsRelUnionKey;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "article_tags_rel")
@IdClass(ArticleTagsRelUnionKey.class)
public class ArticleTagsRel implements Serializable {
    @Id
    private Long id;

    @Id
    @Column(name = "tag_id", nullable = false)
    private Long tagId;

    @ManyToOne(
            targetEntity = Article.class,
            optional = false
    )
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_article_id"))
    private Article article;

    @ManyToOne(
            targetEntity = Tags.class,
            optional = false
    )
    @JoinColumn(name = "tag_id", referencedColumnName = "id", insertable = false, updatable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_article_tag_id"))
    private Tags tags;
}
