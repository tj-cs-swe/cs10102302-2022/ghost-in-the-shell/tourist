package top.tourist.backend.model.entity.tag;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.utils.data.SnowFlakeUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tags")
public class Tags implements Serializable {
    @Id
    private Long id;

    @Column(name = "name", nullable = false, length = 63)
    private String name;

    @Column(name = "roles", nullable = false)
    private Integer roles;

    @Column(name = "heat")
    private Integer heat;

    @OneToMany(
            targetEntity = AccountTagsRel.class,
            mappedBy = "tags",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST}
    )
    private Set<AccountTagsRel> accountTagsRel;

    @Transient
    private static final SnowFlakeUtils idGen = new SnowFlakeUtils(2, 0);

    public Tags(String nameIn) {
        this.id = idGen.NextId();
        this.name = nameIn;
    }

    public Tags(String nameIn, Integer rolesIn)
    {
        this.id = idGen.NextId();
        this.name = nameIn;
        this.roles = rolesIn;
    }

    @OneToMany(
            targetEntity = ArticleTagsRel.class,
            mappedBy = "tags",
            cascade = {CascadeType.REMOVE}
    )
    private Set<ArticleTagsRel> articleTagsRel;
}
