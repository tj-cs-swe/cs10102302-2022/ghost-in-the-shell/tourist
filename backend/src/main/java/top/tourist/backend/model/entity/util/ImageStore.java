package top.tourist.backend.model.entity.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.commerce.ProductImage;
import top.tourist.backend.model.enumerate.EImagePrivacy;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "image_store")
public class ImageStore implements Serializable {
    @Id
    private Long id;

    @Column(name = "path", length = 1023, nullable = false)
    private String path;

    @Column(name = "upload_time", nullable = false)
    private Date uploadTime;

    @Column(name = "upload_user_id", nullable = false)
    private Long uploadUserId;

    @Enumerated(EnumType.STRING)
    @Column(name = "privacy", nullable = false)
    private EImagePrivacy privacy;

    @ManyToOne(
            targetEntity = Account.class,
            optional = false
    )
    @JoinColumn(name = "upload_user_id", referencedColumnName = "id", insertable = false, updatable = false, nullable = false,
            foreignKey = @ForeignKey(name = "fk_image_upload_user_id"))
    private Account uploadAccount;

    @OneToMany(
            targetEntity = ProductImage.class,
            mappedBy = "imageStore",
            cascade = {CascadeType.REMOVE}
    )
    private Set<ProductImage> productImages;
}
