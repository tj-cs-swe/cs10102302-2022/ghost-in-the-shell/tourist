package top.tourist.backend.model.entity.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "nation_district", indexes = {
        @Index(name = "index_district_name", columnList = "name")
})
public class NationDistrict implements Serializable {
    @Id
    private Long id;

    @Column(name = "name", length = 127, nullable = false)
    private String name;
}
