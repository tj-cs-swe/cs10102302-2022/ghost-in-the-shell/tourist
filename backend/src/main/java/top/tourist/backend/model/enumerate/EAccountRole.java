package top.tourist.backend.model.enumerate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public enum EAccountRole implements Serializable {

    USER(1, "USER"),
    FIRM(2, "FIRM"),
    ADMIN(3, "ADMIN");

    private final Integer code;
    private final String role;
    private static final ArrayList<String> roleList =
            new ArrayList<String>(List.of("USER", "FIRM", "ADMIN"));

    EAccountRole(Integer code, String role) {
        this.code = code;
        this.role = role;
    }

    public Integer getCode() {
        return code;
    }

    public String getRole() {
        return role;
    }

    public static EAccountRole stringConverter(String str) {
        switch (str) {
            case "USER":
                return EAccountRole.USER;
            case "FIRM":
                return EAccountRole.FIRM;
            case "ADMIN":
                return EAccountRole.ADMIN;
            default:
                return null;
        }
    }

    public static boolean isLegalRole(String role) {
        return roleList.contains(role);
    }
}
