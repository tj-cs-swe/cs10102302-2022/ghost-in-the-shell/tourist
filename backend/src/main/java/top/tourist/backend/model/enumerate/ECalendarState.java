package top.tourist.backend.model.enumerate;

import java.io.Serializable;

public enum ECalendarState implements Serializable {
    BUSY(1, "BUSY"),
    FREE(2, "FREE"),
    ;

    private final Integer code;
    private final String status;

    ECalendarState(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public static ECalendarState stringConverter(String str) {
        switch (str) {
            case "BUSY":
                return ECalendarState.BUSY;
            case "FREE":
                return ECalendarState.FREE;
            default:
                throw new IllegalArgumentException();
        }
    }
}
