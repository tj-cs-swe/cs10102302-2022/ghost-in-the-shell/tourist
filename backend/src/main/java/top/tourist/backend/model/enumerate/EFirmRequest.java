package top.tourist.backend.model.enumerate;

import java.io.Serializable;

public enum EFirmRequest implements Serializable {

    JOIN(1, "join"),
    CONTRACT(2, "contract"),
    SETTLE(3, "settle"),
    PROMOTE(4, "promote"),
    QUIT(5, "quit");

    private final Integer code;
    private final String status;

    EFirmRequest(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }
}
