package top.tourist.backend.model.enumerate;


import java.io.Serializable;

public enum EImagePrivacy implements Serializable {

    PRIVATE(1, "private"),
    PUBLIC(2, "public");

    private final Integer code;
    private final String status;

    EImagePrivacy(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }
}
