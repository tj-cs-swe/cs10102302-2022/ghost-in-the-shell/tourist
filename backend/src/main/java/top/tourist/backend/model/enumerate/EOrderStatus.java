package top.tourist.backend.model.enumerate;

import java.io.Serializable;

public enum EOrderStatus implements Serializable {

    CHECKING(1, "checking"),
    NORMAL(2, "normal"),
    USED(3, "used"),
    REFUND(4, "refund"),
    CANCEL(5, "cancel");

    private final Integer code;
    private final String status;

    EOrderStatus(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }
}
