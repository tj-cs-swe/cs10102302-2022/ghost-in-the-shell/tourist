package top.tourist.backend.model.enumerate;

import java.io.Serializable;

public enum EUserRequest implements Serializable {

    ARTICLE(1, "article"),
    APPEAL(2, "appeal"),
    REFUND(3, "refund");
    private final Integer code;
    private final String status;

    EUserRequest(Integer code, String status) {
        this.code = code;
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }
}
