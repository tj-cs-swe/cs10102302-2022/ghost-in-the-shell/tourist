package top.tourist.backend.model.factory;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.enumerate.EAccountRole;
import top.tourist.backend.model.vo.SignUpVo;
import top.tourist.backend.utils.data.PasswordEncoderUtils;
import top.tourist.backend.utils.data.SnowFlakeUtils;
import top.tourist.backend.utils.general.TimeUtils;

@Service
public class AccountFactory {
    private static final SnowFlakeUtils authIdGen = new SnowFlakeUtils(0, 0);

    public static Account Create(SignUpVo signUpVo) {
        return new Account(authIdGen.NextId(), signUpVo.getName(),
                PasswordEncoderUtils.getEncoder().encode(signUpVo.getPassword()),
                EAccountRole.stringConverter(signUpVo.getRole()), false,
                TimeUtils.getTime());
    }
}
