package top.tourist.backend.model.factory;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.calendar.CalendarTodo;
import top.tourist.backend.model.enumerate.ECalendarState;
import top.tourist.backend.model.vo.CalendarTodoVo;
import top.tourist.backend.utils.data.SnowFlakeUtils;

@Service
public class CalendarTodoFactory {
    private static final SnowFlakeUtils idGen = new SnowFlakeUtils(0, 2);

    public static CalendarTodo Create(CalendarTodoVo vo) {
        return new CalendarTodo(
                idGen.NextId(), 0L, vo.getName(),
                vo.getLocation(), ECalendarState.stringConverter(vo.getState()),
                vo.getStartTime(), vo.getEndTime(), vo.isPrivateControl(), vo.isAllDay());
    }

}
