package top.tourist.backend.model.factory;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.FirmProfile;
import top.tourist.backend.repo.FirmProfileRepo;

@Service
public class FirmProfileFactory {
    private final FirmProfileRepo firmProfileRepo;

    public FirmProfileFactory(FirmProfileRepo firmProfileRepo) {
        this.firmProfileRepo = firmProfileRepo;
    }

    public FirmProfile CreateAtSignUp(Account account) {
        return new FirmProfile(account.getId(), account.getName());
    }
}
