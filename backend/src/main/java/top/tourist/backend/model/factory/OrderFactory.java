package top.tourist.backend.model.factory;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.commerce.UserOrder;
import top.tourist.backend.model.vo.TicketsNumVo;
import top.tourist.backend.utils.data.SnowFlakeUtils;

@Service
public class OrderFactory {
    private static final SnowFlakeUtils idGen = new SnowFlakeUtils(1, 1);

    public static UserOrder create(TicketsNumVo ticketsNumVo, Long userId, Double price) {
        return new UserOrder(idGen.NextId(), userId, ticketsNumVo.getId(), ticketsNumVo.getNum(), price);
    }

    public static UserOrder create(Long productId, Integer productNum, Long userId, Double price) {
        return new UserOrder(idGen.NextId(), userId, productId, productNum, price);
    }

}
