package top.tourist.backend.model.factory;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.AuthSession;
import top.tourist.backend.utils.data.SnowFlakeUtils;
import top.tourist.backend.utils.general.TimeUtils;

import java.time.LocalDateTime;

@Service
public class SessionFactory {
    private static final SnowFlakeUtils sessionIdGen = new SnowFlakeUtils(0, 1);

    public static AuthSession create(Long accountId, String secret) {
        return new AuthSession(sessionIdGen.NextId(), accountId, secret,
                TimeUtils.getSessionExpireTime(), false);
    }

    public static AuthSession create(Long accountId, String secret, LocalDateTime expireTime, boolean closeStatus) {
        return new AuthSession(sessionIdGen.NextId(), accountId, secret, expireTime, closeStatus);
    }
}
