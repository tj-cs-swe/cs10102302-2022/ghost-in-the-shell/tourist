package top.tourist.backend.model.factory;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.repo.AccountRepo;

@Service
public class UserProfileFactory {

    private final AccountRepo accountRepo;

    public UserProfileFactory(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    public UserProfile CreateAtSignUp(Account account) {
        return new UserProfile(
                account.getId(), account.getName(), (short) 0,
                "", "", "");
    }
}
