package top.tourist.backend.model.unionkey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ArticleTagsRelUnionKey implements Serializable {

    private Long id;
    private Long tagId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleTagsRelUnionKey that = (ArticleTagsRelUnionKey) o;
        return Objects.equals(id, that.id) && Objects.equals(tagId, that.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tagId);
    }

    @Override
    public String toString() {
        return "ArticleTagsRelUnionKey{" +
                "id=" + id +
                ", tagId=" + tagId +
                '}';
    }
}
