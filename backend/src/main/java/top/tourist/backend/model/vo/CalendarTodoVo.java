package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import top.tourist.backend.model.dto.CalendarTodoDto;
import top.tourist.backend.model.enumerate.ECalendarState;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class CalendarTodoVo implements Serializable {
    private String id;
    private String name;
    private String location;
    private String state;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private boolean privateControl;
    private boolean allDay;

    public CalendarTodoDto toDto() {
        ECalendarState eState = ECalendarState.stringConverter(state);
        return new CalendarTodoDto(
                Long.parseLong(id), name, location, eState,
                startTime, endTime, privateControl, allDay);
    }
}
