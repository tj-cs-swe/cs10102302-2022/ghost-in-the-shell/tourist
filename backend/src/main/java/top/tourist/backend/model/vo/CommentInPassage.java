package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.tourist.backend.model.entity.forum.Comment;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentInPassage implements Serializable {

    private Long id;
    private String userName;
    private Long likes;
    private String cont;
    private Long parentId;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    public CommentInPassage(Comment comment,String nickName)
    {
        id = comment.getId();
        userName = nickName;
        likes = comment.getLikes();
        cont = comment.getCont();
        parentId = comment.getParentId();
        createTime = comment.getCreateTime();
        updateTime = comment.getUpdateTime();
    }
}
