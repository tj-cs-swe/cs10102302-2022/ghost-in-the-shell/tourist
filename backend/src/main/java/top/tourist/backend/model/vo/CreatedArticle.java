package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatedArticle implements Serializable {

    private String title;
    private String content;
    //private Date time;
}
