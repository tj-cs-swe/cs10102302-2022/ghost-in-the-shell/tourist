package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatedComment implements Serializable {

    private Long articleId;
    private String content;
    //private Date time;
}
