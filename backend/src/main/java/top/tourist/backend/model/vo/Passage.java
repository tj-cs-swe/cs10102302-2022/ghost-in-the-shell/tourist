package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.tourist.backend.model.entity.forum.Article;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Passage implements Serializable {
    private Long id;
    private String nickName;
    private String topicName;
    private String title;
    private Long views;
    private Long likes;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    public Passage(Article a, String n){
        id = a.getId();
        nickName = n;
        topicName = null;
        title = a.getTitle();
        views = a.getViews();
        likes = a.getLikes();
        createTime = a.getCreateTime();
        updateTime =a.getUpdateTime();
    }
}
