package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.tourist.backend.model.entity.forum.Article;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassageWithComment implements Serializable {
    private Passage passage;
    private String content;
    private List<CommentInPassage> commentList;

    public PassageWithComment(Article article, String nickName)
    {
        passage = new Passage(article, nickName);
        content = article.getCont();
        commentList = new ArrayList<>();
    }

    public void AddComment(CommentInPassage com)
    {
        commentList.add(com);
    }
}
