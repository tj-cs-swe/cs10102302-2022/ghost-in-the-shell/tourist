package top.tourist.backend.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@Schema(description = "用户登录时发送的请求体")
public class SignInVo implements Serializable {

    @NotNull(message = "必须具有用户名")
    @Schema(description = "用户名", example = "jackson", minLength = 1, maxLength = 256, required = true)
    private String name;

    @NotNull(message = "必须具有密码")
    @Schema(description = "密码", minLength = 6, maxLength = 512, required = true)
    private String password;
}
