package top.tourist.backend.model.vo;

import lombok.Data;
import top.tourist.backend.model.entity.commerce.Product;
import top.tourist.backend.model.entity.commerce.UserOrder;

@Data
public class TicketInfoVo {
    private Long id;
    private String name;
    private Integer num;
    private Double price;
    private String address;
    private String intro;
    private String status;

    public TicketInfoVo(UserOrder order, Product product) {
        this.id = order.getId();
        this.name = product.getName();
        this.price = order.getPrice();
        this.num = order.getProductNum();
        this.address = product.getAddress();
        this.intro = (product.getIntro() == null) ? "" : product.getIntro();
        this.status = order.getStatus().name();
    }
}
