package top.tourist.backend.model.vo;

import lombok.Data;

import java.util.Map;

@Data
public class TicketsNumVo {
    // product id || order id
    private Long id;
    private Integer num;

    public TicketsNumVo(Map<String, String> map) {
        String id = map.get("id");
        String num = map.get("num");
        if (id == null || num == null) {
            throw new IllegalArgumentException();
        }
        this.id = Long.parseLong(id);
        this.num = Integer.parseInt(num);
    }

    public TicketsNumVo(String id, String num) {
        this.id = Long.parseLong(id);
        this.num = Integer.parseInt(num);
    }
}
