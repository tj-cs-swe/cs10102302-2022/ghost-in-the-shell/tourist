package top.tourist.backend.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class UserProfileVo implements Serializable {
    private String nickname;
    private String motto;
    private String email;
}
