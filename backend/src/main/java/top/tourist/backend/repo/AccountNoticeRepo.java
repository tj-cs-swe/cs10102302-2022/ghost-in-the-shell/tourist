package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.notice.AccountNotice;

public interface AccountNoticeRepo extends JpaRepository<AccountNotice, Long>,
        JpaSpecificationExecutor<AccountNotice> {
}