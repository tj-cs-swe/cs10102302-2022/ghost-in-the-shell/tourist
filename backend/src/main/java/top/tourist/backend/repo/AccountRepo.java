package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.enumerate.EAccountRole;

import java.time.LocalDateTime;
import java.util.List;

@Transactional
public interface AccountRepo extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {

    @Query("select a from Account a where a.name = ?1")
    Account findByName(String name);

    @Modifying
    @Query("delete from Account a where a.name = ?1")
    void deleteByNameEquals(@Nullable String name);

    @Modifying
    @Query("update Account a set a.signinTime = ?2 where a.id = ?1")
    int updateSigninTimeById(@NonNull Long id, LocalDateTime signinTime);


    @Modifying
    @Query("update Account a set a.disableStatus = ?2, a.disableTime = ?3 where a.id = ?1")
    int updateDisableStatusById(@NonNull Long id,
                                @NonNull boolean disableStatus, @NonNull LocalDateTime disableTime);

    List<Account> findAccountsByRoles(EAccountRole roles);
}
