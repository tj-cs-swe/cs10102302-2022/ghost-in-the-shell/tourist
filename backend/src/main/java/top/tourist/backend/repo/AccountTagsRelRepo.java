package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.tag.AccountTagsRel;
import top.tourist.backend.model.unionkey.TagsRelUnionKey;

import java.util.List;

public interface AccountTagsRelRepo extends JpaRepository<AccountTagsRel, TagsRelUnionKey>, JpaSpecificationExecutor<AccountTagsRel> {
    @Transactional
    @Modifying
    @Query("delete from AccountTagsRel a where a.id = ?1 and a.tagId = ?2")
    int deleteByAccountIdAndTagId(@NonNull Long id, @NonNull Long tagId);

    @Query("select a from AccountTagsRel a where a.id = ?1 and a.isAdminAdd = ?2 order by a.tagId")
    List<AccountTagsRel> findByIdOrderByTagIdAsc(@NonNull Long id, @NonNull boolean isAdminAdd);


}