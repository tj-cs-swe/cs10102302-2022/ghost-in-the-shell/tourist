package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.forum.Article;
import top.tourist.backend.model.entity.tag.Tags;

public interface ArticleRepo extends JpaRepository<Article, Long>, JpaSpecificationExecutor<Article> {
    @Query("select count(a) from Article a where a.user_id = ?1")
    long countByUserId(@NonNull Long user_id);

    @Query("select a from Article a where a.id = ?1")
    Article findByIdEquals(@NonNull Long id);

    @Transactional
    @Modifying
    @Query("delete from Article a where a.id = ?1")
    void deleteByIdEquals(@NonNull Long id);
}