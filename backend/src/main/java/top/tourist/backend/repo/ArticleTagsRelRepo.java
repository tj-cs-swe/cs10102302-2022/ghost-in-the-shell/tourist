package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.tag.ArticleTagsRel;
import top.tourist.backend.model.unionkey.ArticleTagsRelUnionKey;

public interface ArticleTagsRelRepo extends JpaRepository<ArticleTagsRel, ArticleTagsRelUnionKey>,
        JpaSpecificationExecutor<ArticleTagsRel> {
}