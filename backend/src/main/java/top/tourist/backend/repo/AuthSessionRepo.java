package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.account.AuthSession;

import java.time.LocalDateTime;
import java.util.Optional;

@Transactional
public interface AuthSessionRepo extends JpaRepository<AuthSession, Long>, JpaSpecificationExecutor<AuthSession> {

    @Query("select a from AuthSession a where a.accountId = ?1")
    Optional<AuthSession> findByAccountId(@NonNull Long accountId);

    @Modifying
    @Query("update AuthSession a set a.expireTime = ?2 where a.id = ?1")
    int updateExpireTimeById(@Nullable Long id, @NonNull LocalDateTime expireTime);

    @Modifying
    @Query("update AuthSession a set a.closeStatus = ?2 where a.id = ?1")
    int updateCloseStatusById(@NonNull Long id, @NonNull boolean closeStatus);

    @Modifying
    @Transactional
    @Query("delete from AuthSession a where a.accountId = ?1")
    int deleteByAccountId(@NonNull Long accountId);

    @Modifying
    @Query("update AuthSession a set a.secret = ?2, a.expireTime = ?3 where a.accountId = ?1")
    int updateSecretAndExpireTimeByAccountId(@NonNull Long id,
                                             @NonNull String secret, @NonNull LocalDateTime expireTime);

    @Modifying
    @Query("update AuthSession a set a.secret = ?2, a.expireTime = ?3, a.closeStatus = ?4 where a.id = ?1")
    int updateSessionSignInStatusById(@NonNull Long id, @NonNull String secret,
                                      @NonNull LocalDateTime expireTime,
                                      @NonNull boolean closeStatus);


}