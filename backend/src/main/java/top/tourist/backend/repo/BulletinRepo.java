package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.forum.Bulletin;

public interface BulletinRepo extends JpaRepository<Bulletin, Long>, JpaSpecificationExecutor<Bulletin> {
}