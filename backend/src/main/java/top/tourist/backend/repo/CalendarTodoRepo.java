package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.calendar.CalendarTodo;
import top.tourist.backend.model.enumerate.ECalendarState;

import java.time.LocalDateTime;
import java.util.List;

public interface CalendarTodoRepo extends JpaRepository<CalendarTodo, Long>, JpaSpecificationExecutor<CalendarTodo> {
    @Query("select c from CalendarTodo c where c.userId = ?1 order by c.startTime")
    List<CalendarTodo> findByUserId(@NonNull Long userId);

    @Transactional
    @Modifying
    @Query("update CalendarTodo c set c.name = ?1, c.location = ?2, c.state = ?3, c.startTime = ?4, c.endTime = ?5, c.privateControl = ?6, c.allDay = ?7 " +
            "where c.id = ?8")
    int updateById(@NonNull String name, @Nullable String location,
                   @NonNull ECalendarState state, @NonNull LocalDateTime startTime,
                   @NonNull LocalDateTime endTime, @NonNull boolean isPrivate,
                   @NonNull boolean isAllDay, @NonNull Long id);

    @Transactional
    @Modifying
    @Query("delete from CalendarTodo c where c.id = ?1")
    int deleteByIdEquals(@NonNull Long id);


}