package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.forum.Comment;

import java.util.List;

public interface CommentRepo extends JpaRepository<Comment, Long>, JpaSpecificationExecutor<Comment> {
    @Query("select count(c) from Comment c where c.userId = ?1")
    long countByUserId(@NonNull Long userId);

    @Query("select count(c) from Comment c where c.articleId = ?1")
    long countByArticleId(@NonNull Long userId);

    @Query("select c from Comment c")
    List<Comment> findAllPlease();

    @Transactional
    @Modifying
    @Query("delete from Comment c where c.id = ?1")
    void deleteByIdEquals(@NonNull Long id);
}