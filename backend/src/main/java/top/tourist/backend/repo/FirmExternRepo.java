package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.account.FirmExtern;

public interface FirmExternRepo extends JpaRepository<FirmExtern, Long>, JpaSpecificationExecutor<FirmExtern> {
}