package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.account.FirmProfile;

public interface FirmProfileRepo extends JpaRepository<FirmProfile, Long>, JpaSpecificationExecutor<FirmProfile> {
}