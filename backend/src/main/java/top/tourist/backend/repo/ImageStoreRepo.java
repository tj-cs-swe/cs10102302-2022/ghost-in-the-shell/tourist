package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.util.ImageStore;

public interface ImageStoreRepo extends JpaRepository<ImageStore, Long>, JpaSpecificationExecutor<ImageStore> {
}