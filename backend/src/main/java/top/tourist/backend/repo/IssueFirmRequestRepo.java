package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.notice.IssueFirmRequest;

public interface IssueFirmRequestRepo extends JpaRepository<IssueFirmRequest, Long>, JpaSpecificationExecutor<IssueFirmRequest> {
}