package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.notice.IssueUserRequest;

public interface IssueUserRequestRepo extends JpaRepository<IssueUserRequest, Long>, JpaSpecificationExecutor<IssueUserRequest> {
}