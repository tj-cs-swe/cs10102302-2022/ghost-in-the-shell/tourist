package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.util.NationDistrict;

public interface NationDistrictRepo extends JpaRepository<NationDistrict, Long>, JpaSpecificationExecutor<NationDistrict> {
}