package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.commerce.ProductImage;

public interface ProductImageRepo extends JpaRepository<ProductImage, Long>, JpaSpecificationExecutor<ProductImage> {
}