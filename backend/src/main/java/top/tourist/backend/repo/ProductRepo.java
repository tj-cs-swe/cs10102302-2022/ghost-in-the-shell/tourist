package top.tourist.backend.repo;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import top.tourist.backend.model.entity.commerce.Product;

import javax.transaction.Transactional;
import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
    /**
     * 通过name查找商品
     * @param productName 商品名称
     * @return 查询到的商品列表
     */
    List<Product> findProductsByName(String productName);

    // 此处弃用原因是在执行第二次查询时会抛异常，据悉是JPA版本问题
    @Deprecated
    List<Product> findProductsByNameContains(String name);

    @Query("select p from Product p where p.name like concat('%', ?1, '%')")
    List<Product> findByNameContains(String name, Pageable pageable);


    @Query(value = "select * from product order by rand() limit ?1 ", nativeQuery = true)
    List<Product> getRandomProducts(int num);

    @Query("SELECT p FROM Product p WHERE p.name=:productName")
    List<Product> findProductByName(String productName);

    @Query("select (count(p) > 0) from Product p where p.id = ?1")
    boolean existsById(@NonNull Long id);

    @Query("select p from Product p where p.id = ?1")
    Product findByIdEquals(@NonNull Long id);

    @Transactional
    @Modifying
    @Query("update Product p set p.remain = ?1 where p.id = ?2")
    int updateRemainById(Integer remain, Long id);


}