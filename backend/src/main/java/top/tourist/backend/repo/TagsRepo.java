package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import top.tourist.backend.model.entity.tag.Tags;

import java.util.Collection;
import java.util.List;

public interface TagsRepo extends JpaRepository<Tags, Long>, JpaSpecificationExecutor<Tags> {
    @Query("select (count(t) > 0) from Tags t where t.name = ?1")
    boolean existsByName(@NonNull String name);

    @Query("select t from Tags t where t.name = ?1")
    Tags findByNameEquals(@NonNull String name);

//    @Query("select t from Tags t where t.name = ?1 and t.roles = ?2")
//    Tags findByNameRoles(@NonNull String name, @NonNull Integer roles);

    @Query("select t.name from Tags t where t.id in ?1")
    List<String> findNameById(@NonNull Collection<Long> ids);


}