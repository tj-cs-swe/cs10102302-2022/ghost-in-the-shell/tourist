package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.tourist.backend.model.entity.forum.Topic;

public interface TopicRepo extends JpaRepository<Topic, Long>, JpaSpecificationExecutor<Topic> {
}