package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.commerce.UserOrder;
import top.tourist.backend.model.enumerate.EOrderStatus;

import java.time.LocalDateTime;
import java.util.List;

public interface UserOrderRepo extends JpaRepository<UserOrder, Long>, JpaSpecificationExecutor<UserOrder> {
    @Query("select u from UserOrder u where u.userId = ?1 order by u.createTime DESC")
    List<UserOrder> findByUserIdSortCreateTimeDesc(@NonNull Long userId);

    @Query("select u from UserOrder u where u.id = ?1")
    UserOrder findByIdEquals(@NonNull Long id);

    @Transactional
    @Modifying
    @Query("update UserOrder u set u.productNum = ?1, u.price = ?2 where u.id = ?3")
    int updateNumAndPriceById(@NonNull Integer productNum, @NonNull Double price, @NonNull Long id);

    @Transactional
    @Modifying
    @Query("update UserOrder u set u.status = ?1 where u.id = ?2")
    int updateStatusByIdEquals(@NonNull EOrderStatus status, @NonNull Long id);

    @Transactional
    @Modifying
    @Query("update UserOrder u set u.status = ?1, u.updateTime = ?2 where u.id = ?3")
    int updateStatusAndUpdateTimeById(@NonNull EOrderStatus status, @NonNull LocalDateTime updateTime, @NonNull Long id);


    @Transactional
    @Modifying
    @Query("update UserOrder u set u.productNum = ?1, u.price = ?2 " +
            "where u.userId = ?3 and u.productId = ?4 and u.status = ?5")
    int updateNumAndPriceBySameRefundStatus(@NonNull Integer productNum, @NonNull Double price, Long userId, Long productId, EOrderStatus status);


}