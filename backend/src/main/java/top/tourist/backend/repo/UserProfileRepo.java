package top.tourist.backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.account.UserProfile;

import java.time.LocalDateTime;

public interface UserProfileRepo extends JpaRepository<UserProfile, Long>, JpaSpecificationExecutor<UserProfile> {
    @Transactional
    @Modifying
    @Query("update UserProfile u set u.nickname = ?1, u.email = ?2, u.motto = ?3, u.updateTime = ?4 where u.id = ?5")
    int updateInfoById(@NonNull String nickname, @NonNull String email, @NonNull String motto, @NonNull LocalDateTime updateTime, @NonNull Long id);

    @Query("select u from UserProfile u where u.id = ?1")
    UserProfile findByIdEquals(@NonNull Long id);
}