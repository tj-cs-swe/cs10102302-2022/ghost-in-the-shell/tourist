package top.tourist.backend.service.account;

import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.enumerate.EAccountRole;
import top.tourist.backend.repo.AccountRepo;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class JpaAccountService {
    @Resource
    private AccountRepo accountRepo;

    public List<Account> findAccountsByRoles(EAccountRole roles) {
        return accountRepo.findAccountsByRoles(roles);
    }

    public Account findAccountByName(String name) {
        return accountRepo.findByName(name);
    }

    public void updateAccount(Account account) {
        accountRepo.save(account);
    }

    public void deleteAccount(Account account) {
        accountRepo.delete(account);
    }

    public Account findAccountById(Long id) {
        Optional<Account> optionalAccount = accountRepo.findById(id);
        if (optionalAccount.isEmpty()) {
            return null;
        }
        else {
            return optionalAccount.get();
        }
    }
}
