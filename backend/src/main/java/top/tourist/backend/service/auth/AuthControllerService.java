package top.tourist.backend.service.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.dto.AccountDto;
import top.tourist.backend.model.dto.mapper.AccountMapper;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.AuthSession;
import top.tourist.backend.model.entity.account.FirmProfile;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.enumerate.EAccountRole;
import top.tourist.backend.model.factory.AccountFactory;
import top.tourist.backend.model.factory.FirmProfileFactory;
import top.tourist.backend.model.factory.SessionFactory;
import top.tourist.backend.model.factory.UserProfileFactory;
import top.tourist.backend.model.vo.SignUpVo;
import top.tourist.backend.model.vo.TokenVo;
import top.tourist.backend.repo.AccountRepo;
import top.tourist.backend.repo.AuthSessionRepo;
import top.tourist.backend.repo.FirmProfileRepo;
import top.tourist.backend.repo.UserProfileRepo;
import top.tourist.backend.utils.general.ExMsgUtils;
import top.tourist.backend.utils.general.TimeUtils;
import top.tourist.backend.utils.json.JwtUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class AuthControllerService {

    private final AccountMapper accountMapper;
    private final AccountRepo accountRepo;
    private final AuthSessionRepo sessionRepo;
    private final UserProfileRepo userProfileRepo;
    private final FirmProfileRepo firmProfileRepo;
    private final UserProfileFactory userProfileFactory;
    private final FirmProfileFactory firmProfileFactory;

    public AuthControllerService(AccountRepo accountRepo, AuthSessionRepo sessionRepo, AccountMapper accountMapper,
                                 UserProfileRepo userProfileRepo, UserProfileFactory userProfileFactory,
                                 FirmProfileRepo firmProfileRepo, FirmProfileFactory firmProfileFactory) {
        this.accountRepo = accountRepo;
        this.sessionRepo = sessionRepo;
        this.accountMapper = accountMapper;
        this.userProfileRepo = userProfileRepo;
        this.firmProfileRepo = firmProfileRepo;
        this.userProfileFactory = userProfileFactory;
        this.firmProfileFactory = firmProfileFactory;
    }


    public Result<AccountDto> signUpService(SignUpVo signUpVo) {
        if (!(EAccountRole.isLegalRole(signUpVo.getRole()))) {
            return Result.resultFactory(EResult.BAD_REQUEST, null);
        }
        Account accountDup = accountRepo.findByName(signUpVo.getName());
        if (accountDup != null) {
            return Result.resultFactory(EResult.DATA_DUPLICATE, null);
        }

        Account account = AccountFactory.Create(signUpVo);
        accountRepo.save(account);
        if (account.getRoles() == EAccountRole.USER) {
            UserProfile profile = userProfileFactory.CreateAtSignUp(account);
            userProfileRepo.save(profile);
        } else if (account.getRoles() == EAccountRole.FIRM) {
            FirmProfile profile = firmProfileFactory.CreateAtSignUp(account);
            firmProfileRepo.save(profile);
        }
        return Result.success(signInService(account.getName()));
    }

    private boolean isRefreshJwt(Account account, Optional<AuthSession> optional, LocalDateTime timeNow) {
        if (optional.isEmpty() || optional.get().isCloseStatus())
            return true;
        else
            return optional.get().getExpireTime().isBefore(timeNow) ||
                    TimeUtils.getJwtExpireTime(account.getSigninTime()).isBefore(timeNow);
    }

    public AccountDto signInService(String username) {
        LocalDateTime time = TimeUtils.getTime();
        Account account = accountRepo.findByName(username);

        if (account == null) {
            String nameNotFoundStr = "Username do not exist";
            log.debug(ExMsgUtils.GetMessage(nameNotFoundStr));
            throw new UsernameNotFoundException(nameNotFoundStr);
        }

        if(account.isDisableStatus()) {
            log.debug(ExMsgUtils.GetMessage("Account is locked"));
            throw new LockedException("");
        }

        AccountDto accountDto = accountMapper.toAccountDto(account);

        Optional<AuthSession> optional = sessionRepo.findByAccountId(account.getId());
        if (optional.isPresent() && !isRefreshJwt(account, optional, time)) {
            AuthSession session = optional.get();
            // Jwt keep same
            accountDto.setJwt(createSignInJwt(account.getId(), session.getId(),
                    session.getSecret(), TimeUtils.getJwtExpireTime(account.getSigninTime())));
        } else {
            // Jwt or Session expired , create new Jwt
            accountRepo.updateSigninTimeById(account.getId(), time);
            String secret = BCrypt.gensalt();
            AuthSession session = signInUpdateSession(account, optional, secret, time);
            accountDto.setJwt(createSignInJwt(account.getId(),
                    session.getId(), secret, TimeUtils.getJwtExpireTime(time)));
            accountDto.setSigninTime(time);
        }
        return accountDto;
    }

    private AuthSession signInUpdateSession(Account account, Optional<AuthSession> optional,
                                            String secret, LocalDateTime time) {
        if (optional.isEmpty()) {
            AuthSession authSession = SessionFactory.create(account.getId(), secret);
            sessionRepo.save(authSession);
            return authSession;
        } else {
            LocalDateTime expireTime = TimeUtils.getSessionExpireTime(time);
            AuthSession session = optional.get();
            long accountId = session.getAccountId();
            ClearSession(accountId);

            session = SessionFactory.create(accountId, secret, expireTime, false);
            sessionRepo.save(session);
            return session;
        }
    }

    void ClearSession(Long accountId) {
        sessionRepo.deleteByAccountId(accountId);
    }

    private String createSignInJwt(Long accountId, Long sessionId, String secret, LocalDateTime time) {
        String jwt = JwtUtils.sign(new TokenVo(accountId, sessionId),
                secret, time);
        if (jwt == null) {
            String jwtCreateStr = "Jwt creation failure";
            log.debug(ExMsgUtils.GetMessage(jwtCreateStr));
            throw new AuthenticationServiceException(jwtCreateStr);
        }
        return jwt;
    }
}
