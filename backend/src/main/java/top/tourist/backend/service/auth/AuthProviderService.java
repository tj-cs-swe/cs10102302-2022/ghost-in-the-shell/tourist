package top.tourist.backend.service.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.AuthSession;
import top.tourist.backend.repo.AccountRepo;
import top.tourist.backend.repo.AuthSessionRepo;
import top.tourist.backend.utils.general.ExMsgUtils;
import top.tourist.backend.utils.general.TimeUtils;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class AuthProviderService implements UserDetailsService {

    private final AccountRepo accountRepo;
    private final AuthSessionRepo sessionRepo;

    public AuthProviderService(AccountRepo accountRepo, AuthSessionRepo sessionRepo) {
        this.accountRepo = accountRepo;
        this.sessionRepo = sessionRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepo.findByName(username);
        Optional<Account> optional = Optional.ofNullable(account);
        return userDetailsBuilder(optional);
    }

    public UserDetails loadUserById(Long id) {
        Optional<Account> optional = accountRepo.findById(id);
        return userDetailsBuilder(optional);
    }

    public AuthSession loadSessionById(Long id) {
        Optional<AuthSession> optional = sessionRepo.findById(id);
        if (optional.isEmpty()) {
            String sessionNotFoundStr = "Session do not exist";
            log.debug(ExMsgUtils.GetMessage(sessionNotFoundStr));
            throw new AccountExpiredException(sessionNotFoundStr);
        }
        return optional.get();
    }

    private UserDetails userDetailsBuilder(Optional<Account> optional) {
        if (optional.isEmpty()) {
            String nameNotFoundStr = "Username do not exist.";
            log.debug(ExMsgUtils.GetMessage(nameNotFoundStr));
            throw new UsernameNotFoundException(nameNotFoundStr);
        }
        Account account = optional.get();
        return User.builder()
                .username(account.getName())
                .password(account.getPassword())
                .roles(account.getRoles().getRole())
//                .accountLocked(account.isDisableStatus()) // 由于需要区分封禁和密码错误，这里不设置locked，在AuthController.SignIn里区分
                .build();
    }

    public void deleteSessionById(Long id) {
        sessionRepo.deleteById(id);
    }

    public void logoutById(Long id) {
        Optional<AuthSession> optional = sessionRepo.findById(id);
        if (optional.isEmpty()) {
            log.debug(ExMsgUtils.GetMessage("Logout with no session before."));
        } else {
            AuthSession session = optional.get();
            if (!(session.isCloseStatus() ||
                    session.getExpireTime().isBefore(TimeUtils.getTime()))) {
                log.debug(ExMsgUtils.GetMessage("Logout normally, session close."));
//                sessionRepo.updateCloseStatusById(session.getId(), true);
                sessionRepo.deleteById(session.getId());
            } else {
                log.debug(ExMsgUtils.GetMessage("Logout with session already expired or closed."));
            }
        }
    }

    public void verifyUpdateSessionExpireTime(Long sessionId, LocalDateTime time) {
        if (sessionRepo.updateExpireTimeById(sessionId, time) <= 0) {
            throw new AuthenticationServiceException("Session update error");
        }
    }
}
