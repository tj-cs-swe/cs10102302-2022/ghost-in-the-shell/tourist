package top.tourist.backend.service.auth.handler;

import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JwtFailedHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        if(exception instanceof LockedException) {
            HttpUtils.reviseResponseFormat(response, EResult.DATA_DUPLICATE.getCode());
            return;
        }
        HttpUtils.reviseResponseFormat(response, EResult.AUTH_FAIL.getCode());
    }
}
