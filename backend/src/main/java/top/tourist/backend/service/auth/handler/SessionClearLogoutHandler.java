package top.tourist.backend.service.auth.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.vo.TokenVo;
import top.tourist.backend.service.auth.AuthProviderService;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class SessionClearLogoutHandler implements LogoutHandler {

    private final AuthProviderService authProviderService;
    private final ObjectMapper mapper;

    public SessionClearLogoutHandler(AuthProviderService authProviderService) {
        this.authProviderService = authProviderService;
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
    }

    @Override
    @Transactional
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        if (authentication == null) {
            return;
        }
        TokenVo tokenVo = ((JwtVerifyToken) authentication).getTokenVo();

        Result<String> result = Result.resultFactory(EResult.SUCCESS, "Logout Success");
        try {

            String json = mapper.writeValueAsString(result);
            authProviderService.logoutById(tokenVo.getSessionId());
            SecurityContextHolder.clearContext();
        } catch (Exception e) {
            HttpUtils.reviseResponseFormat(response, EResult.UNKNOWN_ERROR.getCode());
        }
    }
}
