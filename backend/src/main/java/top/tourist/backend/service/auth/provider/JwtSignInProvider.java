package top.tourist.backend.service.auth.provider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import top.tourist.backend.service.auth.AuthProviderService;
import top.tourist.backend.utils.data.PasswordEncoderUtils;

@Slf4j
@Service
public class JwtSignInProvider extends AbstractUserDetailsAuthenticationProvider {

    private final AuthProviderService authProviderService;

    public JwtSignInProvider(AuthProviderService authProviderService) {
        this.authProviderService = authProviderService;
    }

    @Override
    protected void additionalAuthenticationChecks(
            UserDetails userDetails, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        if (authentication.getPrincipal() == null) {
            log.debug("Authentication failed: No credentials attached");
            throw new BadCredentialsException("BadCredentials");
        }

        String requestPassword = authentication.getCredentials().toString();
        if (!PasswordEncoderUtils.getEncoder().matches(requestPassword, userDetails.getPassword())) {
            log.debug("Authentication failed: Wrong password");
            throw new BadCredentialsException("BadCredentials");
        }

        if (!(userDetails.isAccountNonLocked())) {
            log.debug("Authentication failed: Account locked");
            throw new LockedException("Account locked");
        }
    }

    @Override
    protected UserDetails retrieveUser(
            String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        try {
            return authProviderService.loadUserByUsername(username);
        } catch (UsernameNotFoundException ex) {
            log.debug("Authentication failed: No user using given name");
            throw ex;
        }
    }
}
