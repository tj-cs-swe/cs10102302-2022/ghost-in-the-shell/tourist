package top.tourist.backend.service.auth.provider;

import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.NonceExpiredException;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.account.AuthSession;
import top.tourist.backend.model.vo.TokenVo;
import top.tourist.backend.service.auth.AuthProviderService;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.utils.general.TimeUtils;
import top.tourist.backend.utils.json.JwtUtils;

@Service
public class JwtVerifyProvider implements AuthenticationProvider {
    private final AuthProviderService authProviderService;

    public JwtVerifyProvider(AuthProviderService authProviderService) {
        this.authProviderService = authProviderService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        DecodedJWT jwt = ((JwtVerifyToken) authentication).getToken();
        TokenVo tokenVo = parseTokenVo(jwt);

        UserDetails userDetails = authProviderService.loadUserById(tokenVo.getAccountId());
        checkAccountLock(userDetails);

        AuthSession authSession = authProviderService.loadSessionById(tokenVo.getSessionId());
        checkTokenEncrypt(jwt.getToken(), tokenVo, authSession.getSecret());

        checkTokenExpireTime(jwt, authSession);
        checkSessionExpire(authSession);

        return new JwtVerifyToken(userDetails, jwt, userDetails.getAuthorities());
    }

    private void checkAccountLock(UserDetails userDetails) {
        if (!(userDetails.isAccountNonLocked())) {
            throw new LockedException("Account Locked");
        }
    }

    private void checkSessionExpire(AuthSession authSession) {
        if (authSession.isCloseStatus()) {
            authProviderService.deleteSessionById(authSession.getId());
            throw new NonceExpiredException("Token Session Closed");
        }

        if (TimeUtils.isSessionExpired(authSession.getExpireTime())) {
            authProviderService.deleteSessionById(authSession.getId());
            throw new NonceExpiredException("Token Session Expires");
        }

        authProviderService.verifyUpdateSessionExpireTime(
                authSession.getId(), TimeUtils.getSessionExpireTime());
    }

    private void checkTokenExpireTime(DecodedJWT jwt, AuthSession authSession) {
        if (TimeUtils.isJwtExpired(jwt.getExpiresAt())) {
            authProviderService.deleteSessionById(authSession.getId());
            throw new NonceExpiredException("Token Expires");
        }
    }

    private void checkTokenEncrypt(String token, TokenVo tokenVo, String secret) {
        if (!(JwtUtils.verify(token, tokenVo, secret))) {
            throw new BadCredentialsException("Token falsify");
        }
    }

    private TokenVo parseTokenVo(DecodedJWT jwt) {

        TokenVo tokenVo = JwtUtils.decodePayload(jwt);
        if (tokenVo.getAccountId() == null || tokenVo.getSessionId() == null) {
            throw new InsufficientAuthenticationException("Jwt format error");
        }
        return tokenVo;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(JwtVerifyToken.class);
    }
}
