package top.tourist.backend.service.calendar;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.dto.CalendarTodoDto;
import top.tourist.backend.model.dto.mapper.CalendarTodoMapper;
import top.tourist.backend.model.entity.calendar.CalendarTodo;
import top.tourist.backend.model.factory.CalendarTodoFactory;
import top.tourist.backend.model.vo.CalendarTodoVo;
import top.tourist.backend.repo.AccountRepo;
import top.tourist.backend.repo.CalendarTodoRepo;
import top.tourist.backend.utils.general.TimeUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CalendarService {

    private final CalendarTodoMapper calendarMapper;
    private final CalendarTodoRepo calendarRepo;
    private final AccountRepo accountRepo;

    public CalendarService(CalendarTodoMapper calendarMapper, CalendarTodoRepo calendarRepo, AccountRepo accountRepo) {
        this.calendarMapper = calendarMapper;
        this.calendarRepo = calendarRepo;
        this.accountRepo = accountRepo;
    }

    public List<CalendarTodoDto> FindAllTodosForUser(long accountId) {
        List<CalendarTodo> todoList = calendarRepo.findByUserId(accountId);
        List<CalendarTodoDto> dtoList = new ArrayList<>();
        for (CalendarTodo todo : todoList) {
            dtoList.add(calendarMapper.ToDto(todo));
        }
        return dtoList;
    }

    public Long AddTodoForUser(long accountId, CalendarTodoVo todoVo) {
        CalendarTodo todo = CalendarTodoFactory.Create(todoVo);
        todo.setUserId(accountId);
        todo.setUpdateTime(TimeUtils.getTime());

        calendarRepo.save(todo);
        return todo.getId();
    }

    public void UpdateTodoById(CalendarTodoDto todoDto) {
        CalendarTodo todo = calendarMapper.ToCalendarTodo(todoDto);
        if (!(calendarRepo.existsById(todoDto.getId()))) {
            throw new ObjectNotFoundException(todo, "");
        }
        Long id = todoDto.getId();
        calendarRepo.updateById(
                todo.getName(), todo.getLocation(),
                todo.getState(), todo.getStartTime(),
                todo.getEndTime(), todo.isPrivateControl(),
                todo.isAllDay(), id);
    }

    public void DeleteTodoById(Long todoId) {
        int num = calendarRepo.deleteByIdEquals(todoId);
        if (num < 1) {
            throw new ObjectNotFoundException(todoId, "");
        }
    }

}
