package top.tourist.backend.service.commerce;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.tourist.backend.model.entity.commerce.Product;
import top.tourist.backend.model.entity.commerce.UserOrder;
import top.tourist.backend.model.enumerate.EOrderStatus;
import top.tourist.backend.model.factory.OrderFactory;
import top.tourist.backend.model.vo.TicketInfoVo;
import top.tourist.backend.model.vo.TicketsNumVo;
import top.tourist.backend.repo.ProductRepo;
import top.tourist.backend.repo.UserOrderRepo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.utils.general.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class OrderService {
    private final UserOrderRepo userOrderRepo;

    private final ProductRepo productRepo;

    public OrderService(UserOrderRepo userOrderRepo, ProductRepo productRepo) {
        this.userOrderRepo = userOrderRepo;
        this.productRepo = productRepo;
    }

    private Long getUserId() {
        JwtVerifyToken jwtVerifyToken = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
        return jwtVerifyToken.getTokenVo().getAccountId();
    }

    @Transactional
    boolean DecreaseProductNum(Long productId, Integer num, Integer remain) {
        if (num <= 0 || remain < num) {
            return false;
        }
        return productRepo.updateRemainById(remain - num, productId) > 0;
    }

    @Transactional
    void CreateUserOrder(TicketsNumVo ticketsNumVo, Product product, Long userId) {
        Double price = product.getPrice() * ticketsNumVo.getNum();
        UserOrder userOrder = OrderFactory.create(ticketsNumVo, userId, price);
        // 没有接入支付 故直接成功
        userOrder.setStatus(EOrderStatus.NORMAL);
        userOrderRepo.save(userOrder);
    }

    @Transactional
    public void RequestBuyTickets(TicketsNumVo ticketsNumVo) {
        Long userId = getUserId();
        Product product = productRepo.findByIdEquals(ticketsNumVo.getId());
        if (product == null) {
            throw new IllegalArgumentException("product do not exist");
        }

        if (!DecreaseProductNum(ticketsNumVo.getId(), ticketsNumVo.getNum(), product.getRemain())) {
            throw new UnsupportedOperationException("failed to process for no more remain");
        }

        CreateUserOrder(ticketsNumVo, product, userId);
    }

    private List<Product> MatchOrderWithProduct(List<UserOrder> orderList) {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            Product product = productRepo.findByIdEquals(orderList.get(i).getProductId());
            if (product == null) {
                orderList.remove(i);
                i--;
            } else {
                products.add(product);
            }
        }
        return products;
    }

    private List<TicketInfoVo> EngageTicketsInfo(List<UserOrder> orders, List<Product> products) {
        List<TicketInfoVo> ticketInfoVos = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            TicketInfoVo ticketInfoVo = new TicketInfoVo(orders.get(i), products.get(i));
            ticketInfoVos.add(ticketInfoVo);
        }
        return ticketInfoVos;
    }

    public List<TicketInfoVo> GetAllTicketsForUser() {
        Long userId = getUserId();
        List<UserOrder> orderList = userOrderRepo.findByUserIdSortCreateTimeDesc(userId);
        List<Product> productList = MatchOrderWithProduct(orderList);
        return EngageTicketsInfo(orderList, productList);
    }

    @Transactional
    public Double RefundOrderPartial(TicketsNumVo ticketsNumVo) throws UnsupportedOperationException {
        Long userId = getUserId();
        UserOrder order = userOrderRepo.findByIdEquals(ticketsNumVo.getId());
        if (order == null
                || !Objects.equals(order.getUserId(), userId)
                || order.getProductNum() < ticketsNumVo.getNum()
                || ticketsNumVo.getNum() <= 0) {
            throw new IllegalArgumentException("failed to process for illegal request arguments");
        }

        if (order.getStatus() != EOrderStatus.NORMAL) {
            throw new UnsupportedOperationException("failed to process for order status is not available");
        }

        int newNum = order.getProductNum() - ticketsNumVo.getNum();
        if (newNum == 0) {
            userOrderRepo.updateStatusAndUpdateTimeById(EOrderStatus.REFUND, TimeUtils.getTime(), order.getId());
            return order.getPrice();
        } else {
            Double newPrice = order.getPrice() / order.getProductNum() * newNum;
            Double refundPrice = order.getPrice() - newPrice;
            userOrderRepo.updateNumAndPriceById(newNum, newPrice, order.getId());
            UserOrder refundOrder = OrderFactory.create(order.getProductId(), ticketsNumVo.getNum(), userId, refundPrice);
            refundOrder.setStatus(EOrderStatus.REFUND);
            userOrderRepo.save(refundOrder);
            return refundPrice;
        }
    }

}
