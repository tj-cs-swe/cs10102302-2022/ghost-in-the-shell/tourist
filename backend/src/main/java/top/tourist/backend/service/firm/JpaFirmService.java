package top.tourist.backend.service.firm;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.dto.FirmProfileDto;
import top.tourist.backend.model.dto.mapper.FirmProfileMapper;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.FirmProfile;
import top.tourist.backend.model.entity.notice.IssueFirmRequest;
import top.tourist.backend.model.enumerate.EFirmRequest;
import top.tourist.backend.repo.AccountRepo;
import top.tourist.backend.repo.FirmProfileRepo;
import top.tourist.backend.repo.IssueFirmRequestRepo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class JpaFirmService {

    @Resource
    private FirmProfileRepo firmProfileRepo;

    @Resource
    private IssueFirmRequestRepo issueFirmRequestRepo;

    @Resource
    private FirmProfileMapper firmProfileMapper;

    @Resource
    private AccountRepo accountRepo;

    private enum Status {
        unsubmit,
        checking,
        pass,
        success,
        exit_checking,
        exit_pass,
        reject_checking,
        reject_exit,
        rechecking,
        reject_rechecking
    }
    //总结：
    //
    //1.  enum<->int
    //
    //enum -> int: int i = enumType.value.ordinal();
    //
    //int -> enum: enumType b= enumType.values()[i];
    //
    //2.  enum<->String
    //
    //enum -> String: enumType.name();
    //
    //String -> enum: enumType.valueOf(name);

    public short stringToShort(String stat) {
        return (short) Status.valueOf(stat).ordinal();
    }

    public String shortToString(short num) {
        return Status.values()[num].name();
    }

    // 获取商家id
    private Long getUserId() {
        JwtVerifyToken jwtVerifyToken = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
        return jwtVerifyToken.getTokenVo().getAccountId();
    }

    // 基本的增、删、改、查
    private Optional<FirmProfile> getOptionalFirm() {
        Long userId = getUserId();
        return firmProfileRepo.findById(userId);
    }


    private void insertFirm(String nameIn) {
        firmProfileRepo.save(new FirmProfile(getUserId(), nameIn));
    }

    public void updateFirm(FirmProfile newFirm) {
        firmProfileRepo.save(newFirm);
    }

    // 修改AuditStatus
    public int updateFirmStatus(String newStatus) {
        Optional<FirmProfile> myFirm = getOptionalFirm();
        if (myFirm.isEmpty())
            return -1;
        else {
            short statNum = stringToShort(newStatus);
            myFirm.get().setAuditStatus((statNum));
            updateFirm(myFirm.get());
            return 0;
        }
    }

    // FirmController
    // 商户自己操作
    public String getStatus() {
        Optional<FirmProfile> optionalFirm = getOptionalFirm();
        if (optionalFirm.isEmpty())
            return "error";
        else
            return shortToString(optionalFirm.get().getAuditStatus());
    }

    public FirmProfileDto getInfo() {
        Optional<FirmProfile> optionalFirm = getOptionalFirm();
        if (optionalFirm.isEmpty())
            return null;
        else
            return firmProfileMapper.ToDto(optionalFirm.get());
    }

    public int changeInfo(FirmProfileDto newFirmDto) {
        Optional<FirmProfile> optionalFirm = getOptionalFirm();
        if (optionalFirm.isEmpty())
            return -1;
        else {
            getNewFirmProfileObject(newFirmDto, optionalFirm);
            return updateFirmStatus("checking");
        }
    }

    public int rechecking(FirmProfileDto newFirmDto)
    {
        Optional<FirmProfile> optionalFirm = getOptionalFirm();
        if (optionalFirm.isEmpty())
            return -1;
        else {
            getNewFirmProfileObject(newFirmDto, optionalFirm);
            return updateFirmStatus("rechecking");
        }
    }

    private void getNewFirmProfileObject(FirmProfileDto newFirmDto, Optional<FirmProfile> optionalFirm) {
        FirmProfile newFirm = firmProfileMapper.ToFirmProfile(newFirmDto);
        FirmProfile oldFirm = optionalFirm.get();

        oldFirm.setName(newFirm.getName());
        oldFirm.setLocation(newFirm.getLocation());
        oldFirm.setWebsite(newFirm.getWebsite());
        oldFirm.setContactName(newFirm.getContactName());
        oldFirm.setContactTel(newFirm.getContactTel());
        oldFirm.setBankName(newFirm.getBankName());
        oldFirm.setBankAccount(newFirm.getBankAccount());
        oldFirm.setIntro(newFirm.getIntro());

        updateFirm(oldFirm);
    }

    public int signContract() {
        return updateFirmStatus("success");
    }

    public int applyExit(String reason) {
        long i = 0;
        for (; i < 65536; ++i) {
            if (!issueFirmRequestRepo.existsById(i))
                break;
        }
        IssueFirmRequest newIssue = new IssueFirmRequest(i, getUserId(), EFirmRequest.QUIT, reason);
        issueFirmRequestRepo.save(newIssue);
        return updateFirmStatus("exit_checking");
    }

    public int applySettleAgain() {
        return updateFirmStatus("unsubmit");
    }

    // 管理员审核


    // 管理员获取商家信息
    public List<FirmProfile> getAllFirmInfo() {
        return firmProfileRepo.findAll();
    }

    public FirmProfile findByName(String name) {
        Account account = accountRepo.findByName(name);
        if (account == null) {
            return null;
        }
        Optional<FirmProfile> firmProfile = firmProfileRepo.findById(account.getId());

        if (firmProfile.isEmpty()) {
            return null;
        }
        else {
            return firmProfile.get();
        }
    }
}
