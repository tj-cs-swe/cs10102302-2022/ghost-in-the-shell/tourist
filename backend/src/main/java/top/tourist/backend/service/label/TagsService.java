package top.tourist.backend.service.label;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.tag.AccountTagsRel;
import top.tourist.backend.model.entity.tag.Tags;
import top.tourist.backend.model.vo.ChangeLabel;
import top.tourist.backend.model.vo.LabelWithRole;
import top.tourist.backend.model.vo.TagsVo;
import top.tourist.backend.repo.AccountTagsRelRepo;
import top.tourist.backend.repo.TagsRepo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
public class TagsService {

    private final TagsRepo tagsRepo;

    private final AccountTagsRelRepo accountTagsRelRepo;

    private final Comparator<Long> compare = new Comparator<Long>() {
        @Override
        public int compare(Long lhs, Long rhs) {
            long res = lhs - rhs;
            if (res > 0) {
                return 1;
            } else if (res < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    public TagsService(TagsRepo tagsRepo, AccountTagsRelRepo accountTagsRelRepo) {
        this.tagsRepo = tagsRepo;
        this.accountTagsRelRepo = accountTagsRelRepo;
    }

    public void UpdateTags(List<TagsVo> tagsVoList) {
        Long accountId = GetAccountIdFromAuth();
        List<Long> tagsIdNewList = UpdateNewTags(tagsVoList);
        List<Long> tagsIdOldList = GetNormalExistTags(accountId);
        IntersectAndUpdateTags(tagsIdNewList, tagsIdOldList);
        for (Long num : tagsIdNewList) {
            accountTagsRelRepo.save(new AccountTagsRel(accountId, num));
        }
        for (Long num : tagsIdOldList) {
            accountTagsRelRepo.deleteByAccountIdAndTagId(accountId, num);
        }
    }

    public List<TagsVo> GetTags() {
        Long accountId = GetAccountIdFromAuth();
        List<Long> tagsIdList = GetNormalExistTags(accountId);
        List<String> tagsNameList = tagsRepo.findNameById(tagsIdList);
        List<TagsVo> tagsVoList = new ArrayList<>();
        for (String label : tagsNameList) {
            tagsVoList.add(new TagsVo(label));
        }
        return tagsVoList;
    }

    public List<TagsVo> GetAdminTags() {
        Long accountId = GetAccountIdFromAuth();
        List<Long> tagsIdList = GetAdminExistTags(accountId);
        List<String> tagsNameList = tagsRepo.findNameById(tagsIdList);
        List<TagsVo> tagsVoList = new ArrayList<>();
        for (String label : tagsNameList) {
            tagsVoList.add(new TagsVo(label));
        }
        return tagsVoList;
    }

    private void IntersectAndUpdateTags(List<Long> newList, List<Long> oldList) {
        for (int i = 0, j = 0; i < newList.size() && j < oldList.size(); ) {
            long lhs = newList.get(i);
            long rhs = newList.get(i);
            if (lhs > rhs) {
                j++;
            } else if (rhs > lhs) {
                i++;
            } else {
                newList.remove(i);
                oldList.remove(j);
            }
        }
    }

    private List<Long> GetNormalExistTags(Long accountId) {
        List<AccountTagsRel> relList = accountTagsRelRepo.findByIdOrderByTagIdAsc(accountId, false);
        List<Long> labelIdList = new ArrayList<>();
        for (AccountTagsRel rel : relList) {
            labelIdList.add(rel.getTagId());
        }
        return labelIdList;
    }

    private List<Long> GetAdminExistTags(Long accountId) {
        List<AccountTagsRel> relList = accountTagsRelRepo.findByIdOrderByTagIdAsc(accountId, true);
        List<Long> labelIdList = new ArrayList<>();
        for (AccountTagsRel rel : relList) {
            labelIdList.add(rel.getTagId());
        }
        return labelIdList;
    }


    private List<Long> UpdateNewTags(List<TagsVo> tagsVoList) {
        List<Long> labelIdList = new ArrayList<>();
        for (TagsVo tagsVo : tagsVoList) {
            String label = tagsVo.getLabel();
            if (label != null && !(tagsRepo.existsByName(label))) {
                Tags tag = new Tags(label);
                labelIdList.add(tag.getId());
                tagsRepo.save(tag);
            } else if (label != null) {
                Tags tag = tagsRepo.findByNameEquals(label);
                labelIdList.add(tag.getId());
            }
        }
        labelIdList.sort(compare);
        return labelIdList;
    }

    private long GetAccountIdFromAuth() throws AuthenticationCredentialsNotFoundException {
        JwtVerifyToken token;
        try {
            token = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
            return token.getTokenVo().getAccountId();
        } catch (Exception e) {
            throw new AuthenticationCredentialsNotFoundException("");
        }
    }

    private enum roles {
        USER,
        FIRM
    }

    // int 转 string
    private String getStringRoles(Integer i)
    {
        return roles.values()[i].name();
    }

    // string 转 int
    private Integer getIntRoles(String s)
    {
        return roles.valueOf(s).ordinal();
    }

    // 管理员获取tags中的所有标签
    public List<LabelWithRole> adminGetAllLabels() {
        List<Tags> tagsList = tagsRepo.findAll();
        List<LabelWithRole> labelList = new ArrayList<>();
        for(Tags tag : tagsList){
            labelList.add(new LabelWithRole(tag.getName(),getStringRoles(tag.getRoles())));
        }
        return labelList;
    }

    // 管理员添加一个新的tag
    public int adminAddLabel(LabelWithRole roleLabel) {
        Tags findTag = tagsRepo.findByNameEquals(roleLabel.getLabel());
        if(findTag == null){
            tagsRepo.save(new Tags(roleLabel.getLabel(),getIntRoles(roleLabel.getRoles())));
            return 0;
        }
        else
            return -1;
    }

    // 管理员删除若干标签
    public int adminDeleteLabel(List<LabelWithRole> roleLabelList)
    {
        for(LabelWithRole i : roleLabelList)
        {
            Tags findTag = tagsRepo.findByNameEquals(i.getLabel());
            if(findTag == null)
                continue;
            else
                tagsRepo.delete(findTag);
        }
        return 0;
    }

    // 管理员修改若干标签
    public int adminChangeLabelRoles(ChangeLabel changeLabel)
    {
        String stringRoles = changeLabel.getTarget();
        Integer intRoles = getIntRoles(stringRoles);
        List<TagsVo> tagsVoList = changeLabel.getOrigin();
        for(TagsVo i : tagsVoList)
        {
            String originLabel = i.getLabel();
            Tags tag = tagsRepo.findByNameEquals(originLabel);
            if(tag == null)
                continue;
            else
            {
                tagsRepo.delete(tag);
                tagsRepo.save(new Tags(tag.getName(),intRoles));
            }
        }
        return 0;
    }

    // 用户、商户获取所有用户、商户标签
    public List<String> GetAllLabelsByRoles(String roles)
    {
        int intRoles = getIntRoles(roles);
        List<Tags> tagsList = tagsRepo.findAll();
        List<String> labelList = new ArrayList<>();
        for(Tags tag : tagsList){
            if(tag.getRoles() == intRoles)
                labelList.add(tag.getName());
        }
        return labelList;
    }

}
