package top.tourist.backend.service.passage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.forum.Article;
import top.tourist.backend.model.entity.forum.Comment;
import top.tourist.backend.model.vo.*;
import top.tourist.backend.repo.ArticleRepo;
import top.tourist.backend.repo.CommentRepo;
import top.tourist.backend.repo.UserProfileRepo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.utils.data.SnowFlakeUtils;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PassageService {

    private final ArticleRepo articleRepo;
    private final UserProfileRepo userProfileRepo;
    private final CommentRepo commentRepo;
    private final SnowFlakeUtils idGen;

    public PassageService(ArticleRepo articleRepo, UserProfileRepo userProfileRepo, CommentRepo commentRepo) {
        this.articleRepo = articleRepo;
        this.userProfileRepo = userProfileRepo;
        this.commentRepo = commentRepo;
        this.idGen = new SnowFlakeUtils(1, 0);
    }

    // 查找所有文章，不包含“内容”和“评论”
    public List<Passage> FindALLPassage()
    {
        List<Article> articleList = articleRepo.findAll();
        List<Passage> passageList = new ArrayList<>();

        for(Article arti : articleList)
        {
            Long userId = arti.getUser_id();
            String nickName = userProfileRepo.findById(userId).get().getNickname();
            passageList.add(new Passage(arti,nickName));
        }

        return passageList;
    }

    // 按id查找一篇文章的详情，包含“内容”和“评论”
    public PassageWithComment FindPassageComment(Long articleId)
    {
        Article article = articleRepo.findByIdEquals(articleId);
        // 应该先判断 article 是否为空，但是前端可以保证存在（非并发的情况下）
        String nickName = userProfileRepo.findById(article.getUser_id()).get().getNickname();
        PassageWithComment passage = new PassageWithComment(article, nickName);

        if(commentRepo.countByArticleId(articleId) == 0)
            return passage;

        List<Comment> commentList = commentRepo.findAllPlease();
        for(Comment comm : commentList)
        {
            if(Objects.equals(comm.getArticleId(), articleId))
            {
                nickName = userProfileRepo.findById(comm.getUserId()).get().getNickname();
                passage.AddComment(new CommentInPassage(comm, nickName));
            }
        }

        return passage;
    }

    // 用户发布文章
    public void UserCreateArticle(CreatedArticle createdArticle)
    {
        Long aid = idGen.NextId();
        JwtVerifyToken jwtVerifyToken = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
        Long uid = jwtVerifyToken.getTokenVo().getAccountId();

        Article article = new Article(createdArticle, aid, uid);
        articleRepo.save(article);
    }

    // 用户发布评论
    public void UserCreateComment(CreatedComment createdComment)
    {
        Long cid = idGen.NextId();
        JwtVerifyToken jwtVerifyToken = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
        Long uid = jwtVerifyToken.getTokenVo().getAccountId();

        Comment comment = new Comment(createdComment, cid, uid);
        commentRepo.save(comment);
    }

    // 删除文章
    public void UserDeleteArticle(Long articleId)
    {
        articleRepo.deleteByIdEquals(articleId);
    }

    // 删除评论
    public void UserDeleteComment(Long commentId)
    {
        commentRepo.deleteByIdEquals(commentId);
    }
}
