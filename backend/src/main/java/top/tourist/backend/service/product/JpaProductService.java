package top.tourist.backend.service.product;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.entity.commerce.Product;
import top.tourist.backend.repo.ProductRepo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class JpaProductService {
    @Resource
    private ProductRepo productRepo;

    public boolean insertProduct(Product product) {
        try {
            productRepo.save(product);

            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateProduct(Product product) {
        return insertProduct(product);
    }

    public boolean deleteProduct(Product product) {
        try {
            // 这里删除操作没有检查product是否属于当前firm
            // 是因为在调用该方法之前使用findProductByName获取所有待删除的product
            // 而该方法返回的是所有属于当前firm且名为productName的实例
            // 如果单独调用的话需要注意一下
            productRepo.delete(product);

            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Product> findProductByName(String productName) {
        return authorizeFilter(productRepo.findProductsByName(productName));
    }

    public List<Product> findAllProduct() {
        return authorizeFilter(productRepo.findAll());
    }

    private List<Product> authorizeFilter(List<Product> products) {
        // 获取商家id
        JwtVerifyToken jwtVerifyToken =  (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();

        Long firm_id = jwtVerifyToken.getTokenVo().getAccountId();

        List<Product> returnList = new ArrayList<>();

        for (Product product : products) {
            if (firm_id.equals(product.getFirmId())) {
                returnList.add(product);
            }
        }
        return returnList;
    }

    public List<Product> findProductsByNameContains(String name) {
        Pageable pageable = PageRequest.of(0, 100);
        return productRepo.findByNameContains(name, pageable);
    }

    public List<Product> getRecommendProducts() {
        //todo 暂时采用随机推荐方法
        return productRepo.getRandomProducts(20);
    }
}
