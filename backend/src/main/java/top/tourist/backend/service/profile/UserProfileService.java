package top.tourist.backend.service.profile;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import top.tourist.backend.model.dto.UserProfileDto;
import top.tourist.backend.model.entity.account.UserProfile;
import top.tourist.backend.model.vo.UserProfileVo;
import top.tourist.backend.repo.AccountRepo;
import top.tourist.backend.repo.ArticleRepo;
import top.tourist.backend.repo.CommentRepo;
import top.tourist.backend.repo.UserProfileRepo;
import top.tourist.backend.service.auth.token.JwtVerifyToken;
import top.tourist.backend.utils.general.TimeUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

@Service
public class UserProfileService {

    private final AccountRepo accountRepo;
    private final UserProfileRepo userProfileRepo;

    private final ArticleRepo articleRepo;

    private final CommentRepo commentRepo;

    public UserProfileService(
            AccountRepo accountRepo, UserProfileRepo userProfileRepo,
            ArticleRepo articleRepo, CommentRepo commentRepo) {
        this.accountRepo = accountRepo;
        this.userProfileRepo = userProfileRepo;
        this.articleRepo = articleRepo;
        this.commentRepo = commentRepo;
    }

    public int UpdateUserProfile(UserProfileVo vo) throws Exception {
        ValidateVo(vo);
        long userId = GetAccountIdFromAuth();
        return userProfileRepo.updateInfoById(vo.getNickname(), vo.getEmail(), vo.getMotto(), TimeUtils.getTime(), userId);
    }

    private void ValidateVo(UserProfileVo vo) {
        if (vo.getNickname() == null) {
            vo.setNickname("");
        }

        if (vo.getEmail() == null) {
            vo.setEmail("");
        }

        if (vo.getMotto() == null) {
            vo.setMotto("");
        }
    }

    public Result<UserProfileDto> GetUserProfile() {
        long userId = GetAccountIdFromAuth();
        UserProfile userProfile;
        userProfile = userProfileRepo.findByIdEquals(userId);
        if (userProfile == null) {
            throw new AuthenticationServiceException("");
        }

        long articleNum, commentNum;
        articleNum = articleRepo.countByUserId(userId);
        commentNum = commentRepo.countByUserId(userId);

        UserProfileDto userProfileDto = new UserProfileDto(userProfile, (int) articleNum, (int) commentNum);
        return Result.resultFactory(EResult.SUCCESS, userProfileDto);
    }

    private long GetAccountIdFromAuth() throws AuthenticationCredentialsNotFoundException {
        JwtVerifyToken token;
        try {
            token = (JwtVerifyToken) SecurityContextHolder.getContext().getAuthentication();
            return token.getTokenVo().getAccountId();
        } catch (Exception e) {
            throw new AuthenticationCredentialsNotFoundException("");
        }
    }
}
