package top.tourist.backend.utils.data;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderUtils {
    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public PasswordEncoderUtils() {
    }

    public static BCryptPasswordEncoder getEncoder() {
        return encoder;
    }
}
