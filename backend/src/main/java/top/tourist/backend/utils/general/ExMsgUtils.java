package top.tourist.backend.utils.general;

public class ExMsgUtils {
    public static String GetMessage(String msg) {
        if (msg == null) {
            return "";
        }
        StackTraceElement[] arr = new Throwable().getStackTrace();
        String className = arr[1].getClassName();
        String methodName = arr[1].getMethodName();
        return className + "." + methodName + ": " + msg;
    }
}
