package top.tourist.backend.utils.general;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class TimeUtils {

    public static LocalDateTime getTime() {
        return LocalDateTime.now(ZoneId.systemDefault());
    }

    public static Date getDate(LocalDateTime time) {
        return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime DateToTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDateTime getSessionExpireTime() {
        return getTime().plusMinutes(30);
    }

    public static LocalDateTime getSessionExpireTime(LocalDateTime time) {
        return time.plusMinutes(30);
    }

    public static LocalDateTime getJwtExpireTime() {
        LocalDateTime time = getTime();
        return time.plusHours(3);
    }

    public static LocalDateTime getJwtExpireTime(LocalDateTime time) {
        return time.plusHours(3);
    }

    public static boolean isJwtExpired(Date time) {
        LocalDateTime local = TimeUtils.DateToTime(time);
        LocalDateTime now = TimeUtils.getTime();
        return now.isAfter(local);
    }

    public static boolean isSessionExpired(LocalDateTime time) {
        return TimeUtils.getTime().isAfter(time);
    }

}
