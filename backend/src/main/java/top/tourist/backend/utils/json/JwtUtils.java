package top.tourist.backend.utils.json;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import top.tourist.backend.model.vo.TokenVo;
import top.tourist.backend.utils.general.TimeUtils;

import java.time.LocalDateTime;
import java.util.Date;

public class JwtUtils {
    private static final String accountStr = "accountId";
    private static final String sessionStr = "sessionId";
    private static final String issuer = "tourist";

    public static String sign(TokenVo tokenVo, String secret, LocalDateTime expireTime) {
        try {
            Date date = TimeUtils.getDate(expireTime);
            Algorithm algo = Algorithm.HMAC512(secret);
            return JWT.create()
                    .withClaim(accountStr, tokenVo.getAccountId())
                    .withClaim(sessionStr, tokenVo.getSessionId())
                    .withExpiresAt(date)
                    .sign(algo);
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean verify(String token, TokenVo tokenVo, String secret) {
        try {
            Algorithm algo = Algorithm.HMAC512(secret);
            JWTVerifier verifier = JWT.require(algo)
                    .withClaim(accountStr, tokenVo.getAccountId())
                    .withClaim(sessionStr, tokenVo.getSessionId())
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static TokenVo decodePayload(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return new TokenVo(
                jwt.getClaim(accountStr).asLong(),
                jwt.getClaim(sessionStr).asLong()
        );
    }

    public static TokenVo decodePayload(DecodedJWT jwt) {
        return new TokenVo(
                jwt.getClaim(accountStr).asLong(),
                jwt.getClaim(sessionStr).asLong()
        );
    }

}
