package top.tourist.backend.utils.network;

import org.springframework.http.MediaType;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import javax.servlet.http.HttpServletResponse;

public class HttpUtils {
    public final static String authHeader = "Authorization";
    public final static String authPrefix = "Bearer ";

    public final static String success = "success";
    public final static String fail = "fail";

    public static void reviseResponseFormat(HttpServletResponse response, int status) {
        response.setStatus(status);
        response.setCharacterEncoding("UTF-8");
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    }

    public static Result<String> reviseStringResponseSuccess(HttpServletResponse response) {
        HttpUtils.reviseResponseFormat(response, EResult.SUCCESS.getCode());
        return Result.resultFactory(EResult.SUCCESS, HttpUtils.success);
    }

    public static Result<String> reviseStringResponseFail(HttpServletResponse response, EResult eResult) {
        HttpUtils.reviseResponseFormat(response, eResult.getCode());
        return Result.resultFactory(eResult, HttpUtils.fail);
    }

    public static void setTokenHeader(HttpServletResponse response, String token) {
        response.setHeader(authHeader, authPrefix + token);
    }


}
