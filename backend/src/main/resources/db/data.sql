insert into account
values (1, 'root', '$2a$10$BqLM.jqWsU/DIzMY9FChBe96Mrp8ECEvjviDyrDNyqHWWA2/2hZ9e', 'ADMIN', 0, '2022-01-01 00:00:00',
        '2022-01-01 00:00:00', '2022-01-01 00:00:00', null) ^;

insert into account value (2, 'test', '$2a$10$WhhYFQBv3TDXIuxD/2Rnk.rGYr0aA34h4Pys5kFHxIn43TkFZZG/u',
                           'ADMIN', 0, '2022-04-18 14:56:52', '2022-04-18 14:56:52', '2022-04-18 14:56:52', null) ^;

insert into account value (3, 'user', '$2a$10$WhhYFQBv3TDXIuxD/2Rnk.rGYr0aA34h4Pys5kFHxIn43TkFZZG/u',
                           'USER', 0, '2022-05-05 00:00:00', '2022-05-05 00:00:00', '2022-05-05 00:00:00', null) ^;

insert into user_profile value (3, 'user', 0, null, null, null, '2022-05-22 00:00:00', '2022-05-22 00:00:00') ^;

