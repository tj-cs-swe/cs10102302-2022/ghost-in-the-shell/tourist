# 执行前注意事项
# - 由于 spring boot 的 bug 故自定义增加了 ^ 识别行尾
# - 传入 MYSQL_URL MYSQL_USER MYSQL_PASS 环境变量
# - 创建 tourist 空数据库
# - 为用户开启 tourist 数据库上的所有特权
# - 设置 set global log_bin_trust_function_creators = 1 允许创建触发器
# - 慎用 root 账户登录

drop trigger if exists trig_tag_rel_insert ^;
drop trigger if exists trig_bulletin_insert ^;
drop trigger if exists trig_topic_insert ^;
drop table if exists account_tags_rel, comment, article_tags_rel, product_image, order_product_rel ^;
drop table if exists article, tags, bulletin, topic, user_order, image_store ^;
drop table if exists firm_extern, issue_user_request, issue_firm_request ^;
drop table if exists product, calendar_todo, account_notice ^;
drop table if exists firm_profile, user_profile , auth_session^;
drop table if exists account, nation_district ^;

create table nation_district
(
    id   integer      not null primary key,
    name varchar(127) not null,
    index index_district_name (name)
) ^;

create table account
(
    id           bigint                         not null primary key,
    name         varchar(255)                   not null unique,
    password     varchar(511)                   not null,
    roles        enum ('USER', 'FIRM', 'ADMIN') not null,
    status       tinyint(1),
    signup_time  datetime                       not null,
    update_time  datetime                       not null,
    signin_time  datetime                       not null,
    disable_time datetime,
    unique index_auth_name (name)
) ^;

create table auth_session
(
    id           bigint       not null primary key,
    account_id   bigint       not null unique,
    secret       varchar(255) not null,
    expire_time  datetime     not null,
    close_status tinyint(1),
    constraint fk_auth_session_account_id foreign key (account_id) references account (id) on delete cascade
) ^;

create table user_profile
(
    id          bigint       not null primary key,
    nickname    varchar(255) not null,
    level       tinyint      not null default 0,
    avatar_url  varchar(1023),
    email       varchar(127),
    motto       varchar(255),
    create_time datetime     not null,
    update_time datetime     not null,
    constraint fk_user_profile_auth_id foreign key (id) references account (id) on delete cascade,
    index index_profile_nickname (nickname)
) ^;

create table firm_profile
(
    id           bigint       not null primary key,
    name         varchar(255) not null,
    avatar_url   varchar(1023),
    cert_url     varchar(1023),
    level        tinyint      not null default 0,
    audit_status tinyint      not null default 0,

    intro        text,
    location     varchar(255),
    website      varchar(1023),
    contact_name varchar(63),
    contact_num  varchar(127),
    bank_name    varchar(127),
    bank_account varchar(127),
    turnover     decimal(16, 2),

    constraint fk_firm_profile_auth_id foreign key (id) references account (id) on delete cascade,
    index index_firm_name (name)
) ^;

create table account_notice
(
    id          bigint       not null primary key,
    sender_id   bigint       not null,
    receiver_id bigint       not null,
    title       varchar(255) not null,
    cont        text,
    create_time datetime     not null,
    expire_time datetime     not null,
    constraint fk_notice_sender_id foreign key (sender_id) references account (id) on delete cascade,
    constraint fk_notice_receiver_id foreign key (receiver_id) references account (id) on delete cascade,
    index index_notice_receiver_id (receiver_id)
) ^;

create table issue_firm_request
(
    id          bigint                                                  not null primary key,
    firm_id     bigint                                                  not null,
    type        enum ('JOIN', 'CONTRACT', 'SETTLE', 'PROMOTE' , 'QUIT') not null,
    message     varchar(1023),
    audit       tinyint                                                 not null default 0,
    create_time datetime                                                not null,
    update_time datetime                                                not null,
    constraint fk_issue_firm_id foreign key (firm_id) references firm_profile (id) on delete cascade
) ^;

create table issue_user_request
(
    id          bigint   not null primary key,
    user_id     bigint   not null,
    type        enum ('ARTICLE', 'APPEAL','REFUND'),
    message     varchar(1023),
    audit       tinyint  not null default 0,
    create_time datetime not null,
    update_time datetime not null,
    constraint fk_issue_user_id foreign key (user_id) references user_profile (id) on delete cascade
) ^;

create table image_store
(
    id             bigint                     not null primary key,
    path           varchar(1023)              not null,
    upload_time    datetime                   not null,
    upload_user_id bigint                     not null,
    privacy        enum ('PRIVATE', 'PUBLIC') not null default 'PRIVATE',
    constraint fk_image_upload_user_id foreign key (upload_user_id) references account (id) on delete cascade
) ^;

create table calendar_todo
(
    id              bigint                not null primary key,
    user_id         bigint                not null,
    name            varchar(511)          not null,
    location        varchar(511),
    state           enum ('BUSY', 'FREE') not null default 'BUSY',

    start_time      datetime              not null,
    end_time        datetime              not null,

    private_control tinyint(1)            not null default 0,
    all_day         tinyint(1)            not null default 0,

    update_time     datetime              not null,

    constraint fk_calendar_issue_auth_id foreign key (user_id) references user_profile (id) on delete cascade,
    index index_todo_user_id (user_id)
) ^;

create table firm_extern
(
    id      int auto_increment not null primary key,
    name    varchar(255)       not null,
    website varchar(1023),
    index index_firm_extern_name (name)
) ^;

create table product
(
    id          bigint       not null primary key,
    firm_id     bigint       not null,
    name        varchar(255) not null,
    price       decimal(12, 2) default 0,
    remain      integer      not null,
    intro       text,
    address     varchar(255) not null,
    create_time datetime     not null,
    update_time datetime     not null,

    constraint fk_product_firm_id foreign key (firm_id) references firm_profile (id) on delete cascade,
    index index_product_name (name)
) ^;

create table product_image
(
    image_id   bigint  not null primary key,
    product_id bigint  not null,
    serial     integer not null unique,
    constraint fk_image_store_id foreign key (image_id) references image_store (id) on delete cascade,
    constraint fk_image_product_id foreign key (product_id) references product (id) on delete cascade
) ^;

create table user_order
(
    id          bigint                                                  not null primary key,
    user_id     bigint                                                  not null,
    product_id  bigint                                                  not null,

    product_num integer                                                 not null,
    price       decimal(12, 2)                                          not null,
    status      enum ('CHECKING', 'NORMAL', 'USED', 'REFUND', 'CANCEL') not null default 'CHECKING',
    create_time datetime                                                not null,
    update_time datetime                                                not null,

    coupon      varchar(1023),
    expire_time datetime,
    constraint fk_order_user_id foreign key (user_id) references user_profile (id) on delete cascade,
    constraint fk_order_product_id foreign key (product_id) references product (id) on delete cascade
) ^;

create table bulletin
(
    id          bigint       not null primary key,
    admin_id    bigint       not null,
    title       varchar(255) not null,
    cont        text,
    create_time datetime     not null,
    update_time datetime     not null,
    constraint fk_bulletin_admin_id foreign key (admin_id) references account (id) on delete cascade
) ^;

create table topic
(
    id          bigint       not null primary key,
    name        varchar(255) not null,
    admin_id    bigint       not null,
    create_time datetime     not null,
    click_cnt   bigint       not null default 0,
    constraint fk_topic_admin_id foreign key (admin_id) references account (id) on delete cascade
) ^;

create table article
(
    id          bigint       not null primary key,
    user_id     bigint       not null,
    topic_id    bigint,

    title       varchar(255) not null,
    cont        text,
    views       bigint,
    likes       bigint,
    create_time datetime     not null,
    update_time datetime     not null,

    constraint fk_article_user_id foreign key (user_id) references user_profile (id) on delete cascade,
    constraint fk_article_topic_id foreign key (topic_id) references topic (id) on delete cascade
) ^;

create table comment
(
    id          bigint       not null primary key,
    user_id     bigint       not null,
    article_id  bigint       not null,
    likes       bigint,
    create_time datetime     not null,
    update_time datetime     not null,
    cont        varchar(255) not null,
    parent_id   bigint,

    constraint fk_comment_user_id foreign key (user_id) references user_profile (id) on delete cascade,
    constraint fk_comment_article_id foreign key (article_id) references article (id) on delete cascade,
    constraint fk_comment_parent_id foreign key (parent_id) references comment (id) on delete cascade
) ^;

create table tags
(
    id   bigint      not null primary key,
    name varchar(63) not null unique,
    roles integer not null,
    heat integer
) ^;

create table account_tags_rel
(
    id           bigint     not null,
    tag_id       bigint     not null,
    is_admin_add tinyint(1) not null default 0,
    constraint pk_tags_rel primary key (id, tag_id),
    constraint fk_tags_rel_auth_id foreign key (id) references account (id) on delete cascade,
    constraint fk_tags_rel_tag_id foreign key (tag_id) references tags (id) on delete cascade
) ^;

create table article_tags_rel
(
    id     bigint not null,
    tag_id bigint not null,
    constraint pk_article_rel primary key (id, tag_id),
    constraint fk_article_id foreign key (id) references article (id) on delete cascade,
    constraint fk_article_tag_id foreign key (tag_id) references tags (id) on delete cascade
) ^;

create trigger trig_topic_insert
    before insert
    on topic
    for each row
begin
    declare role enum ('USER', 'FIRM', 'ADMIN');
    set role = (select roles from account where new.admin_id = account.id);
    if (role is null or role <> 'ADMIN') then
        signal sqlstate '45000' set message_text = 'topic must be published by admin';
    end if;
end ^;

create trigger trig_bulletin_insert
    before insert
    on bulletin
    for each row
begin
    declare role enum ('USER', 'FIRM', 'ADMIN');
    set role = (select roles from account where new.admin_id = account.id);
    if (role is null or role <> 'ADMIN') then
        signal sqlstate '45001' set message_text = 'bulletin must be published by admin';
    end if;
end ^;

create trigger trig_tag_rel_insert
    before insert
    on account_tags_rel
    for each row
begin
    declare roles enum ('USER', 'FIRM', 'ADMIN');
    set roles = (select roles from account where id = new.id);
    if roles = 'ADMIN' then
        signal sqlstate '45002' set message_text = 'tags can not added to admin';
    end if;
end ^;
