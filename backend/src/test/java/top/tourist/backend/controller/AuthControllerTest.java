package top.tourist.backend.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import top.tourist.backend.model.dto.AccountDto;
import top.tourist.backend.model.vo.SignInVo;
import top.tourist.backend.model.vo.SignUpVo;
import top.tourist.backend.repo.AccountRepo;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthControllerTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private AccountRepo accountRepo;

    private MockMvc mvc;
    private ObjectMapper mapper;

    private String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJhY2NvdW50SWQiOjM5MDAxMjA1OTE3MDg5NzkyLCJzZXNzaW9uSWQiOjM5MDAxMjY2Mzc3OTg2MDQ4LCJleHAiOjE2NTAzMTU0MTJ9.qw4d2PRDH_pKeN5J9t-iPEE6rbxItRksX2UFm58THloHcOP4QldXQyNDUcsITLDT-evwEf4A7_PXPGAZPRCo2w";

    private Result<AccountDto> parseResponseData(String body) throws JsonProcessingException {
        return mapper.readValue(body, new TypeReference<Result<AccountDto>>() {
        });
    }

    private String parseJwtHeader(MockHttpServletResponse response) {
        String header = response.getHeader(HttpUtils.authHeader);
        try {

            header = header.split("")[1];
        } catch (Exception ex) {
            throw new RuntimeException();
        }
        return header;
    }

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
    }

    @Test
    @Order(1)
    public void signUp() throws Exception {
        accountRepo.deleteByNameEquals("tourist");
        SignUpVo signUpVo = new SignUpVo("tourist", "321654987", "USER");
        String sender = mapper.writeValueAsString(signUpVo);

        RequestBuilder request = post("/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(sender);
        System.out.println("-------------- HTTP Response -----------------");
        MockHttpServletResponse response = mvc
                .perform(request)
                .andDo(MockMvcResultHandlers.print()).
                andReturn()
                .getResponse();
        Assertions.assertEquals(EResult.SUCCESS.getCode().intValue(), response.getStatus());

        System.out.println("-------------- Json Data -----------------");
        Result<AccountDto> result = parseResponseData(response.getContentAsString());
        System.out.println(result);
        System.out.println("-------------- Sign Up Successful -----------------");
        jwt = parseJwtHeader(response);
    }

    @Test
    @Order(2)
    public void signIn() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        SignInVo signInVo = new SignInVo("tourist", "321654987");
        String sender = mapper.writeValueAsString(signInVo);
        RequestBuilder request = post("/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(sender);
        System.out.println("-------------- HTTP Response -----------------");
        MockHttpServletResponse response = mvc.perform(request)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse();
        String receive = response.getContentAsString();
        System.out.println("-------------- Json Data -----------------");
        System.out.println(receive);
        Assertions.assertEquals(EResult.SUCCESS.getCode().intValue(), response.getStatus());
        System.out.println("-------------- Sign Up Successful -----------------");
    }

    @Test
    @Order(3)
    public void logout() throws Exception {
        RequestBuilder request = post("/logout").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").header("Authorization", "Bearer " + jwt);
        System.out.println("-------------- HTTP Response -----------------");
        MockHttpServletResponse response = mvc
                .perform(request)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse();
        String receive = response.getContentAsString();
        System.out.println("-------------- Json Data -----------------");
        System.out.println(receive);
        Assertions.assertEquals(EResult.SUCCESS.getCode().intValue(), response.getStatus());
        System.out.println("-------------- Sign Up Successful -----------------");
//        accountRepo.deleteByNameEquals("test");
    }
}