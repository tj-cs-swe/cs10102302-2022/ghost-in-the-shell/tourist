package top.tourist.backend.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import top.tourist.backend.model.dto.CalendarTodoDto;
import top.tourist.backend.model.vo.CalendarTodoVo;
import top.tourist.backend.model.vo.SignInVo;
import top.tourist.backend.utils.general.TimeUtils;
import top.tourist.backend.utils.network.HttpUtils;
import top.tourist.backend.utils.result.EResult;
import top.tourist.backend.utils.result.Result;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class CalendarControllerTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;
    private ObjectMapper mapper;

    private String jwt;
    private Long todoId;

    private Result<List<CalendarTodoDto>> parseResponseData(String body) throws JsonProcessingException {
        return mapper.readValue(body, new TypeReference<Result<List<CalendarTodoDto>>>() {
        });
    }

    private String parseJwtHeader(MockHttpServletResponse response) {
        String header = response.getHeader(HttpUtils.authHeader);
        try {

            header = header.split(" ")[1];
        } catch (Exception ex) {
            throw new RuntimeException();
        }
        return header;
    }

    private MockHttpServletResponse doRequest(String url, String requestType, String json, boolean isNeedAuth) throws Exception {
        MockHttpServletRequestBuilder request = ParseHttpRequestType(url, requestType, json);
        if (isNeedAuth) {
            request = request
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("UTF-8")
                    .header("Authorization", "Bearer " + jwt);
        } else {
            request = request
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("UTF-8");
        }
        System.out.println("-------------- HTTP Response -----------------");
        return mvc.perform(request)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse();
    }

    private MockHttpServletRequestBuilder ParseHttpRequestType(String url, String requestType, String json) {
        MockHttpServletRequestBuilder request;
        switch (requestType) {
            case "GET":
                request = get(url);
                break;
            case "POST":
                request = post(url).content(json);
                break;
            case "PUT":
                request = put(url).content(json);
                break;
            case "DELETE":
                request = org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .delete(url).param("id", json);
                break;
            default:
                throw new IllegalArgumentException(requestType);
        }
        return request;
    }

    private void VerifyAndPrintResponse(MockHttpServletResponse response) throws UnsupportedEncodingException {
        Assertions.assertEquals(EResult.SUCCESS.getCode().intValue(), response.getStatus());

        System.out.println("-------------- Json Data -----------------");
        String receive = response.getContentAsString();
        System.out.println(receive);
        System.out.println();
    }

    @BeforeEach
    void setUp() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        signIn();
    }

    private void signIn() throws Exception {
        SignInVo signInVo = new SignInVo("user", "321654987");
        String sender = mapper.writeValueAsString(signInVo);

        MockHttpServletResponse response = doRequest("/signin", "POST", sender, false);
        jwt = parseJwtHeader(response);

        VerifyAndPrintResponse(response);
    }

    @Order(1)
    @Test
    void add() throws Exception {
        LocalDateTime time = TimeUtils.getTime();
        CalendarTodoVo todo = new CalendarTodoVo(
                1L, "出游", "上海", "FREE",
                time, time, true, true);
        String sender = mapper.writeValueAsString(todo);
        MockHttpServletResponse response = doRequest("/calendar/add", "POST", sender, true);
        VerifyAndPrintResponse(response);
    }

    @Order(2)
    @Test
    void findAll() throws Exception {
        MockHttpServletResponse response = doRequest("/calendar/findAll", "GET", null, true);
        VerifyAndPrintResponse(response);
        String receive = response.getContentAsString();
        List<CalendarTodoDto> dtoList = parseResponseData(receive).getData();
        if (dtoList.size() < 1) {
            throw new IllegalArgumentException();
        }
        CalendarTodoDto dto = dtoList.get(0);
        todoId = dto.getId();
        System.out.println("------------ The new todo id is " + todoId);
        System.out.println();
    }

    @Order(3)
    @Test
    void modify() throws Exception {
        LocalDateTime time = TimeUtils.getTime();
        CalendarTodoVo todo = new CalendarTodoVo(
                todoId, "出差", "北京", "BUSY",
                time, time, false, true);
        String sender = mapper.writeValueAsString(todo);
        MockHttpServletResponse response = doRequest("/calendar/modify", "PUT", sender, true);
        VerifyAndPrintResponse(response);
    }

    @Order(4)
    @Test
    void delete() throws Exception {
        MockHttpServletResponse response = doRequest("/calendar/delete", "DELETE", todoId.toString(), true);
        VerifyAndPrintResponse(response);
    }
}