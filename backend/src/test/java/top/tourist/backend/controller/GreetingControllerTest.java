package top.tourist.backend.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import top.tourist.backend.repo.AccountRepo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class GreetingControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private AccountRepo accountRepo;

    private MockMvc mvc;
    private final ObjectMapper mapper = new ObjectMapper();
    private String jwt = "";

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void greeting() throws Exception {
//        RequestBuilder request = get("/greeting/wayne").contentType(MediaType.APPLICATION_JSON)
//                .characterEncoding("UTF-8").header("Authorization", "Bearer ");
        RequestBuilder request = post("/greeting/wayne").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").header("Authorization", "Bearer " + jwt);
        String receive = mvc.perform(request).andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString();
        System.out.println(receive);
    }
}