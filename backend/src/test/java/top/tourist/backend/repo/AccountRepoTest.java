package top.tourist.backend.repo;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.tourist.backend.model.entity.account.Account;

import java.util.List;

@SpringBootTest
class AccountRepoTest {
    @Autowired
    private AccountRepo accountRepo;

    @Test
    void accountFindAllTest() {

        List<Account> list = accountRepo.findAll();
        for (Account account : list) {
            System.out.println(account);
        }
    }
}