package top.tourist.backend.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.tourist.backend.model.entity.account.Account;
import top.tourist.backend.model.entity.account.AuthSession;
import top.tourist.backend.model.enumerate.EAccountRole;
import top.tourist.backend.utils.general.TimeUtils;

import java.util.Optional;

@SpringBootTest
public class AuthSessionRepoTest {
    @Autowired
    AuthSessionRepo sessionRepo;

    @Autowired
    AccountRepo accountRepo;

    @Test
    public void sessionAccountLinkTest() {
        String testName = "bear";
        Long testAccountId = 456L;
        Long testSessionId = 789L;
        accountRepo.deleteByNameEquals(testName);
        sessionRepo.deleteById(testSessionId);

        Account account = new Account(testAccountId, testName, "48161639",
                EAccountRole.ADMIN, false, TimeUtils.getTime());
        accountRepo.save(account);

        AuthSession session = new AuthSession(testSessionId, testAccountId, "123",
                TimeUtils.getSessionExpireTime(), false);
        sessionRepo.save(session);

        Optional<AuthSession> optional = sessionRepo.findById(789L);
        if (optional.isEmpty()) {
            System.out.println("null session");
        } else {
            System.out.println(optional.get().getAccount());
        }
    }
}