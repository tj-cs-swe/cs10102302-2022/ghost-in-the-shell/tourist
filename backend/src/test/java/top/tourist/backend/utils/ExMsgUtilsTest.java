package top.tourist.backend.utils;

import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import top.tourist.backend.model.vo.SignUpVo;
import top.tourist.backend.utils.general.ExMsgUtils;

public class ExMsgUtilsTest {
    @Test
    public void TraceNameTest() {
        System.out.println(ExMsgUtils.GetMessage("test"));
    }

    @Test
    public void TestData() throws JsonProcessingException {
        SignUpVo signUpVo = new SignUpVo("test", "321654987", "ADMIN");
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();

        String json = mapper.writeValueAsString(signUpVo);
        System.out.println(json);
    }
}