package top.tourist.backend.utils;

import org.junit.jupiter.api.Test;
import top.tourist.backend.utils.data.SnowFlakeUtils;

import java.util.Set;
import java.util.TreeSet;

class SnowFlakeGeneratorTest {

    @Test
    void nextId() {
        SnowFlakeUtils idGenerator = new SnowFlakeUtils(1, 2);
        Set<Long> idSet = new TreeSet<>();
        for(int i = 0; i < 50; i++) {
            idSet.add(idGenerator.NextId());
        }
        System.out.printf("Target Set Size : 50, Current Set Size %d\n", idSet.size());
        System.out.println(idSet);

        long startTime = System.currentTimeMillis();
        for(int i = 0; i < 1000000; i++) {
            idGenerator.NextId();
        }
        System.out.printf("1 Million Time Consuming : %d\n", System.currentTimeMillis() - startTime);
    }
}