# Week 6~7 Back Task



### 1. 商讨具体的开发环境设置

- `IDE` 环境同步
- 本地 / 远程调试配置
- 远程部署环境配置
- CI / CD 初步设置



### 2. 初步学习 `Spring` 全家桶，能够建立简单的 `RESTful` 网页应用

- 参考本目录中的 `Backend Example` 项目



### 3. 构建**账户系统**，包括**用户与商家的账户**数据库 `scheme`，完成初始化 `sql` 文件(Important)



### 4. 确定主页与登录注册的 `RESTful API`  (Important)



### 5 . 实现登录注册的业务逻辑（Important）



### 6. 解决跨域问题



### 7 建立初步 CI / CD 工作流

- 尤其是确定 `Docker` 虚拟化的范围与配置