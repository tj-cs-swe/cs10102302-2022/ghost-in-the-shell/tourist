# milestone 3
milestone 3 的汇报日期为15周
交付时间未知
考虑到部署联调，请尽快完成，最晚14周周六进行联调

## 旅游论坛
wjj

## 管理员部分
商家审核、普通用户审核、数据查看

部分用户故事

zmx,gyt

## 购票

用户故事+开发+联调
szm

# token使用
token存储在缓存```user_token```中，使用axios获得后端信息时携带在header中即可，使用方法参照下图
![](token1.png)
用户名存储在缓存```nickname```中
登录成功自动更新缓存
<b>注意</b> 不要新开页面，缓存会丢失
如论坛需要多个界面，建议设置自己的顶部导航栏，具体可参考``` tourist\frontend\src\components\Navbars\BusinessNavbar.js```


# axios使用
可参考
```tourist\frontend\src\components\business\info\table.js```