# week7 8 front task

## 均为个人任务

![](week78-1.png)

在Landing页面，点击同路er下面的对应内容跳转到各自负责部分

Landing的Navbar设计暂不修改

若个人负责部分没有写用户故事，自行添加至issue

![](week78-2.png)
每人负责部分以component形式添加到App.js中，格式参考模板文件

模板中对css class中进行了重命名，建议沿用模板命名，参考
https://tailwindcss.com/docs/margin#add-margin-to-a-single-side

若有css自行定义，放在对应component下，以防止格式冲突，参考statusCard.js

### srs3.2.2
szm

### srs3.2.3
wjj：景点查询 
gyt：路线查询+最优路线规划
zmx: 天气查询

调用高德等网站API完成

### 关于API调试
前端若进展较快，可先自行规定API请求数据格式，srs3.2.3中应该仅前端即可完成功能
将需要后端提供的API写在issue中
+ [API 管理平台 yapi](http://yapi.smart-xwork.cn/)
> 在与后端进展不一致的情况下,可以借助yapi进行调试

### 关于外部包安装
若npm安装新包，请在issue中说明

### 个人进展说明
szm：使用日历插件 
安装：
npm install --save tui-calendar

gyt: 使用安装包
安装：
npm install @mui/material @emotion/react @emotion/styled

zmx:
npm install --save redux react-redux

wjj:
cd frontend
npm install (npm install --force)
npm install --save reactstrap
npm start

版本修改
    在 DefaultFooter.js 中增加景点信息查询链接跳转
    在 pages 目录下新建 Spot.js 文件作为景点查询主页面结构
    在 components 目录下新建 spot 文件目录作为景点信息查询模块组件的顶层目录
    在 App.js 中 引入 /pages/Spot.js 并且 增加对文件目录 /spot 的路径定向
    添加 Bootstrap 组件库 (建议利用新的 package.son 直接 install)
        使用命令 npm install --save reactstrap bootstrap@4
        参考 https://madewith.cn/215
    在 https://heroicons.com/ 寻找可使用的图标
    在 img 文件中添加了一些图片
    在 src 目录下加入 views 文件夹, 在 components 目录下加入 Headers Navbars Footers 文件夹, 提供一些组件调用模版

待处理问题
    Carousel 组件未对图片大小进行限制，考虑之后加入裁剪器，商家上传图像的时候裁剪，保证之后前端页面展示图片的大小一致
    图片接口目前是写死的，之后根据后端情况完成接口
    搜索窗口的回车事件
    