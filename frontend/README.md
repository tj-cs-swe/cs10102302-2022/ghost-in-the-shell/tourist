# 安装
```
npm install
```
> 可能会有许多wrong和warning，可忽略

# 运行
```
npm run
```
> 第一次网页加载比较慢，正常运行在http://localhost:3000/
> 以后部署在服务器上

文件比较大，500+Mb, 暂不在gitlab上传node_modules文件夹，可以直接针对模板进行修改
