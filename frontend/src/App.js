import { Switch, Route, Redirect } from 'react-router-dom';
import Landing from 'pages/Landing';
import Profile from 'pages/Profile';
import Login from 'pages/Login';
import Register from 'pages/Register';
import Calendar from 'pages/Calendar';
import Weather from 'pages/Weather';
import Line from 'pages/Line';
import Spot from 'pages/Spot';
import BusCredit from 'pages/business/BusCredit';
import BusInfo from 'pages/business/BusInfo';
import BusProfile from 'pages/business/BusProfile';
import Shopping from 'pages/Shopping';

import Forum from 'pages/Forum';
import Passage from 'pages/Passage';
import Edit from 'pages/Edit';
import AdminMain from 'pages/admin/AdminMain';
import AdminLabel from 'pages/admin/AdminLabel';
import AdminUser from 'pages/admin/AdminUser';
// Font Awesome Style Sheet
import '@fortawesome/fontawesome-free/css/all.min.css';

// Tailwind CSS Style Sheet
import 'assets/styles/tailwind.css';

function App() {
    return (
        <Switch>
            <Route exact path="/" component={Landing} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/calender" component={Calendar} />
            <Route exact path="/weather" component={Weather} />
            <Route exact path="/line" component={Line} />
            <Route exact path="/spot" component={Spot} />
            <Route exact path="/busInfo" component={BusInfo} />
            <Route exact path="/busCredit" component={BusCredit} />
            <Route exact path="/busProfile" component={BusProfile} />
            <Route exact path="/adminMain" component={AdminMain} />
            <Route exact path="/adminUser" component={AdminUser}/>
            <Route exact path="/shopping" component={Shopping} />
            <Route exact path="/adminLabel" component={AdminLabel} />
            
            <Route exact path="/forum" component={Forum} />
            <Route exact path="/passage" component={Passage} />
            <Route exact path="/edit" component={Edit} />
            <Redirect from="*" to="/" />
        </Switch>
    );
}

export default App;
