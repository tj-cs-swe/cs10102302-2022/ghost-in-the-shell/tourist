import H5 from '@material-tailwind/react/Heading5';
import LeadText from '@material-tailwind/react/LeadText';
import Icon from '@material-tailwind/react/Icon';
import { Link } from 'react-router-dom';
import NavLink from '@material-tailwind/react/NavLink'; 

export default function DefaultFooter() {
    return (
        <>
            <footer className="relative bg-gray-100 pt-8 pb-6">
                <hr className="my-1 border-gray-300" />
                <div className="container max-w-7xl mx-auto px-4">
                    <div className="flex flex-wrap text-center lg:text-left pt-6">
                        <div className="w-full lg:w-6/12 px-4">
                            <H5 color="gray">美好出游即刻开始</H5>
                            {/* <div className="-mt-4">
                                <LeadText color="blueGray">
                                    Easy to use React components for Tailwind
                                    CSS and Material Design.
                                </LeadText>
                            </div>
                            <div className="flex gap-2 mt-6 md:justify-start md:mb-0 mb-8 justify-center">
                                <a
                                    href="https://www.facebook.com/CreativeTim"
                                    className="grid place-items-center bg-white text-blue-600 shadow-md font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <Icon
                                        family="font-awesome"
                                        name="fab fa-facebook-square"
                                    />
                                </a>
                                <a
                                    href="https://www.twitter.com/creativetim"
                                    className="grid place-items-center bg-white text-blue-400 shadow-md font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <Icon
                                        family="font-awesome"
                                        name="fab fa-twitter"
                                    />
                                </a>
                                <a
                                    href="https://www.instagram.com/creativetimofficial/"
                                    className="grid place-items-center bg-white text-indigo-500 shadow-md font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <Icon
                                        family="font-awesome"
                                        name="fab fa-instagram"
                                    />
                                </a>
                                <a
                                    href="https://www.dribbble.com/creativetim"
                                    className="grid place-items-center bg-white text-pink-400 shadow-md font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <Icon
                                        family="font-awesome"
                                        name="fab fa-dribbble"
                                    />
                                </a>
                                <a
                                    href="https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w"
                                    className="grid place-items-center bg-white text-red-600 shadow-md font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <Icon
                                        family="font-awesome"
                                        name="fab fa-youtube"
                                    />
                                </a>
                                <a
                                    href="https://github.com/creativetimofficial"
                                    className="grid place-items-center bg-white text-gray-900 shadow-md font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <Icon
                                        family="font-awesome"
                                        name="fab fa-github"
                                    />
                                </a>
                            </div> */}
                        </div>
                        <div className="w-full lg:w-6/12 px-4">
                            <div className="flex flex-wrap items-top">
                                <div className="w-full lg:w-4/12 px-4  md:mb-0 mb-8">
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                        <b>同路er</b>
                                    </span>
                                    <ul className="list-unstyled mt-3">
                                        <li>
                                            <Link to="/calender"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                我的行程
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/line"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                路线查询
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/weather"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm"
                                            >
                                                天气查询
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/spot"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                景点信息查询
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/shopping"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                票务服务
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/forum"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                旅游论坛
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                <div className="w-full lg:w-4/12 px-4">
                                    <span className="text-gray-900 text-base font-serif font-medium mb-5">
                                        <b>商家</b>
                                    </span>
                                    <ul className="list-unstyled mt-3">
                                        <li>
                                            <Link to="/busInfo"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                商品管理
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/busCredit"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                资质查看
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                <div className="w-full lg:w-4/12 px-4">
                                    <span className="text-gray-900 text-base font-serif font-medium mb-5">
                                        <Link
                                            to="/adminMain"
                                        >
                                            <div>管理员</div>
                                        </Link>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr className="my-6 border-gray-300" />
                    <div className="flex flex-wrap items-center md:justify-between justify-center">
                        <div className="w-full md:w-4/12 px-4 mx-auto text-center">
                            <div className="text-sm text-gray-700 font-medium py-1">
                                Copyright © {new Date().getFullYear()} &nbsp;
                                TONGLU by{' '}
                                <a
                                    href="https://gitlab.com/tj-cs-swe/cs10102302-2022/ghost-in-the-shell/"
                                    className="text-gray-700 hover:text-gray-900 transition-all"
                                >
                                    攻壳机动队
                                </a>
                                .
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
}
