import { useState } from 'react';
import { Link } from 'react-router-dom';
import Navbar from '@material-tailwind/react/Navbar';
import NavbarContainer from '@material-tailwind/react/NavbarContainer';
import NavbarWrapper from '@material-tailwind/react/NavbarWrapper';
import NavbarBrand from '@material-tailwind/react/NavbarBrand';
import NavbarToggler from '@material-tailwind/react/NavbarToggler';
import NavbarCollapse from '@material-tailwind/react/NavbarCollapse';
import Nav from '@material-tailwind/react/Nav';
import NavLink from '@material-tailwind/react/NavLink';
import Dropdown from '@material-tailwind/react/Dropdown';
import DropdownItem from '@material-tailwind/react/DropdownItem';
import Icon from '@material-tailwind/react/Icon';
import Button from '@material-tailwind/react/Button';


export default function DefaultNavbar() {

    const nickname = ((window.sessionStorage.getItem('nickname')==null||window.sessionStorage.getItem('nickname')=='undefined')?
    '登录':window.sessionStorage.getItem('nickname'))
    const mypage = !!(window.sessionStorage.getItem('user_type')==='USER')

    return (
        <Navbar color="transparent" navbar>
            <NavbarContainer>
                <NavbarWrapper>
                    <Link to="/">
                        <NavbarBrand>同路 tonglu</NavbarBrand>
                    </Link>
                </NavbarWrapper>

                <NavbarCollapse >
                    <Nav>
                        <div className="flex flex-col z-50 lg:flex-row lg:items-center">
                            <NavLink
                                href="/calender"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="description" size="2xl" />
                                <div style={{ fontSize: 14 }}>行程日历</div>
                            </NavLink>
                            <div className="text-white">
                                <Dropdown
                                    color="transparent"
                                    size="sm"
                                    buttonType="link"
                                    buttonText={
                                        <div className="py-2.5 font-medium flex items-center">
                                            <Icon
                                                name="search"
                                                size="2xl"
                                                color="white"
                                            />
                                            <span className="ml-2">
                                                信息查询
                                            </span>
                                        </div>
                                    }
                                    ripple="light"
                                >
                                    <Link to="/line">
                                        <DropdownItem color="lightBlue">
                                            路线查询
                                        </DropdownItem>
                                    </Link>
                                    <Link to="/weather">
                                        <DropdownItem color="lightBlue">
                                            天气查询
                                        </DropdownItem>
                                    </Link>
                                    <Link to="/spot">
                                        <DropdownItem color="lightBlue">
                                            景点查询
                                        </DropdownItem>
                                    </Link>
                                </Dropdown>
                            </div>
                            <NavLink
                                href="/shopping"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="apps" size="2xl" />
                                <div style={{ fontSize: 14 }}>票务服务</div>
                            </NavLink>
                            <NavLink
                                href="/forum"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon  name="comment" size="2xl" />
                                <div style={{ fontSize: 14 }}>旅游论坛</div>
                            </NavLink>
                            {mypage&&(<NavLink
                                href="/profile"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon  name="home" size="2xl" />
                                <div style={{ fontSize: 14 }}>我的主页</div>
                            </NavLink>)}
                            <Link to="/login">
                                <Button
                                    color="transparent"
                                    className="bg-white text-black ml-4"
                                    ripple="dark"
                                >
                                    {nickname}
                                </Button>
                            </Link>
                        </div>
                    </Nav>
                </NavbarCollapse>
            </NavbarContainer>
        </Navbar>
    );
}
