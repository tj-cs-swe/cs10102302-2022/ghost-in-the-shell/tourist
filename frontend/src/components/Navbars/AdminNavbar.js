import { useState } from 'react';
import { Link } from 'react-router-dom';
import Navbar from '@material-tailwind/react/Navbar';
import NavbarContainer from '@material-tailwind/react/NavbarContainer';
import NavbarWrapper from '@material-tailwind/react/NavbarWrapper';
import NavbarBrand from '@material-tailwind/react/NavbarBrand';
import NavbarCollapse from '@material-tailwind/react/NavbarCollapse';
import NavLink from '@material-tailwind/react/NavLink';
import Nav from '@material-tailwind/react/Nav';
import Icon from '@material-tailwind/react/Icon';
import Button from '@material-tailwind/react/Button';


export default function AdminNavbar() {

    const nickname = ((window.sessionStorage.getItem('nickname')==null||window.sessionStorage.getItem('nickname')=='undefined')?
    '登录':window.sessionStorage.getItem('nickname'))
    return (
        <Navbar color="transparent" navbar>
            <NavbarContainer>
                <NavbarWrapper>
                    <Link to="/">
                        <NavbarBrand>同路 tonglu</NavbarBrand>
                    </Link>
                </NavbarWrapper>

                <NavbarCollapse >
                    <Nav>
                        <div className="flex flex-col z-50 lg:flex-row lg:items-center">
                           
                            <NavLink
                                href="/adminMain"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="apps" size="2xl" />
                                <div style={{ fontSize: 14 }}>商家管理</div>
                            </NavLink>
                            <NavLink
                                href="/adminUser"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="apps" size="2xl" />
                                <div style={{ fontSize: 14 }}>用户管理</div>
                            </NavLink>
                            <NavLink
                                href="/adminLabel"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="apps" size="2xl" />
                                <div style={{ fontSize: 14 }}>标签管理</div>
                            </NavLink>
                            <NavLink
                                href="/forum"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon  name="comment" size="2xl" />
                                <div style={{ fontSize: 14 }}>旅游论坛</div>
                            </NavLink>
                            <Link to="/login">
                                <Button
                                    color="transparent"
                                    className="bg-white text-black ml-4"
                                    ripple="dark"
                                >
                                    {nickname}
                                </Button>
                            </Link>
                        </div>
                    </Nav>
                </NavbarCollapse>
               
            </NavbarContainer>
        </Navbar>
    );
}
