import { useState } from 'react';
import { Link } from 'react-router-dom';
import Navbar from '@material-tailwind/react/Navbar';
import NavbarContainer from '@material-tailwind/react/NavbarContainer';
import NavbarWrapper from '@material-tailwind/react/NavbarWrapper';
import NavbarBrand from '@material-tailwind/react/NavbarBrand';
import NavbarCollapse from '@material-tailwind/react/NavbarCollapse';
import NavLink from '@material-tailwind/react/NavLink';
import Nav from '@material-tailwind/react/Nav';
import Icon from '@material-tailwind/react/Icon';
import Button from '@material-tailwind/react/Button';


export default function BusinessNavbar() {

    const nickname = ((window.sessionStorage.getItem('nickname') == null || window.sessionStorage.getItem('nickname') == 'undefined') ?
        '登录' : window.sessionStorage.getItem('nickname'))
    return (
        <Navbar color="transparent" navbar>
            <NavbarContainer>
                <NavbarWrapper>
                    <Link to="/">
                        <NavbarBrand>同路 tonglu</NavbarBrand>
                    </Link>
                </NavbarWrapper>

                <NavbarCollapse >
                    <Nav>
                        <div className="flex flex-col z-50 lg:flex-row lg:items-center">
                            <NavLink
                                href="/busProfile"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="home" size="2xl" />
                                <div style={{ fontSize: 14 }}>我的主页</div>
                            </NavLink>

                            <NavLink
                                href="/busInfo"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="apps" size="2xl" />
                                <div style={{ fontSize: 14 }}>商品信息</div>
                            </NavLink>
                            <NavLink
                                href="/busCredit"
                                rel="noreferrer"
                                ripple="light"
                            >
                                <Icon name="apps" size="2xl" />
                                <div style={{ fontSize: 14 }}>资质查看</div>
                            </NavLink>

                            <Link to="/login">
                                <Button
                                    color="transparent"
                                    className="bg-white text-black ml-4"
                                    ripple="dark"
                                >
                                    {nickname}
                                </Button>
                            </Link>
                        </div>
                    </Nav>
                </NavbarCollapse>

            </NavbarContainer>
        </Navbar>
    );
}
