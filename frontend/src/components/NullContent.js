//此页是空白页，在无权限时render

import H6 from '@material-tailwind/react/Heading6';

export default function NullContent({msg}) {
    return (
        <section className="relative py-16 bg-gray-100">
            <div className="container max-w-7xl px-4 mx-auto">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                    <div className="px-6 mt-10">
                       
                        <div className="text-center my-8">
                            <H6 color="gray">{msg}</H6>
                        </div>

                        <div className="mb-10 py-2 border-t border-gray-200 text-center">
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
