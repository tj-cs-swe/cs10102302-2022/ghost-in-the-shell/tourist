import H5 from '@material-tailwind/react/Heading5';
import LeadText from '@material-tailwind/react/LeadText';
import Icon from '@material-tailwind/react/Icon';
import { Link } from 'react-router-dom';

export default function Footer() {
    return (
        <>
            <footer className="relative bg-gray-100 pt-8 pb-6">
                {/* <hr className="my-1 border-gray-300" /> */}
                <div className="container max-w-7xl mx-auto px-4">
                    {/* <div className="flex flex-wrap text-center lg:text-left pt-6">
                        <div className="w-full lg:w-6/12 px-4">
                            <H5 color="gray">美好出游即刻开始</H5>
                        </div>
                        <div className="w-full lg:w-6/12 px-4">
                            <div className="flex flex-wrap items-top">
                                <div className="w-full lg:w-4/12 px-4  md:mb-0 mb-8">
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                        <b>同路er</b>
                                    </span>
                                    <ul className="list-unstyled ">
                                        <li>
                                            <Link to="/calender"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                我的行程
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/spot"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                景点信息查询
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/weather">
                                            <a
                                                // href="https://www.creative-tim.com/presentation?ref=mtk"
                                                target="_blank"
                                                rel="noreferrer"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm"
                                            >
                                                天气查询
                                            </a>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/line"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">
                                                路线查询
                                            </Link>
                                        </li>
                                        <li>
                                            <a
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm"
                                                target="_blank"
                                                href=" https://www.creative-tim.com/blog?ref=mtk"
                                            >
                                                票务服务
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href="https://www.github.com/creativetimofficial?ref=mtk"
                                                target="_blank"
                                                rel="noreferrer"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm"
                                            >
                                                旅游论坛
                                            </a>
                                        </li>                                        
                                    </ul>
                                </div>
                                <div className="w-full lg:w-4/12 px-4">
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2">
                                        <b>商家</b>
                                    </span>
                                    <ul className="list-unstyled">
                                        <li>
                                            <a
                                                href="https://github.com/creativetimofficial/material-tailwind/blob/main/LICENSE.md?ref=mtk"
                                                target="_blank"
                                                rel="noreferrer"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm"
                                            >
                                                商品管理
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href="https://github.com/creativetimofficial/material-tailwind/blob/main/CONTRIBUTING.md?ref=mtk"
                                                target="_blank"
                                                rel="noreferrer"
                                                className="text-gray-700 hover:text-gray-900 block pb-2 text-sm"
                                            >
                                                资质查看
                                            </a>
                                        </li>                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> */ }
                    {/* <hr className="my-6 border-gray-300" /> */}
                    <div className="flex flex-wrap items-center md:justify-between justify-center">
                        <div className="w-full md:w-4/12 px-4 mx-auto text-center">
                            <div className="text-sm text-gray-700 font-medium py-1">
                                Copyright © {new Date().getFullYear()} &nbsp;
                                TONGLU by{' '}
                                <a
                                    href="https://gitlab.com/tj-cs-swe/cs10102302-2022/ghost-in-the-shell/"
                                    className="text-gray-700 hover:text-gray-900 transition-all"
                                >
                                    攻壳机动队
                                </a>
                                .
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
}
