import React, { useRef, useEffect } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContentText from '@mui/material/DialogContentText';
import { Typography } from 'antd';
import axios from 'axios';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import { Url } from '../../../index'
//import axios from 'axios';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

export default function ModifyDialog(props) {
    const [bstate,setBstate]=React.useState(props.bstate);
    const [buttonShow,setBshow]=React.useState(props.buttonShow);
    const [stateArr,setStateA]=React.useState("");
    const [stateBrr,setStateB]=React.useState("");
    //setBstate(props.bstate);
    //bstate=props.bstate;
    // const [bname, setbName] = React.useState(props.bname);
    // const [blocal, setBlocal] = React.useState(0);
    // const [number_in_storehouse, setNumber_in_storehouse] = React.useState(0);
    // const [price, setPrice] = React.useState(0);
    // const [remarks, setRemarks] = React.useState('');
    // console.log('props:'+props.bname)
    // useEffect(async () => {
    //     if (props.id) {
    //         await axios.get(Url + "/product" + "/findById/" + props.bname)
    //             .then(function (response) {
    //                 setBlocal(response.data.blocal);
    //             })
    //             .catch(function (error) {
    //                 console.log(error);
    //             });
    //     }
    // }, [props]);

    useEffect(async () => {
        console.log(props.bstate);
        if(props.bstate=="checking"){
            setStateA("同意申请并发送合同");
            setStateB("驳回审核");
        }
        else if(props.bstate=="exit_checking"){
            setStateA("同意退驻");
            setStateB("驳回退驻申请");
        }
        else if(props.bstate=="rechecking"){
            setStateA("同意审核信息修改");
            setStateB("驳回修改");
        }
        
    }, [props]);

    const handleClose = () => {
        props.setOpen(false);
    };

    const reject = () => {
        //props.bname
        //props.bstate
        //props.setOpen(false);
        console.log(props.bstate);
        let tempName=props.bname;
        let tempState;
        console.log(tempName);
        if(props.bstate=="checking"){
            tempState="reject_checking";
        }
        else if(props.bstate=="exit_checking"){
            tempState="reject_exit";
        }
        else if(props.bstate=="rechecking"){
            tempState="reject_rechecking";
        }

        axios.put(Url+"/admin/changeaduit",null,
        {
            timeout:20000,
                headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                },
                params: {
                    "name":tempName,
                    "state":tempState
                }
        })
            .then(function(response){
                console.log(response.data)
                if(response.data.data==0){
                    if(props.bstate=="checking"){
                        alert("已驳回审核！");
                    }
                    else if(props.bstate=="exit_checking"){
                        alert("已驳回退驻申请！");
                    }
                    else if(props.bstate=="rechecking"){
                        alert("已驳回审核修改！");
                    }
                }
                else{
                    if(props.bstate=="checking"){
                        alert("驳回审核失败！");
                    }
                    else if(props.bstate=="exit_checking"){
                        alert("驳回退驻申请失败！");
                    }
                    else if(props.bstate=="rechecking"){
                        alert("驳回审核修改失败！");
                    }
                }
            })

            .catch(function(error){
                console.log(error);
                if(props.bstate=="checking"){
                    alert("驳回审核失败！");
                }
                else if(props.bstate=="exit_checking"){
                    alert("驳回退驻申请失败！");
                }
                else if(props.bstate=="rechecking"){
                    alert("驳回审核修改失败！");
                }
            });
        
        props.setOpen(false);
        //console.log("shihsi");
        //props.refresh();
    };

    const sendout = () =>{
        //props.bname
        //props.bstate
        //props.setOpen(false);
        console.log(props.bstate);
        let tempName=props.bname;
        let tempState;
        if(props.bstate=="checking"){
            tempState="pass";
        }
        else if(props.bstate=="exit_checking"){
            tempState="exit_pass";
        }
        else if(props.bstate=="rechecking"){
            tempState="success";
        }

        axios.put(Url+"/admin/changeaduit",null,
        {
            timeout:20000,
                headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                },
                params: {
                    "name":tempName,
                    "state":tempState
                }
        })
            .then(function(response){
                console.log(response.data)
                if(response.data.data==0){
                    if(props.bstate=="checking"){
                        alert("同意申请成功！");
                    }
                    else if(props.bstate=="exit_checking"){
                        alert("同意退驻成功！");
                    }
                    else if(props.bstate=="rechecking"){
                        alert("同意审核信息修改成功！");
                    }
                }
                else{
                    if(props.bstate=="checking"){
                        alert("同意申请失败！");
                    }
                    else if(props.bstate=="exit_checking"){
                        alert("同意退驻失败！");
                    }
                    else if(props.bstate=="rechecking"){
                        alert("同意审核信息修改失败！");
                    }
                }
            })

            .catch(function(error){
                console.log(error);
                if(props.bstate=="checking"){
                    alert("同意申请失败！");
                }
                else if(props.bstate=="exit_checking"){
                    alert("同意退驻失败！");
                }
                else if(props.bstate=="rechecking"){
                    alert("同意审核信息修改失败！");
                }
            });
        
        props.setOpen(false);
        //console.log("shihsi");
        //props.refresh();
    }

    return (
        <>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={props.open}                          
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    店铺详情
                </BootstrapDialogTitle>
                {/* <Dialog open={props.open} onClose={handleClose}>                 */}
                <DialogContent dividers>
                    <Typography gutterBottom > 
                        店铺名称：{props.bname}
                    </Typography>
                    <Typography gutterBottom>
                        地理位置：{props.blocal}
                    </Typography>
                    <Typography gutterBottom>
                        店铺网站：{props.bwebsite}
                    </Typography>
                    <Typography gutterBottom>
                        店铺联系人：{props.bcontact}
                    </Typography>
                    <Typography gutterBottom>
                        店铺联系方式：{props.bphone}
                    </Typography>
                    <Typography gutterBottom>
                        合作银行：{props.bbank}
                    </Typography>
                    <Typography gutterBottom>
                        银行账户：{props.baccount}
                    </Typography>
                    <Typography gutterBottom>
                        店铺介绍：{props.bintroduce}
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <div style={{ display:props.buttonShow}}>
                        <Button onClick={reject}>{stateBrr}</Button>
                    </div>
                    <div style={{ display:props.buttonShow}}>
                        <Button onClick={sendout}>{stateArr}</Button>
                    </div>
                </DialogActions>
            {/* </Dialog> */}
            </BootstrapDialog>
        </>
    );
}