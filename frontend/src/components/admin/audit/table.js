import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { TableHead } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import { Typography } from 'antd';
import ModifyDialog from './dialog';
import { Url } from '../../../index'
import axios from 'axios';


function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

function createData( bname,blocal,bwebsite,bcontact,bphone,bbank,baccount,bintroduce,bstate ) {
    let change_state;
    if(bstate=="unsubmit"){change_state="未提交审核";}
    else if(bstate=="reject_checking"){change_state="已驳回审核申请";}
    else if(bstate=="checking"){change_state="等待审核";}
    else if(bstate=="pass"){change_state="审核通过";}
    else if(bstate=="success"){change_state="成功签约";}
    else if(bstate=="reject_exit"){change_state="已驳回退驻申请";}
    else if(bstate=="exit_checking"){change_state="等待退驻审核";}
    else if(bstate=="exit_pass"){change_state="已退驻";}
    else if(bstate=="rechecking"){change_state="等待重新审核"}
    else if(bstate=="reject_rechecking"){change_state="已驳回重新审核申请"}
    return { bname,blocal,bwebsite,bcontact,bphone,bbank,baccount,bintroduce,bstate,change_state };
}

const columns = [
    { id: 'bname', label: '店铺名称', minWidth: 170, align: 'center' },
    { id: 'blocal', label: '地理位置', minWidth: 170, align: 'center' },
    { id: 'bwebsite', label: '店铺网站', minWidth: 170, align: 'center' },
    { id: 'bcontact', label: '店铺联系人', minWidth: 170, align: 'center' },
    { id: 'bphone', label: '店铺联系方式', minWidth: 170, align: 'center' },
    { id: 'bbank', label: '合作银行', minWidth: 170, align: 'center' },
    { id: 'baccount', label: '银行账户', minWidth: 170, align: 'center' },
    { id: 'bintroduce', label: '店铺介绍', minWidth: 170, align: 'center' },
    { id: 'bstate', label: '状态', minWidth: 170, align: 'center' },
];

// const rows=[
//     createData('汪汪队','汪汪省汪汪市汪汪学校','https://wangwangdui.com','汪汪','13866668888','中国工商银行','6354345654','汪汪队赚大钱！','同意并发送合同'),
//     createData('33333','33333','33333','33333','33333','33333','33333','33333','同意并发送合同'),
//     createData('22222','22222','22222','22222','22222','22222','22222','22222','同意并发送合同'),
//     createData('55555','22222','22222','22222','22222','22222','22222','22222','退驻申请'),
//     createData('00000','22222','22222','22222','22222','22222','22222','22222','退驻申请'),
//     createData('77777','22222','22222','22222','22222','22222','22222','22222','同意并发送合同'),
// ];

export default function BusinessAuditTable(props) {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [rows, setRows] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [bname,setBname] = React.useState('');
    const [blocal,setBlocal] =React.useState('');
    const [bwebsite,setBwebsite]=React.useState('');
    const [bcontact,setBcontact]=React.useState('');
    const [bphone,setBphone]=React.useState('');
    const [bbank,setBbank]=React.useState('');
    const [baccount,setBaccount]=React.useState('');
    const [bintroduce,setBintroduce]=React.useState('');
    const [bstate,setBstate]=React.useState('');
    const [buttonShow,setBshow]=React.useState('');

    const renewData=(allData)=>{
        //console.log(allData[0].name);
        let rows=[];
        for(let i=0;i<allData.length;i++){
            rows.push(createData(allData[i].name,allData[i].location,allData[i].website,allData[i].contact_name,allData[i].contact_num,allData[i].bank_name,allData[i].bank_account,allData[i].intro,allData[i].state));
        }
        //rows.push(createData('33333','33333','33333','33333','33333','33333','33333','33333',"checking"));
        //rows.push(createData('444','444','444','444','444','444','444','33333',"exit_checking"));       
        setRows(rows);
    }
    //
    useEffect(async()=>{
        await axios.get(Url+"/admin"+"/getaduit",{
            timeout:10000,
            headers:{
                Authorization: window.sessionStorage.getItem('user_token')
                
            }
        })
            .then(function (response) {
                console.log(response.data.data);
                //console.log(response.data);
                //函数
                renewData(response.data.data);
            })
            .catch(function(error){
                //console.log("错误");
                console.log(error);
            });
    },[]);

    const handleClose = () => {
        setOpen(false);
    };

    const handleToggle = (name,local,website,contact,phone,bank,account,introduce,state) => {
        setBname(name);
        setBlocal(local);
        setBwebsite(website);
        setBcontact(contact);
        setBphone(phone);
        setBbank(bank);
        setBaccount(account);
        setBintroduce(introduce);
        setBstate(state);
        // console.log('name:'+name);
        // console.log('bname:'+bname);
        setOpen(!open);
        //
        if(state=="checking"||state=="exit_checking" || state=="rechecking"){
            setBshow('');
        }
        else{
            setBshow('none');
        }   
        //setBshow('');//记得注释
    };

    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const refresh=()=>{
        //console.log("test");
        axios.get(Url+"/admin"+"/getaduit",{
            timeout:10000,
            headers:{
                Authorization: window.sessionStorage.getItem('user_token')
                
            }
        })
            .then(function (response) {
                console.log(response.data.data);
                //console.log(response.data);
                //函数
                renewData(response.data.data);
            })
            .catch(function(error){
                //console.log("错误");
                console.log(error);
            });
    }

    return (
        <>
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">
                            <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                <b>店铺名称</b>
                            </span>
                        </TableCell>
                        <TableCell align="center">
                            <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                <b>地理位置</b>
                            </span>
                        </TableCell>
                        <TableCell align="center">
                            <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                <b>店铺网站</b>
                            </span>
                        </TableCell>
                        <TableCell align="center">
                            <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                <b>审核状态</b>
                            </span>
                        </TableCell>
                        <TableCell align="center">
                            <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                <b>详细信息</b>
                            </span>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align="right"></TableCell>
                        <TableCell align="right"></TableCell>
                        <TableCell align="right"></TableCell>
                        <TableCell align="right"></TableCell>
                        <TableCell align="right">注册商家总数：{rows.length}</TableCell>
                    </TableRow>
                </TableHead>
                
                <TableBody>
                    {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows
                    ).map((row) => (
                        <TableRow key={row.bname}>
                            <TableCell align="center">
                                {row.bname}
                            </TableCell>
                            <TableCell align="center">
                                {row.blocal}
                            </TableCell>
                            <TableCell align="center">
                                {row.bwebsite}
                            </TableCell >
                            <TableCell align="center">
                                {row.change_state}
                            </TableCell >
                            <TableCell align="center">
                                <Button onClick={()=>{handleToggle(row.bname,row.blocal,row.bwebsite,row.bcontact,row.bphone,row.bbank,row.baccount,row.bintroduce,row.bstate)}}>详情</Button>                               
                            </TableCell>                            
                        </TableRow>
                    ))}

                    {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6} />
                        </TableRow>
                    )}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                            colSpan={5}
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: {
                                    'aria-label': 'rows per page',
                                },
                                native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                            labelRowsPerPage="每页数量"
                        />
                    </TableRow>
                </TableFooter>               
            </Table>
        </TableContainer>
        <div style={{marginTop: "20px"}} align="right">                                   
            <Button variant="contained" onClick={refresh}>刷新商家审核列表</Button>
        </div>
        <ModifyDialog 
            bname={bname}
            blocal={blocal}
            bwebsite={bwebsite}
            bcontact={bcontact}
            bphone={bphone}
            bbank={bbank}
            baccount={baccount}
            bintroduce={bintroduce}
            bstate={bstate} 
            setOpen={setOpen} 
            open={open}
            buttonShow={buttonShow}            
            >
        </ModifyDialog>
        {/* <BootstrapDialog
                                    onClose={handleClose}
                                    aria-labelledby="customized-dialog-title"
                                    open={open}                          
                                >
                                    <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                                        店铺详情
                                    </BootstrapDialogTitle>
                                    <DialogContent dividers>
                                        <Typography gutterBottom>
                                            店铺名称：{row.bname}
                                        </Typography>
                                        <Typography gutterBottom>
                                            地理位置：{row.blocal}
                                        </Typography>
                                        <Typography gutterBottom>
                                            店铺网站：{row.blocal}
                                        </Typography>
                                        <Typography gutterBottom>
                                            店铺联系人：{row.blocal}
                                        </Typography>
                                        <Typography gutterBottom>
                                            店铺联系方式：{row.blocal}
                                        </Typography>
                                        <Typography gutterBottom>
                                            合作银行：{row.blocal}
                                        </Typography>
                                        <Typography gutterBottom>
                                            银行账户：{row.blocal}
                                        </Typography>
                                        <Typography gutterBottom>
                                            店铺介绍：{row.blocal}
                                        </Typography>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button autoFocus onClick={handleClose} >
                                            {row.bstate}
                                        </Button>
                                    </DialogActions>
                            </BootstrapDialog> */}
        </>
    );
}