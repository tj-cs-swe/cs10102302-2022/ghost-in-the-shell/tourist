import * as React from 'react';
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormHelperText from '@mui/material/FormHelperText';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

import { useEffect, useRef } from 'react';
import { Url } from '../../../index'
import axios from 'axios';

function createData(label, Role) {
    let role;
    if (Role == 'USER') {
        role = '用户标签';
    }
    else {
        role = '商家标签';
    }
    return {
        label,
        role
    }
}

const init_labels = [
    createData('爱风景', 'USER'),
    createData('爱美食', 'USER'),
    createData('爱人文', 'USER'),
    createData('北京景点', 'FIRM'),
    createData('低调奢华', 'FIRM'),
    createData('上海景点', 'FIRM'),
];

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

const headCells = [
    {
        id: 'label',
        disablePadding: true,
        label: '标签内容',
    },
    {
        id: 'role',
        disablePadding: false,
        label: '标签类型',
    },
];

function EnhancedTableHead(props) {
    const { onSelectAllClick, numSelected, rowCount } = props;
    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all desserts',
                        }}
                    />
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align='left'
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired
};

const EnhancedTableToolbar = (props) => {
    const { numSelected } = props;
    return (
        <Toolbar
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                ...(numSelected > 0 && {
                    bgcolor: (theme) =>
                        alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
                }),
            }}>
            <Typography
                sx={{ flex: '1 1 100%' }}
                color="inherit"
                variant="subtitle1"
                component="div"
            >
                {numSelected} 项已被选中
            </Typography>
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};
export default function LabelManageTable() {
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const [opt, setOp] = React.useState("");
    const [labels, setLabel] = React.useState([]);
    const [type, setType] = React.useState("");

    const labelRef = useRef();
    const roleRef = useRef();

    const handleOpChange = (event) => {
        setOp(event.target.value);
        StartOperate(event.target.value);
    };
    const handleChangeType = (event) => {
        let value = event.target.value;
        if (value == 40) {
            setType("USER");
        }
        else if (value == 50) {
            setType("FIRM");
        }
    }
    function StartOperate(op) {
        console.log(op);
        if (op == 10) {
            document.getElementById("del").style.display = "none";
            document.getElementById("change").style.display = "none";
            document.getElementById("add").style.display = "block";
        }
        else if (op == 20) {
            document.getElementById("add").style.display = "none";
            document.getElementById("change").style.display = "none";
            document.getElementById("del").style.display = "block";
        }
        else if (op == 30) {
            document.getElementById("add").style.display = "none";
            document.getElementById("del").style.display = "none";
            document.getElementById("change").style.display = "block";
        }
        else {
            document.getElementById("add").style.display = "none";
            document.getElementById("change").style.display = "none";
            document.getElementById("del").style.display = "none";
        }
    }

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = labels.map((n) => n.label);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (label) => {
        const selectedIndex = selected.indexOf(label);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, label);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const isSelected = (label) => selected.indexOf(label) !== -1;
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - labels.length) : 0;


    const renewLabel = (data) => {
        let rows = [];
        for (let i = 0; i < data.length; i++) {
            rows.push(createData(data[i].label, data[i].roles));
        }
        setLabel(rows);
    }
    const addLabel = () => {
        let l = labelRef?.current?.value;
        console.log(l, type);
        if (l == "" || type == "") {
            alert("标签内容或标签类别不能为空，请检查");
            return;
        }
        axios.put(Url + "/admin/addLabel",
            {
                label: l,
                roles: type
            }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/admin" + "/getLabel", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewLabel(response.data.data);
                            alert("添加成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    if(response.data.data === -1&&response.data.code==200){
                        alert("已有同名标签，添加失败");
                        return;
                    }
                    alert("添加失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("添加失败");
            });
    }
    const delLabel = () => {
        let delTarget = [];
        for (let i = 0; i < selected.length; i++) {
            let l = selected[i];
            let r;
            for(let j = 0;j<labels.length;j++){
                if(labels[j].label == l){
                    r = labels[j].role=="用户标签"?"USER":(labels[j].role=="商家标签"?"FIRM":"");
                    break;
                }
            }
            if(l == "" || r == ""){
                alert("出现错误，删除中止");
                return;
            }
            delTarget.push({ label: l, roles: r });
        }
        console.log(delTarget);
        if(delTarget.length==0){
            alert("您已选中0条内容,请选择");
            return;
        }
        axios.put(Url + "/admin/deleteLabels",
            delTarget, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/admin" + "/getLabel", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewLabel(response.data.data);
                            setSelected([]);
                            alert("删除成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("删除失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("删除失败");
            });
    }
    const changeToUser = () => {
        let changeTarget = [];
        for (let i = 0; i < selected.length; i++) {
            let l = selected[i];
            changeTarget.push({ label: l});
        }
        console.log(changeTarget);
        if(changeTarget.length==0){
            alert("您已选中0条内容，请选择");
            return;
        }
        axios.put(Url + "/admin/changeLabel",
        {
            target:"USER",
            origin:changeTarget
        }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/admin" + "/getLabel", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewLabel(response.data.data);
                            setSelected([]);
                            alert("类别更改成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("类别更改失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("类别更改失败");
            });
    }
    const changeToFirm = () => {
        let changeTarget = [];
        for (let i = 0; i < selected.length; i++) {
            let l = selected[i];
            changeTarget.push({ label: l});
        }
        console.log(changeTarget);
        if(changeTarget.length==0){
            alert("您已选中0条内容,请选择");
            return;
        }
        axios.put(Url + "/admin/changeLabel",
        {
            target:"FIRM",
            origin:changeTarget
        }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/admin" + "/getLabel", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewLabel(response.data.data);
                            setSelected([]);
                            alert("类别更改成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("类别更改失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("类别更改失败");
            });
    }
    useEffect(async () => {
        await axios.get(Url + "/admin/getLabel", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewLabel(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    return (
        <section>
            <Box sx={{ width: '60%' }} style={{ float: 'left' }}>
                <Paper sx={{ width: '100%', mb: 2 }}>
                    <EnhancedTableToolbar numSelected={selected.length} />
                    <TableContainer>
                        <Table
                            sx={{ minWidth: 200 }}
                            aria-labelledby="tableTitle"
                            size='medium'
                        >
                            <EnhancedTableHead
                                numSelected={selected.length}
                                onSelectAllClick={handleSelectAllClick}
                                rowCount={labels.length}
                            />
                            <TableBody>
                                {stableSort(labels, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((row, index) => {
                                        const isItemSelected = isSelected(row.label);
                                        const labelId = `enhanced-table-checkbox-${index}`;

                                        return (
                                            <TableRow
                                                hover
                                                onClick={(event) => handleClick(row.label)}
                                                role="checkbox"
                                                aria-checked={isItemSelected}
                                                tabIndex={-1}
                                                key={row.label}
                                                selected={isItemSelected}
                                            >
                                                <TableCell padding="checkbox">
                                                    <Checkbox
                                                        color="primary"
                                                        checked={isItemSelected}
                                                        inputProps={{
                                                            'aria-labelledby': labelId,
                                                        }}
                                                    />
                                                </TableCell>
                                                <TableCell
                                                    component="th"
                                                    id={labelId}
                                                    scope="row"
                                                    padding="none"
                                                >
                                                    {row.label}
                                                </TableCell>
                                                <TableCell align="left">{row.role}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                {emptyRows > 0 && (
                                    <TableRow
                                        style={{
                                            height: 53 * emptyRows,
                                        }}
                                    >
                                        <TableCell colSpan={6} />
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={labels.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        labelRowsPerPage="每页展示的标签数量"
                    />
                </Paper>
            </Box>
            <div style={{ float: 'right', width: '30%', marginRight: '5%' }} >
                <FormControl sx={{ m: 1, minWidth: 120 }}>
                    <Select
                        value={opt}
                        onChange={handleOpChange}
                        id='op'
                        displayEmpty
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                        <MenuItem value="">
                            <em>未选择操作</em>
                        </MenuItem>
                        <MenuItem value={10}>增加标签</MenuItem>
                        <MenuItem value={20}>删除标签</MenuItem>
                        <MenuItem value={30}>更改标签类别</MenuItem>
                    </Select>
                    <FormHelperText>选择操作</FormHelperText>
                </FormControl>

                <div id="add" style={{ display: "none" }}>
                    <TextField
                        fullWidth
                        inputRef={labelRef}
                        label="标签内容"
                        style={{ marginTop: "20px" }}
                    />
                    <FormControl fullWidth style={{ marginTop: "20px" }}>
                        <InputLabel id="type-label">选择标签类别</InputLabel>
                        <Select
                            fullWidth
                            labelId="type-label"
                            id="type"
                            label="选择标签类别"
                            align="left"
                            onChange={handleChangeType}
                        >
                            <MenuItem value={40}>用户标签</MenuItem>
                            <MenuItem value={50}>商家标签</MenuItem>
                        </Select>
                    </FormControl>

                    <Button variant="contained" style={{ marginTop: "20px" }} onClick={addLabel}>确认添加标签</Button>
                </div>
                <div id="del" style={{ display: "none" }}>
                    <Button variant="contained" style={{ marginTop: "20px" }} onClick={delLabel}>确认删除所有选中标签</Button>
                </div>
                <div id="change" style={{ display: "none" }}>
                    <Button variant="contained" style={{ marginTop: "20px" }} onClick={changeToUser}>将所有选中标签更改为用户类</Button>
                    <Button variant="contained" style={{ marginTop: "20px" }} onClick={changeToFirm}>将所有选中标签更改为商家类</Button>
                </div>
            </div>
        </section>
    );
}
