import H3 from '@material-tailwind/react/Heading3';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import React, { useEffect, useRef } from 'react';
import {Url} from '../../../index';
import axios from 'axios';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { TableHead } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';

function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

function createData(uuid,uustatus,uuname){
    return {uuid,uustatus,uuname};
}

const uarr=[
    createData('3','0','xiaohong'),
    createData('20','1','xiaohua'),
];

export default function Content() {
    
    const uid=useRef();
    const [tag,setTag]=React.useState(false);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    
    const [arr,setUarr]=React.useState([]);
    const [status,setStatus]=React.useState('0');
    const [userName,setUname]=React.useState("");
    const [idarr,setIdarr]=React.useState("");
    const [namearr,setNamearr]=React.useState("");
    const [statusarr,setStatusarr]=React.useState("");
    const [errorout,setErrorout]=React.useState("");
    const [buttonout,setButtonout]=React.useState("");

    const [buttonShow,setBshow]=React.useState('none');
    const [errorShow,setEshow]=React.useState('none');
    const [userShow,setUshow]=React.useState('none');
    
    const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - arr.length) : 0;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const confirmButton=()=>{
        let info=uid.current?.value;       
        setTag(true);
        let i=0;
        //console.log(idarr+"hhhhhhhh");
        for(i=0;i<arr.length;i++){
            if(arr[i].uuid==info){
                setErrorout("");
                setIdarr("用户ID: "+arr[i].uuid);
                setNamearr("用户昵称: "+arr[i].uuname);
                setUname(arr[i].uuname);
                setStatus(arr[i].uustatus);
                if(arr[i].uustatus=='0'){
                    setStatusarr("用户当前状态：正常");
                    setButtonout("冻结该用户");
                }
                else{
                    setStatusarr("用户当前状态：已冻结");
                    setButtonout("解封该用户");
                }
                setBshow(''); 
                setEshow('none');
                setUshow('');
                //console.log(arr[i].uuid);
                //console.log(arr[i].uuname);
                //console.log(arr[i].uustatus);
                break;
            }
        }
        if(i>=arr.length){
            setIdarr("");
            setNamearr("");
            setStatusarr("");
            setErrorout("没有该用户");
            setButtonout("");
            setBshow('none');
            setEshow('');
            setUshow('none');
            setUname("");
        }
        
    }

    const changeStatus = () => {
        //修改当前用户的状态
        let tempStatus;
        let tempid=uid.current?.value;
        if(status=='0'){
            tempStatus="1";
            //alert("正在冻结当前用户账号···");
        }
        else{
            tempStatus="0";
            //alert("正在解封当前用户账号···");
        }
        //console.log(tempStatus);
        //console.log(tempid);
        //console.log(userName); 
        axios.put(Url+"/admin/putUserid",null,
            {    
                timeout:20000,
                headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                },
                params: {
                    "id":tempid,
                    // name:userName,
                    "status":tempStatus
                }
        })
            .then(function(response){
                console.log(response.data)
                console.log("修改账号状态成功")
                setStatus(tempStatus);
                //-
                if(response.data.data==0){
                    axios.get(Url+"/admin"+"/getUsers",{
                        timeout:10000,
                        headers:{
                            Authorization: window.sessionStorage.getItem('user_token')           
                        }
                    })
                        .then(function (response) {
                            //console.log(response.data.data);
                            //console.log(response.data);
                            
                            //函数
                            renewData(response.data.data);
                            
                            if(tempStatus=='0'){
                                alert("解封用户账号成功！");
                                setStatusarr("用户当前状态：正常");
                                setButtonout("冻结该用户"); 
                            }
                            else{
                                alert("冻结用户账号成功！");
                                setStatusarr("用户当前状态：已冻结");
                                setButtonout("解封该用户");
                                
                            }                        
                        })
                        .catch(function(error){
                            console.log("错误");
                            console.log(error);
                        });
                }
                else{
                    if(status=='0'){                    
                        alert("冻结用户账号失败！");
                    }
                    else{                    
                        alert("解封用户账号失败！");
                    }
                }
            })
            .catch(function(error){ 
                console.log(error);
                if(status=='0'){                    
                    alert("冻结用户账号失败！");
                }
                else{                    
                    alert("解封用户账号失败！");
                }
            });
        //console.log(uid.current?.value);
        //console.log(status);
    }

    const deleteUser=()=>{
        let tempid=uid.current?.value;
        console.log(status)
        console.log(tempid)       
        if(status=='0'){
            alert("删除失败！该用户为正常状态，若要将其删除，请先冻结当前用户");
        }
        else{
            //alert("正在删除当前用户账号···");???
            axios.delete(Url+"/admin/deleteUser",
            {    
                //timeout:20000,
                headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                },
                params: {
                    "id":tempid                  
                }
            })
                .then(function(response){
                    console.log(response.data)
                    //-
                    if(response.data.data==0){
                        axios.get(Url+"/admin"+"/getUsers",{
                            timeout:10000,
                            headers:{
                            Authorization: window.sessionStorage.getItem('user_token')           
                            }
                        })
                            .then(function (response) {                            
                                alert("删除用户账号成功")
                                //函数
                                renewData(response.data.data);  
                                setIdarr("");
                                setNamearr("");
                                setStatusarr("");
                                setErrorout("没有该用户");
                                setButtonout("");
                                setBshow('none');
                                setEshow('none');
                                setUshow('none');
                                setUname("");                 
                            })

                            .catch(function(error){
                                console.log("错误");
                                console.log(error);
                            });
                    }
                    else{
                        alert("删除用户账号失败！");
                    }
                })

                .catch(function(error){
                    console.log(error);
                    alert("删除用户账号失败！");
                });
        }
        
    }

    const renewData=(allUser)=>{
        let arr=[];
        for(let i=0;i<allUser.length;i++){
            arr.push(createData(allUser[i].id,allUser[i].status,allUser[i].name));
        }
        setUarr(arr);
    }

    useEffect(async()=>{
        await axios.get(Url+"/admin"+"/getUsers",{
            timeout:10000,
            headers:{
                Authorization: window.sessionStorage.getItem('user_token')
                
            }
        })
            .then(function (response) {
                console.log(response.data.data);
                console.log(response.data);
                //函数
                renewData(response.data.data);
            })
            .catch(function(error){
                console.log("错误");
                console.log(error);
            });
    },[]);

    return (
        <section className="relative py-16 bg-gray-100">
            <div className="container max-w-7xl px-4 mx-auto">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                       
                        <div className="text-center my-8">
                            <H3 color="gray">用户管理</H3>
                        </div>

                        <div className="mb-10 py-2 border-t border-gray-200 text-center">
                            <div style={{ float:'left' , width:'50%',marginBottom: "75px",marginTop: "25px",margin:0}}>
                                {/* <div style={{ marginBottom: "25px", marginTop: "25px",width:"60%"}}>
                                    
                                </div>
                                <div style={{ width: "30%", marginBottom: "25px", marginTop: "50px" }}>
                                    
                                </div>
                                <div style={{ width: "30%", marginBottom: "25px", marginTop: "120px" }}>
                                    
                                </div> */}
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="center">
                                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                                    <b>用户ID</b>
                                                    </span>
                                                </TableCell>
                                                <TableCell align="center">
                                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                                    <b>用户昵称</b>
                                                    </span>
                                                </TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell align="right">                                                                               
                                                </TableCell>
                                                <TableCell align="right">
                                                    注册用户总数：{arr.length}                                            
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                                                           
                                        <TableBody>                                           
                                            {(rowsPerPage > 0
                                                ? arr.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                : arr
                                            ).map((row) => (
                                                <TableRow>
                                                    <TableCell align="center">
                                                        {row.uuid}
                                                    </TableCell>
                                                    <TableCell align="center">
                                                        {row.uuname}
                                                    </TableCell>                                                     
                                                </TableRow>
                                            ))}

                                            {emptyRows > 0 && (
                                                <TableRow style={{ height: 53 * emptyRows }}>
                                                    <TableCell colSpan={6} />
                                                </TableRow>
                                            )}
                                        </TableBody>

                                        <TableFooter>
                                            <TableRow>
                                                <TablePagination
                                                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                                    colSpan={5}
                                                    count={arr.length}
                                                    rowsPerPage={rowsPerPage}
                                                    page={page}
                                                    SelectProps={{
                                                        inputProps: {
                                                            'aria-label': 'rows per page',
                                                        },
                                                        native: true,
                                                    }}
                                                    onPageChange={handleChangePage}
                                                    onRowsPerPageChange={handleChangeRowsPerPage}
                                                    ActionsComponent={TablePaginationActions}
                                                    labelRowsPerPage="每页展示的用户数量"
                                                />
                                            </TableRow>
                                        </TableFooter>
                                        
                                    </Table>
                                </TableContainer>
                            </div>
                            <div style={{ float:'right' , width:'40%',marginBottom: "75px",margin:0}}>
                                <div style={{ marginBottom: "25px", marginTop: "25px",width:"60%"}}>
                                    <TextField 
                                        required
                                        fullWidth
                                        inputRef={uid}
                                        label="用户ID"
                                        variant="filled"
                                    />
                                </div>
                                <div>
                                    <Button variant="contained" onClick={confirmButton}>搜索</Button>
                                </div>
                                <TableCell align="center" style={{display:errorShow}}>
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                    {errorout}
                                    </span>
                                </TableCell><p></p>
                                <TableCell align="center" style={{display:userShow}}>
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                    {idarr}
                                    </span>
                                </TableCell><p></p>
                                <TableCell align="center" style={{display:userShow}}>
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                    {namearr}
                                    </span>
                                </TableCell><p></p>
                                <TableCell align="center" style={{display:userShow}}>
                                    <span className="text-gray-900 text-base font-serif font-medium mb-2 ">
                                    {statusarr}
                                    </span>
                                </TableCell><p></p>
                                                             
                                <div style={{ display:buttonShow}}>
                                    <Button variant="contained" onClick={changeStatus}>{buttonout}</Button>                                    
                                </div>
                                <p></p>
                                <div style={{ display:buttonShow,marginTop: "20px"}}>                                   
                                    <Button variant="contained" onClick={deleteUser}>删除该用户</Button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </section>
    );
}