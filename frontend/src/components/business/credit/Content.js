import H3 from '@material-tailwind/react/Heading3'
import React from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Input from '@mui/material/Input';
import PropTypes from 'prop-types';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import Skeleton from '@mui/material/Skeleton';
import 'rsuite/dist/rsuite.min.css';
import { Steps } from 'rsuite';

import { useEffect, useRef } from 'react';
import { Url } from '../../../index'
import axios from 'axios';

const ariaLabel = { 'aria-label': 'description' };
const Inp = styled('input')({
    display: 'none',
});
const styles = {
    width: '200px',
    display: 'inline-table',
    verticalAlign: 'top'
};
const initMsg = {
    name: '汪汪队',
    location: '汪汪省汪汪市汪汪学校',
    website: 'https://wangwangdui.com',
    contact_name: '汪汪',
    contact_num: '13866668888',
    bank_name: '中国工商银行',
    bank_account: '6354345654',
    intro: '汪汪队赚大钱！'
};
const data = {
    path: ""
}
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

function createData(name, location, website, contact_name, contact_num, bank_name, bank_account, intro) {
    return { name, location, website, contact_name, contact_num, bank_name, bank_account, intro };
}

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

export default function Content() {
    const [page, setPage] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [msg, setMsg] = React.useState([]);

    // const nameRef = useRef();
    const locRef = useRef();
    const webRef = useRef();
    const personRef = useRef();
    const callRef = useRef();
    const bankRef = useRef();
    const accountRef = useRef();
    const introRef = useRef();
    const reasonRef = useRef();

    const handleClose = () => {
        setOpen(false);
    };
    const handleToggle = () => {
        setOpen(!open);
    };

    const renewInfo = (data) => {
        let row;
        console.log(data)
        row = createData(data.name, data.location, data.website, data.contactName, data.contactTel, data.bankName, data.bankAccount, data.intro);
        console.log(row);
        setMsg(row);
    }

    const renewStatus = (data) => {
        let p = -1;
        console.log(data);
        if (data == "unsubmit") {
            p = 0;
            setPage(p);
        }
        else if (data == "checking") {
            p = 1;
            setPage(p);
        }
        else if (data == "reject_checking") {
            p = 11;
            setPage(p);
        }
        else if (data == "pass") {
            p = 2;
            setPage(p);
        }
        else if (data == "success") {
            p = 3;
            setPage(p);
        }
        else if (data == "rechecking") {
            p = 6;
            setPage(p);
        }
        else if (data == "reject_rechecking") {
            p = 16;
            setPage(p);
        }
        else if (data == "exit_checking") {
            p = 4;
            setPage(p);
        }
        else if (data == "reject_exit") {
            p = 14;
            setPage(p);
        }
        else {
            p = 5;
            setPage(p);
        }
        console.log(p);
    }

    const changeSettleInfo = () => {
        //console.log(window.sessionStorage.getItem('nickname'))
        let info = createData(window.sessionStorage.getItem('nickname'), locRef.current?.value, webRef.current?.value, personRef.current?.value, callRef.current?.value, bankRef.current?.value, accountRef.current?.value, introRef.current?.value);
        console.log(info);
        if (info.name == "" || info.location == "" || info.website == "" || info.contact_name == "" || info.contact_num == "" || info.bank_name == "" || info.bank_account == "" || info.intro == "") {
            alert("您有信息未填写,请完善");
            return;
        }
        axios.put(Url + "/credit/changeInfo",
            {
                name: info.name,
                location: info.location,
                website: info.website,
                contactName: info.contact_name,
                contactTel: info.contact_num,
                bankName: info.bank_name,
                bankAccount: info.bank_account,
                intro: info.intro
            }, {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/credit" + "/getInfo", {
                        timeout: 10000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewInfo(response.data.data);
                            setPage(1);
                            alert("提交成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("提交失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("提交失败");
            });
    };

    const changeAfterSuccess = () => {
        //console.log(window.sessionStorage.getItem('nickname'))
        let info = createData(window.sessionStorage.getItem('nickname'), locRef.current?.value, webRef.current?.value, personRef.current?.value, callRef.current?.value, bankRef.current?.value, accountRef.current?.value, introRef.current?.value);
        console.log(info);
        if (info.name == "" || info.location == "" || info.website == "" || info.contact_name == "" || info.contact_num == "" || info.bank_name == "" || info.bank_account == "" || info.intro == "") {
            alert("您有信息未填写,请完善");
            return;
        }
        axios.put(Url + "/credit/rechecking",
            {
                name: info.name,
                location: info.location,
                website: info.website,
                contactName: info.contact_name,
                contactTel: info.contact_num,
                bankName: info.bank_name,
                bankAccount: info.bank_account,
                intro: info.intro
            }, {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/credit" + "/getInfo", {
                        timeout: 10000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewInfo(response.data.data);
                            setPage(6);
                            alert("提交成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("提交失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("提交失败");
            });
    };
    const signContract = () => {
        axios.put(Url + "/credit/signContract",
            {}, {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/credit" + "/getInfo", {
                        timeout: 10000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewInfo(response.data.data);
                            setPage(3);
                            setOpen(!open);
                            alert("合同签署成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("合同签署失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("合同签署失败");
            });
    }

    const submitExitApply = () => {
        let rea = reasonRef.current?.value;
        //console.log(rea);
        axios.put(Url + "/credit/applyExit",
            {
                reason: rea
            }, {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/credit" + "/getInfo", {
                        timeout: 10000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data.data)
                            renewInfo(response.data.data);
                            setPage(4);
                            setOpen(false);
                            alert("退驻申请提交成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("退驻申请提交失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("退驻申请提交失败");
            });
    }

    const applySettleAgain = () => {
        axios.put(Url + "/credit/applySettleAgain",
            {}, {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    setPage(0);
                    alert("申请重新入驻权限成功");
                    return;
                } else {
                    alert("申请重新入驻权限失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("申请重新入驻权限失败");
            });
    }

    const refresh = () => {
        axios.get(Url + "/credit" + "/getInfo", {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewInfo(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });

        axios.get(Url + "/credit" + "/getStatus", {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                //console.log(response.data)
                renewStatus(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    useEffect(async () => {
        //console.log(window.sessionStorage.getItem('user_token'))

        await axios.get(Url + "/credit" + "/getInfo", {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewInfo(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });

        await axios.get(Url + "/credit" + "/getStatus", {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                //console.log(response.data)
                renewStatus(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    if (page == 0) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={0} vertical style={styles}>
                                        <Steps.Item title="待完成" description="未提交审核信息" />
                                        <Steps.Item title="待完成" description="未通过审核" />
                                        <Steps.Item title="待完成" description="未签约合同" />
                                        <Steps.Item title="待完成" description="未成功入驻" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={nameRef}
                                                label="店铺名称(请使用注册名)"
                                                variant="filled"
                                            />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={locRef}
                                                label="地理位置"
                                                variant="filled"
                                            />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={webRef}
                                                label="店铺网址"
                                                variant="filled"
                                            />                                    </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={personRef}
                                                label="店铺联系人"
                                                variant="filled"
                                            />                                    </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={callRef}
                                                label="店铺联系方式"
                                                variant="filled"
                                            />                                    </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={bankRef}
                                                label="合作银行"
                                                variant="filled"
                                            />                                    </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={accountRef}
                                                label="银行账户"
                                                variant="filled"
                                            />                                    </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                label="店铺介绍"
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <label htmlFor="certification">
                                            <Inp accept="image/*" id="certification" multiple type="file" />
                                            <Button variant="contained" component="span" style={{ marginLeft: "130px", marginTop: "10px" }}>
                                                上传营业执照
                                            </Button>
                                        </label>
                                        <Skeleton variant="rectangular" width={210} height={118} src={data.path} style={{ marginLeft: "90px", marginTop: "10px" }}></Skeleton>
                                        <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ width: "30%", marginBottom: "25px", marginTop: "500px", marginLeft: "69%" }}>
                                        <Button variant="contained" color="success" onClick={changeSettleInfo} >提交信息进入审核</Button>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 1) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={1} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="待完成" description="正在审核中" />
                                        <Steps.Item title="待完成" description="未签约合同" />
                                        <Steps.Item title="待完成" description="未成功入驻" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <label htmlFor="contained-button-file">
                                            <Inp accept="image/*" id="contained-button-file" multiple type="file" />
                                            <Button variant="contained" component="span" style={{ marginLeft: "130px", marginTop: "10px" }}>
                                                上传营业执照
                                            </Button>
                                        </label>
                                        <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ width: "30%", marginBottom: "25px", marginTop: "500px", marginLeft: "69%" }}>
                                        <Button variant="contained" color="success" onClick={changeSettleInfo}>重新提交审核信息</Button>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 11) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={1} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核已驳回" />
                                        <Steps.Item title="待完成" description="未签约合同" />
                                        <Steps.Item title="待完成" description="未成功入驻" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <label htmlFor="contained-button-file">
                                            <Inp accept="image/*" id="contained-button-file" multiple type="file" />
                                            <Button variant="contained" component="span" style={{ marginLeft: "130px", marginTop: "10px" }}>
                                                上传营业执照
                                            </Button>
                                        </label>
                                        <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ width: "30%", marginBottom: "25px", marginTop: "500px", marginLeft: "69%" }}>
                                        <Button variant="contained" color="success" onClick={changeSettleInfo}>重新提交审核信息</Button>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 2) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={2} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="待完成" description="未签约合同" />
                                        <Steps.Item title="待完成" description="未成功入驻" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <label htmlFor="contained-button-file">
                                            <Inp accept="image/*" id="contained-button-file" multiple type="file" />
                                            <Button variant="contained" component="span" style={{ marginLeft: "130px", marginTop: "10px" }}>
                                                上传营业执照
                                            </Button>
                                        </label>
                                        <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "18%", marginBottom: "25px", marginLeft: "6%", marginTop: "200px" }}>
                                        <Button variant="contained" color="success" onClick={changeSettleInfo} >重新提交审核信息</Button>
                                    </div>
                                    <div style={{ float: "left", width: "12%", marginBottom: "25px", marginLeft: "5%", marginTop: "200px" }}>
                                        <Button onClick={handleToggle} variant="contained" color="success">合同签约</Button>
                                        <BootstrapDialog
                                            onClose={handleClose}
                                            aria-labelledby="customized-dialog-title"
                                            open={open}
                                        >
                                            <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                                                商家签约合同
                                            </BootstrapDialogTitle>
                                            <DialogContent dividers>
                                                <Typography gutterBottom>
                                                    这是合同第一段，这是合同第一段，这是合同第一段，这是合同第一段，这是合同第一段，
                                                    这是合同第一段，这是合同第一段，这是合同第一段，这是合同第一段，这是合同第一段，
                                                    这是合同第一段，这是合同第一段，这是合同第一段，这是合同第一段，这是合同第一段
                                                </Typography>
                                                <Typography gutterBottom>
                                                    这是合同第二段，这是合同第二段，这是合同第二段，这是合同第二段，这是合同第二段，
                                                    这是合同第二段，这是合同第二段，这是合同第二段，这是合同第二段，这是合同第二段，
                                                    这是合同第二段，这是合同第二段，这是合同第二段，这是合同第二段，这是合同第二段
                                                </Typography>
                                                <Typography gutterBottom>
                                                    这是合同第三段，这是合同第三段，这是合同第三段，这是合同第三段，这是合同第三段，
                                                    这是合同第三段，这是合同第三段，这是合同第三段，这是合同第三段，这是合同第三段，
                                                    这是合同第三段，这是合同第三段，这是合同第三段，这是合同第三段，这是合同第三段，
                                                </Typography>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button autoFocus onClick={signContract} >
                                                    确认签约
                                                </Button>
                                            </DialogActions>
                                        </BootstrapDialog>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 3) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={4} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="已完成" description="已签约合同" />
                                        <Steps.Item title="已完成" description="已成功入驻" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <label htmlFor="contained-button-file">
                                            <Inp accept="image/*" id="contained-button-file" multiple type="file" />
                                            <Button variant="contained" component="span" style={{ marginLeft: "130px", marginTop: "10px" }}>
                                                上传营业执照
                                            </Button>
                                        </label>
                                        <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "18%", marginBottom: "25px", marginLeft: "6%", marginTop: "200px" }}>
                                        <Button variant="contained" color="success" onClick={changeAfterSuccess} >修改店铺信息</Button>
                                    </div>
                                    <div style={{ float: "left", width: "12%", marginBottom: "25px", marginLeft: "5%", marginTop: "200px" }}>
                                        <Button onClick={handleToggle} variant="contained" color="success">我要退驻</Button>
                                        <Dialog open={open} onClose={handleClose}>
                                            <DialogTitle>退驻申请</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    请说明您的退驻缘由，提交后由管理员审核，审核完毕后将按照合同进行结算
                                                </DialogContentText>
                                                <TextField
                                                    autoFocus
                                                    margin="dense"
                                                    inputRef={reasonRef}
                                                    label="退驻说明"
                                                    type="email"
                                                    fullWidth
                                                    variant="standard"
                                                />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleClose}>取消</Button>
                                                <Button onClick={submitExitApply}>提交退驻申请</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 4) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={4} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="已完成" description="已签约合同" />
                                        <Steps.Item title="已完成" description="已成功入驻" />
                                        <Steps.Item title="待完成" description="退驻审核中" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth disabled defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth disabled defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth disabled defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth disabled defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth disabled defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth disabled defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth disabled defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                disabled
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "30%", marginBottom: "25px", marginLeft: "15%", marginTop: "200px" }}>
                                        <Button onClick={handleToggle} variant="contained" color="success">重新提交退驻申请</Button>
                                        <Dialog open={open} onClose={handleClose}>
                                            <DialogTitle>退驻申请</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    请说明您的退驻缘由，提交后由管理员审核，审核完毕后将按照合同进行结算
                                                </DialogContentText>
                                                <TextField
                                                    autoFocus
                                                    margin="dense"
                                                    inputRef={reasonRef}
                                                    label="退驻说明"
                                                    type="email"
                                                    fullWidth
                                                    variant="standard"
                                                />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleClose}>取消</Button>
                                                <Button onClick={submitExitApply}>提交退驻申请</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 14) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={4} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="已完成" description="已签约合同" />
                                        <Steps.Item title="已完成" description="已成功入驻" />
                                        <Steps.Item title="已完成" description="退驻审核已驳回" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth disabled defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth disabled defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth disabled defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth disabled defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth disabled defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth disabled defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth disabled defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                disabled
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "30%", marginBottom: "25px", marginLeft: "15%", marginTop: "200px" }}>
                                        <Button onClick={handleToggle} variant="contained" color="success">重新提交退驻申请</Button>
                                        <Dialog open={open} onClose={handleClose}>
                                            <DialogTitle>退驻申请</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    请说明您的退驻缘由，提交后由管理员审核，审核完毕后将按照合同进行结算
                                                </DialogContentText>
                                                <TextField
                                                    autoFocus
                                                    margin="dense"
                                                    inputRef={reasonRef}
                                                    label="退驻说明"
                                                    type="email"
                                                    fullWidth
                                                    variant="standard"
                                                />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleClose}>取消</Button>
                                                <Button onClick={submitExitApply}>提交退驻申请</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 6) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={4} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="已完成" description="已签约合同" />
                                        <Steps.Item title="已完成" description="已成功入驻" />
                                        <Steps.Item title="待完成" description="信息修改审核中" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth disabled defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "18%", marginBottom: "25px", marginLeft: "12%", marginTop: "200px" }}>
                                        <Button variant="contained" color="success" onClick={changeAfterSuccess} >重新提交店铺信息</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 16) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={4} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="已完成" description="已签约合同" />
                                        <Steps.Item title="已完成" description="已成功入驻" />
                                        <Steps.Item title="已完成" description="信息修改已驳回" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth disabled defaultValue={msg.name} inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth defaultValue={msg.location} inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth defaultValue={msg.website} inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth defaultValue={msg.contact_name} inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth defaultValue={msg.contact_num} inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth defaultValue={msg.bank_name} inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth defaultValue={msg.bank_account} inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                inputRef={introRef}
                                                defaultValue={msg.intro}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "18%", marginBottom: "25px", marginLeft: "12%", marginTop: "200px" }}>
                                        <Button variant="contained" color="success" onClick={changeAfterSuccess} >重新提交店铺信息</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
    else if (page == 5) {
        return (
            <section className="relative py-16 bg-gray-100">
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <div className="text-center my-8">
                                <H3 color="gray">资质查看</H3>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200" style={{ float: "left", width: "30%" }}>
                                <div style={{ width: "50%", marginTop: "10px", marginLeft: "10%" }}>
                                    <Button variant="contained" onClick={refresh} >刷新审核进度</Button>
                                </div>
                                <div style={{ marginTop: "50px", marginLeft: "200px" }}>
                                    <Steps current={5} vertical style={styles}>
                                        <Steps.Item title="已完成" description="已提交审核信息" />
                                        <Steps.Item title="已完成" description="审核通过" />
                                        <Steps.Item title="已完成" description="已签约合同" />
                                        <Steps.Item title="已完成" description="已成功入驻" />
                                        <Steps.Item title="已完成" description="已成功退驻" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="mb-10 py-2 border-t border-gray-200">
                                <div style={{ float: "left", width: "70%" }}>
                                    <div id="msgInput" style={{ float: "left", width: "55%", paddingLeft: "20%" }}>
                                        {/* <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺名称</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={nameRef} />
                                        </div> */}
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>地理位置</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={locRef} />
                                        </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺网址</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={webRef} />                              </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系人</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={personRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>店铺联系方式</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={callRef} />                               </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>合作银行</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={bankRef} />                                 </div>
                                        <div style={{ marginBottom: "25px", marginTop: "25px" }}>
                                            <p>银行账户</p>
                                            <Input fullWidth disabled inputProps={ariaLabel} inputRef={accountRef} />                                </div>
                                    </div>
                                    <div id="cerImgInput" style={{ float: "left", width: "40%" }}>
                                        <div style={{ marginBottom: "25px", marginTop: "25px", marginLeft: '50px' }}>
                                            <p>店铺介绍</p>
                                            <TextField
                                                required
                                                fullWidth
                                                disabled
                                                inputRef={introRef}
                                                multiline
                                                rows={4}
                                            />
                                        </div>
                                        {/* <p style={{ marginLeft: "150px", marginTop: "20px" }}>营业执照预览</p> */}
                                    </div>
                                    <div style={{ float: "left", width: "30%", marginBottom: "25px", marginLeft: "15%", marginTop: "200px" }}>
                                        <Button onClick={applySettleAgain} variant="contained" color="success">重新进入入驻流程</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        );
    }
}
