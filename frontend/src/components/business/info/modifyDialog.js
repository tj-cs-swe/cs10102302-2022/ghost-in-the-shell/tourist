import React, { useRef, useEffect } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { TextField } from '@mui/material';

import axios from 'axios';


export default function ModifyDialog(props) {


    const nameRef = useRef();
    const priceRef = useRef();
    const remainRef = useRef();
    const addressRef = useRef();
    const introRef = useRef();


    const handleClose = () => {
        props.setOpen(false);
    };

    const handleSubmit = () => {
        if(nameRef.current?.value==''){
            alert("名称不能为空");
            return;
        }
        if(priceRef.current?.value==''){
            alert("价格不能为空");
            return;
        }
        if(priceRef.current?.value<0){
            alert("价格不能小于0");
            return;
        }
        if(remainRef.current?.value==''){
            alert("余量不能为空");
            return;
        }
        if(remainRef.current?.value<0){
            alert("余量不能小于0");
            return;
        }
        if(addressRef.current?.value==''){
            alert("地址不能为空");
            return;
        }
       
        let aPdct = [];
        aPdct.name = nameRef.current?.value;
        aPdct.price = priceRef.current?.value;
        aPdct.remain = remainRef.current?.value;
        aPdct.address = addressRef.current?.value;
        aPdct.intro = introRef.current?.value==''?null:introRef.current?.value;
        props.modifyProduct(props.Row.name,aPdct);
        props.setOpen(false);
    }


    return (
        <div>
            <Dialog open={props.open} onClose={handleClose}>
                <DialogTitle>修改商品信息</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="名称"
                        fullWidth
                        variant="standard"
                        inputRef={nameRef}
                        defaultValue={props.Row.name}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="价格"
                        fullWidth
                        type='number'
                        variant="standard"
                        inputRef={priceRef}
                        defaultValue={props.Row.price}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="余量"
                        fullWidth
                        type='number'
                        variant="standard"
                        inputRef={remainRef}
                        defaultValue={props.Row.remain}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="地址"
                        fullWidth
                        variant="standard"
                        inputRef={addressRef}
                        defaultValue={props.Row.address}
                    />
                     <TextField
                        autoFocus
                        margin="dense"
                        label="介绍"
                        fullWidth
                        variant="standard"
                        inputRef={introRef}
                        defaultValue={props.Row.intro}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>取消</Button>
                    <Button onClick={handleSubmit}>提交</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}