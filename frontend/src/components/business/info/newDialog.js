import React, { useRef } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { TextField } from '@mui/material';


export default function NewDialog(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    if (name.current?.value == '') {
      alert("名称不能为空");
      return;
    }
    if (price.current?.value == '') {
      alert("价格不能为空");
      return;
    }
    if (price.current?.value < 0) {
      alert("价格不能小于0");
      return;
    }
    if (remain.current?.value == '') {
      alert("余量不能为空");
      return;
    }
    if (remain.current?.value < 0) {
      alert("余量不能小于0");
      return;
    }
    if (address.current?.value == '') {
      alert("地址不能为空");
      return;
    }

    let aPdct = [];
    aPdct.name = name.current?.value;
    aPdct.price = price.current?.value;
    aPdct.remain = remain.current?.value;
    aPdct.address = address.current?.value;
    aPdct.intro = intro.current?.value;
    props.addProduct(aPdct);
    setOpen(false);
  }

  const name = useRef();
  const price = useRef();
  const remain = useRef();
  const address = useRef();
  const intro = useRef();


  return (
    <div>
      {(props.canAdd)&&(<div style={{ marginTop: 20 }}>
        <Button color='secondary' onClick={handleClickOpen}>添加新商品</Button>
      </div>)}
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>添加新商品</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="名称"
            fullWidth
            variant="standard"
            inputRef={name}
          />
          <TextField
            autoFocus
            margin="dense"
            label="价格"
            fullWidth
            type='number'
            variant="standard"
            inputRef={price}
          />
          <TextField
            autoFocus
            margin="dense"
            label="余量"
            fullWidth
            type='number'
            variant="standard"
            inputRef={remain}
          />
          <TextField
            autoFocus
            margin="dense"
            label="地址"
            fullWidth
            variant="standard"
            inputRef={address}
          />
          <TextField
            autoFocus
            margin="dense"
            label="介绍"
            fullWidth
            multiline
            variant="standard"
            inputRef={intro}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>取消</Button>
          <Button onClick={handleSubmit}>提交</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}