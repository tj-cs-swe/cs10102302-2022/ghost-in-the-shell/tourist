import * as React from 'react';
import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { TableHead } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import { Button } from '@mui/material';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import Typography from 'views/index-sections/Typography';

import NewDialog from './newDialog';
import ModifyDialog from './modifyDialog';

import { Url } from '../../../index'
import axios from 'axios';

function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

function createData(name, price, remain, address, intro) {
    return { name, price, remain, address, intro };
}

const columns = [
    { id: 'name', label: '名称', minWidth: 170, align: 'center' },
    { id: 'price', label: '价格', minWidth: 100, align: 'center' },
    { id: 'remain', label: '余量', minWidth: 100, align: 'center' },
    { id: 'position', label: '地址', minWidth: 100, align: 'center' },
    { id: 'intro', label: '介绍', minWidth: 100, align: 'center' },
    { id: 'operation', label: '操作', minWidth: 170, align: 'center' },
];

// const trows = [
//     createData('beijing', 200, 200, 'dsaksdh', 'gfhgfgdgfd'),
// ]

export default function ProductPaginationActionsTable() {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [modifyOpen, setModifyOpen] = React.useState(false);
    const [modifyRow, setModifyRow] = React.useState({ name: '', address: '', price: 0, remains: 0, intro: '' });
    const [rows, setRows] = React.useState([]);
    const [change,setChange] = React.useState(false);
    const [canAdd,setCanAdd] = React.useState(false);

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleDelete = (name) => {

        axios.delete(Url + "/product" + "/delete", {
            params: {
                name: name,
            },
            headers: { Authorization: window.sessionStorage.getItem('user_token') }
        }
        )
            .then(function (response) {
                // console.log(response.data)
                if (response.data.data == 1) {
                    alert('删除成功');
                    deleteArow(name);
                    return;
                } else {
                    alert('删除失败');
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("删除失败");
            });

    };

    const handleModify = (row) => {
        setModifyOpen(true);
        setModifyRow(row);
    };

    const addProduct = (aPdct) => {

        console.log(aPdct.intro)
        axios.post(Url + "/product" + "/add",
                {
                    "name":aPdct.name,
                    "price":Number(aPdct.price),
                    "remain":Number(aPdct.remain),
                    "address":aPdct.address,
                    "intro": aPdct.intro
                }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token'),
                'Content-Type':'application/json'
            }
        })
            .then(function (response) {
                console.log(response)
                if (response.data.data == "Add success") {
                    rows.unshift(createData(aPdct.name, aPdct.price, aPdct.remain, aPdct.address, aPdct.intro));
                    setChange(!change);
                } else {
                    alert("添加失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("添加失败");
            });
    }

    const renewData = (Pducts) => {
        let rows = [];
        for (let i = 0; i < Pducts.length; i++) {
            rows.push(createData(Pducts[i].name, Pducts[i].price, Pducts[i].remain, Pducts[i].address, Pducts[i].intro));
        }
        setRows(rows);
    }

    const deleteArow = (name) => 
    {
        for (let i = 0; i < rows.length; i++) {
            if (name == rows[i].name) {
                rows.splice(i,1);
                i--;
            }
        }
        setRows(rows);
        setChange(!change);
    }

    const modifyArow = (oldname, aPdct) => {
        
        for (let i = 0; i < rows.length; i++) {
            if (oldname == rows[i].name) {
                rows[i].name = aPdct.name;
                rows[i].price = aPdct.price;
                rows[i].remain = aPdct.remain;
                rows[i].address = aPdct.address;
                rows[i].intro = aPdct.intro == null ? rows[i].intro : aPdct.intro;
            }
        }
    }

    const modifyProduct = (oldname, aPdct) => {
        console.log(aPdct);
        console.log(oldname);
        axios.post(Url + "/product" + "/modify",
            {
                name: aPdct.name,
                price:Number(aPdct.price),
                remain:Number(aPdct.remain),
                address: aPdct.address,
                intro: aPdct.intro,
                oldname: oldname,
            }, {
            timeout: 1000,
            headers: { Authorization: window.sessionStorage.getItem('user_token') }
        })
            .then(function (response) {
                console.log(response)
                if (response.data.data == "success") {
                    modifyArow(oldname, aPdct);
                    setRows(rows);
                    setChange(!change);
                } else {
                    alert("修改失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("修改失败");
            });

    };

    useEffect(async () => {
        await axios.get(Url + "/product" + "/findAll", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
        .then(function (response) {
            renewData(response.data);
        })
        .catch(function (error) {
            console.log(error);
        });
        axios.get(Url + "/credit" + "/getStatus", {
            timeout: 10000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                //console.log(response.data)
                if(response.data.data==='success'||response.data.data==='reject_exit'||
                response.data.data==='exit_checking'){
                    setCanAdd(true);
                }else{
                    setCanAdd(false);
                };
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    return (
        <>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : rows
                        ).map((row) => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell align="center">
                                    {row.price}
                                </TableCell>
                                <TableCell align="center">
                                    {row.remain}
                                </TableCell >
                                <TableCell align="center">
                                    {row.address}
                                </TableCell >
                                <TableCell align="center">
                                    <Intro intro={row.intro}/>
                                </TableCell >
                                <TableCell align="center">
                                    <Button onClick={() => { handleModify(row) }}>修改</Button>
                                    <Button onClick={() => { handleDelete(row.name) }}>删除</Button>
                                </TableCell>
                            </TableRow>
                        ))}

                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                colSpan={6}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: {
                                        'aria-label': 'rows per page',
                                    },
                                    native: true,
                                }}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage="每页商品数量"
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <NewDialog addProduct={addProduct} canAdd={canAdd}/>
            <ModifyDialog setOpen={setModifyOpen} open={modifyOpen}
                Row={modifyRow} modifyProduct={modifyProduct} />
        </>
    );
}

function Intro({ intro }) {

    let strSplice = [];
    for (let i = 0; intro && i < intro.length; i += 10) {
      strSplice.push(intro.substr(i, Math.min(10, intro.length - i)));
    }
    const renderIntro = strSplice.map(str => {
      return (
        <Box>
          {str}
        </Box>
      )
    });
  
    return (
        <Box>
        {renderIntro}
        </Box>
    );
  }
  
