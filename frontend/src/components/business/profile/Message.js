import Button from '@material-tailwind/react/Button';
import Image from '@material-tailwind/react/Image';
import H3 from '@material-tailwind/react/Heading3';
import Icon from '@material-tailwind/react/Icon';
import ProfilePicture from 'assets/img/scenery1.jpg';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

import { useEffect, useRef } from 'react';
import { Url } from '../../../index'
import axios from 'axios';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};
const ListItem = styled('li')(({ theme }) => ({
    margin: theme.spacing(0.5),
}));
const init_tags = [
    '北京景点',
    '上海景点',
    '高贵奢华',
    '低调典雅'
];
const profile = {
    name: '',
    location: '',
    website: '',
    contact_name: '',
    contact_num: '',
    bank_name: '',
    bank_account: '',
    intro: ''
};
const my_tags = [
    { label: '天安门' },
    { label: '长城' },
    { label: '美食小铺' },
    { label: '价格实惠' },
    { label: '武林高手云集地' }
];

function createData(name, location, website, contact_name, contact_num, bank_name, bank_account, intro) {
    return { name, location, website, contact_name, contact_num, bank_name, bank_account, intro };
}

export default function Message() {
    const [myTag, setMyTag] = React.useState([]);
    const [tagName, setTagName] = React.useState([]);
    const [tags, setAllTag] = React.useState([]);
    const [pro, setProfile] = React.useState(profile);


    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setTagName([]);
        //将value中存储的标签添加到用户的标签池
        let exist = 0;
        for (let i = 0; i < myTag.length; i++) {
            if (myTag[i].label == value) {
                exist = 1;
            }
        }
        if (exist == 0) {
            myTag.push({ label: value })
        }
    };
    const handleDelete = (chipToDelete) => () => {
        setMyTag((chips) => chips.filter((chip) => chip.label !== chipToDelete.label));
    };
    const renewMyTag = (data) => {
        let rows = [];
        for (let i = 0; i < data.length; i++) {
            rows.push(data[i]);
        }
        setMyTag(rows);
    }
    const renewAllTag = (data) => {
        let rows = [];
        for (let i = 0; i < data.length; i++) {
            rows.push(data[i]);
        }
        setAllTag(rows);
    }
    const renewProfile = (data) => {
        let row;
        console.log(data)
        row = createData(data.name, data.location, data.website, data.contactName, data.contactTel, data.bankName, data.bankAccount, data.intro);
        console.log(row);
        setProfile(row);
    }
    const saveLabel = () => {
        axios.put(Url + "/bus/saveLabel",
            myTag, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/bus" + "/getLabel", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewMyTag(response.data.data);
                            alert("标签保存成功");
                            return;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                } else {
                    alert("标签保存失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("标签保存失败");
            });
    }
    useEffect(async () => {
        await axios.get(Url + "/credit" + "/getInfo", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewProfile(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });

        await axios.get(Url + "/bus" + "/getLabel", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewMyTag(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });

        await axios.get(Url + "/bus" + "/getAllBusLabel", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewAllTag(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    return (
        <section>
            <div className="flex flex-wrap justify-center">
                <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                    <div className="relative">
                        <div className="w-40 -mt-20">
                            <Image
                                src={ProfilePicture}
                                alt="Profile picture"
                                raised
                                rounded
                            />
                        </div>
                    </div>
                </div>
            </div>

            <div className="text-center my-8">
                <H3 color="gray">{pro.name}</H3>
                <div className="mt-0 mb-2 text-gray-700 font-medium flex items-center justify-center gap-2">
                    <Icon name="place" size="xl" />
                    {pro.intro}
                </div>
            </div>

            <div className="mb-10 py-2 border-t border-gray-200 text-center">
                <div className="flex flex-wrap">
                    <div style={{ width: "35%", marginLeft: "20%", float: "left" }}>
                        <div style={{ marginTop: "50px" }}>
                            <TextField
                                fullWidth
                                label="店铺名称"
                                value={pro.name}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px" }}>
                            <TextField
                                fullWidth
                                label="地理位置"
                                value={pro.location}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px" }}>
                            <TextField
                                fullWidth
                                label="店铺网址"
                                value={pro.website}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px", marginBottom: "30px" }}>
                            <TextField
                                fullWidth
                                label="联系人"
                                value={pro.contact_name}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px", marginBottom: "30px" }}>
                            <TextField
                                fullWidth
                                label="联系方式"
                                value={pro.contact_num}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px", marginBottom: "30px" }}>
                            <TextField
                                fullWidth
                                label="合作银行"
                                value={pro.bank_name}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px", marginBottom: "30px" }}>
                            <TextField
                                fullWidth
                                label="银行账户"
                                value={pro.bank_account}
                                disabled
                            />
                        </div>
                        <div style={{ marginTop: "20px", marginBottom: "30px" }}>
                            <TextField
                                fullWidth
                                label="店铺介绍"
                                value={pro.intro}
                                disabled
                            />
                        </div>
                    </div>
                    <div style={{ width: "30%", float: "right", marginLeft: "5%" }}>
                        <div style={{ marginTop: "50px", height: "100px" }}>
                            <FormControl sx={{ m: 1, width: 150 }} style={{ float: "left" }}>
                                <InputLabel id="tag_select_label">选择标签</InputLabel>
                                <Select
                                    labelId="tag_select_label"
                                    id="tag_select"
                                    value={tagName}
                                    onChange={handleChange}
                                    MenuProps={MenuProps}
                                >
                                    {tags.map((name) => (
                                        <MenuItem
                                            key={name}
                                            value={name}
                                        >
                                            {name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <div style={{ float: "right", marginRight: "20px", marginTop: "20px" }}>
                                <Button color="lightBlue" ripple="light" style={{ width: "140px" }} onClick={saveLabel}>
                                    保存当前标签
                                </Button>
                            </div>
                        </div>
                        <div style={{ marginTop: "20px" }}>
                            <Paper
                                elevation={10}
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    flexWrap: 'wrap',
                                    listStyle: 'none',
                                    p: 0.5,
                                    m: 0,
                                }}
                                component="ul"
                            >
                                {myTag.map((data) => {
                                    return (
                                        <ListItem>
                                            <Chip
                                                label={data.label}
                                                onDelete={handleDelete(data)}
                                                color="primary"
                                            />
                                        </ListItem>
                                    );
                                })}
                            </Paper>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
