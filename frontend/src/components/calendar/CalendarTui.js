import React, { Component } from 'react';
import Calendar from 'tui-calendar'; /* ES6 */
import '../../../node_modules/tui-calendar/dist/tui-calendar.css'
import '../../../node_modules/tui-date-picker/dist/tui-date-picker.css';
import '../../../node_modules/tui-time-picker/dist/tui-time-picker.css';
import '../../assets/styles/calendar.css'

import { Url } from '../../index'
import axios from 'axios';

export var gCalendar;


Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
 

class CalendarTui extends Component {


    constructor(props) {
        super(props);
        this.options = {
            defaultView: 'month',
            taskView: true,    // Can be also ['milestone', 'task']
            scheduleView: true,  // Can be also ['allday', 'time']
            template: {
                popupIsAllDay: function () {
                    return 'All Day';
                },
                popupStateFree: function () {
                    return 'Free';
                },
                popupStateBusy: function () {
                    return 'Busy';
                },
                titlePlaceholder: function () {
                    return 'Subject';
                },
                locationPlaceholder: function () {
                    return 'Location';
                },
                startDatePlaceholder: function () {
                    return 'Start date';
                },
                endDatePlaceholder: function () {
                    return 'End date';
                },
                popupSave: function () {
                    return 'Save';
                },
                popupUpdate: function () {
                    return 'Update';
                },
                popupDetailDate: function (isAllDay, start, end) {
                    // var isSameDate = moment(start).isSame(end);
                    // var endFormat = (isSameDate ? '' : 'YYYY.MM.DD ') + 'hh:mm a';

                    // if (isAllDay) {
                    //     return moment(start).format('YYYY.MM.DD') + (isSameDate ? '' : ' - ' + moment(end).format('YYYY.MM.DD'));
                    // }

                    // return (moment(start).format('YYYY.MM.DD hh:mm a') + ' - ' + moment(end).format(endFormat));
                },
                popupDetailLocation: function (schedule) {
                    return 'Location : ' + schedule.location;
                },
                popupDetailUser: function (schedule) {
                    return 'User : ' + (schedule.attendees || []).join(', ');
                },
                popupDetailState: function (schedule) {
                    return 'State : ' + schedule.state || 'Busy';
                },
                popupDetailRepeat: function (schedule) {
                    return 'Repeat : ' + schedule.recurrenceRule;
                },
                popupDetailBody: function (schedule) {
                    return 'Body : ' + schedule.body;
                },
                popupEdit: function () {
                    return 'Edit';
                },
                popupDelete: function () {
                    return 'Delete';
                }
            },
            month: {
                daynames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                startDayOfWeek: 0,
                narrowWeekend: false
            },
            week: {
                daynames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                startDayOfWeek: 0,
                narrowWeekend: false
            },
            useCreationPopup: true,
            useDetailPopup: true
        };
    }

    styles = {
        Item: { border: '1px dashed #ccc', margin: '10px 10px' },
        user: { color: '#333' },
        content: { color: 'green', fontSize: '22px' }
    }

    componentDidMount() {
        gCalendar = new Calendar('#calendar-tui', this.options);
        gCalendar.on({
            'clickMore': function (e) {
                console.log('clickMore', e);
            },
            'clickSchedule': function (e) {
                console.log('clickSchedule', e);
            },
            'clickDayname': function (date) {
                console.log('clickDayname', date);
            },
            'beforeCreateSchedule': function (e) {
                console.log('beforeCreateSchedule', e);
                saveNewSchedule(e);
            },
            'beforeUpdateSchedule': function (e) {
                var schedule = e.schedule;
                var changes = e.changes;

                // console.log('beforeUpdateSchedule', e);

                if (changes && !changes.isAllDay && schedule.category === 'allday') {
                    changes.category = 'time';
                }
                let state=changes.state||schedule.state;
                let start = changes.start||schedule.start;
                let end = changes.end||schedule.end;
                // console.log(e.schedule)
                axios.put(Url + "/calendar" + "/modify",
                    {
                        "id": schedule.id,
                        "name": changes.title||schedule.title,
                        "state": state.toUpperCase(),
                        "startTime": start._date.Format("yyyy-MM-dd hh:mm:ss"),
                        "endTime": end._date.Format("yyyy-MM-dd hh:mm:ss"),
                        "privateControl": changes.isPrivate||schedule.isPrivate,
                        "allDay": changes.isAllDay||schedule.isAllDay,
                        "location": changes.location|| schedule.location
                    }, {
                    timeout: 1000,
                    headers: { Authorization: window.sessionStorage.getItem('user_token') ,
                }
                })
                    .then(function (response) {
                        console.log(response)
                        if (response.data.data == "success") {
                            gCalendar.updateSchedule(schedule.id, schedule.calendarId, changes);
                        } else {
                            alert("修改失败");
                            return;
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response&&error.response.status === 402) {
                            alert("您的账号已被封禁，无法完成该操作");
                            return;
                        }
                        alert("修改失败");
                    });
                console.log(changes)
            },
            'beforeDeleteSchedule': function (e) {
                // console.log('beforeDeleteSchedule', e);
                // console.log(typeof e.schedule.id);
                // console.log(e.schedule.id);

                axios.delete(Url + "/calendar" + "/delete", {
                    params: {
                        id:e.schedule.id,
                    },
                    headers: { Authorization: window.sessionStorage.getItem('user_token') }
                }
                )
                    .then(function (response) {
                        console.log(response)
                        if (response.data.data == "success") {
                            alert('删除成功');
                            gCalendar.deleteSchedule(e.schedule.id, e.schedule.calendarId);
                            return;
                        } else {
                            alert('删除失败');
                            return;
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response&&error.response.status === 402) {
                            alert("您的账号已被封禁，无法完成该操作");
                            return;
                        }
                        if (error.request.status == 404) {
                            alert("该记录不存在")
                        } else {
                            alert("删除失败");
                        }
                    });

            },
            'afterRenderSchedule': function (e) {
                var schedule = e.schedule;
            },
            'clickTimezonesCollapseBtn': function (timezonesCollapsed) {
                console.log('timezonesCollapsed', timezonesCollapsed);

                if (timezonesCollapsed) {
                    gCalendar.setTheme({
                        'week.daygridLeft.width': '77px',
                        'week.timegridLeft.width': '77px'
                    });
                } else {
                    gCalendar.setTheme({
                        'week.daygridLeft.width': '60px',
                        'week.timegridLeft.width': '60px'
                    });
                }
            }
        })

        axios.get(Url + "/calendar/findAll", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data);
                for (let j = 0; j < response.data.data.length; j++) {
                    console.log(response.data.data[j].id)
                    gCalendar.createSchedules([
                        {
                            id: response.data.data[j].id,
                            title: response.data.data[j].name,
                            start: response.data.data[j].startTime,
                            end: response.data.data[j].endTime,
                            location: response.data.data[j].location,
                            isPrivate: response.data.data[j].privateControl,
                            state: response.data.data[j].state,
                            isAllDay: response.data.data[j].allDay,
                            category: response.data.data[j].allDay ? 'allday' : 'time',
                            color: getColor(response.data.data[j].id),
                            bgColor: getColor(response.data.data[j].id),
                            dueDateClass: '',
                        }
                    ]);
                }

                //gCalendar.createSchedules([response.data.data]);
                //renewData(response.data);
            })
            .catch(function (error) {
                if (error.response&&error.response.status === 402) {
                    return;
                }
                alert('添加失败');
                console.log(error);
            });

    }

    render() {
        return (
            <div>

                <div id="menu" >
                    <span id="menu-navi">
                        <button type="button" className="move-today" onClick={() => { gCalendar.today(); }}>今天</button>
                        <button type="button" className="move-day" onClick={() => { gCalendar.prev(); }}>
                            <i className="calendar-icon ic-arrow-line-left" ></i>
                        </button>
                        <button type="button" className="move-day" onClick={() => { gCalendar.next(); }}>
                            <i className="calendar-icon ic-arrow-line-right" ></i>
                        </button>
                    </span>
                    <span id="renderRange"></span>
                </div>

                <div id="calendar-tui"></div>
            </div>)
    }

}

export default CalendarTui;

function saveNewSchedule(scheduleData) {

    let id = 0;
    let calendar = scheduleData.calendar;
    let schedule = {
        id: id,
        title: scheduleData.title,
        isAllDay: scheduleData.isAllDay,
        start: scheduleData.start,
        end: scheduleData.end,
        category: scheduleData.isAllDay ? 'allday' : 'time',
        dueDateClass: '',
        color: getColor(id),
        bgColor: getColor(id),
        dragBgColor: getColor(id),
        borderColor: getColor(id),
        location: scheduleData.location,
        isPrivate: scheduleData.isPrivate,
        state: scheduleData.state
    };
    if (calendar) {
        schedule.calendarId = calendar.id;
        schedule.color = calendar.color;
        schedule.bgColor = calendar.bgColor;
        schedule.borderColor = calendar.borderColor;
    }
    console.log(scheduleData.start._date)

    axios.post(Url + "/calendar" + "/add",
        JSON.stringify({
            "id": 0,
            "name": scheduleData.title,
            "state": scheduleData.state.toUpperCase(),
            "startTime": scheduleData.start._date.Format("yyyy-MM-dd hh:mm:ss"),
            "endTime": scheduleData.end._date.Format("yyyy-MM-dd hh:mm:ss"),
            "privateControl": scheduleData.isPrivate,
            "allDay": scheduleData.isAllDay,
            "location": scheduleData.location
        }), {
        timeout: 1000,
        headers: {
            Authorization: window.sessionStorage.getItem('user_token'),
            'Content-Type': 'application/json'
        }
    })
        .then(function (response) {
            console.log(response)
            if (response.data.msg == "request success") {
                console.log(response.data)
                schedule.id=response.data.data
                gCalendar.createSchedules([schedule]);
                alert("添加成功");
                return;
            } else {
                alert("添加失败");
                return;
            }
        })
        .catch(function (error) {
            console.log(error);
            if (error.response&&error.response.status === 402) {
                alert("您的账号已被封禁，无法完成该操作");
                return;
            }
            alert("添加失败");
        });
}

function getColor(i) {
    return '#' + Math.floor(Math.random(i) * 16777215).toString(16);
}