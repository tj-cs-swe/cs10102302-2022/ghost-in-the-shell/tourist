import React from 'react'
import {Modal} from 'antd'
import {Editor} from 'react-draft-wysiwyg'
import draftjs from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import { Input } from "@material-tailwind/react";
import { Link } from 'react-router-dom';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@material-tailwind/react/Button';
import Typography from '@mui/material/Typography';
 
export default class RichText extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showRichText: false,
            //editorContent: this.props.content,
            editorContent: "TEST",
            editorState: ""
        };
        this.handleClearContent = this.handleClearContent.bind(this);
        this.handleGetText = this.handleGetText.bind(this);
        this.onEditorStateChange = this.onEditorStateChange.bind(this);
        this.onEditorChange = this.onEditorChange.bind(this);
    }
   
    handleClearContent = () => {  //清空文本
        this.setState({
            editorState: ''
        })
    }
    handleGetText = () => {    //获取文本内容
        this.setState({
            showRichText: true
        })
    }
    onEditorStateChange = (editorState) => {   //编辑器的状态
        this.setState({
            editorState
        })
    }
    onEditorChange = (editorContent) => {   //编辑器内容的状态
        this.setState({
            editorContent
        })
        this.props.handleContent(editorContent);
    }

    render(){
        const { editorState, editorContent } = this.state;
        return (
            <div>
                {/* 
                <Card>
                    <Button type="primary" onClick={this.handleClearContent}>清空内容</Button>
                    <Button type="primary" onClick={this.handleGetText} style={{marginLeft: 10}}>获取html文本</Button>
                </Card>
                */}
                <Card>
                <CardContent>
                 {/**
                    <div class="relative h-16">
                    <div class="h-12 absolute inset-x-0 bottom-0">
                        <Input variant="outlined" label="Outlined" placeholder="标题" />
                    </div>
                    </div>
                  */}
                    <Editor 
                        editorState={editorState}
                        onEditorStateChange={this.onEditorStateChange}
                        onContentStateChange={this.onEditorChange}
                        toolbarClassName="toolbarClassName"
                        wrapperClassName="wrapperClassName"
                        editorClassName="editorClassName"
                    />
                    </CardContent>
                    <CardActions>
                        
                    </CardActions>
                </Card>
                <Modal 
                    title="富文本"
                    visible={this.state.showRichText}
                    onCancel={() =>{
                        this.setState({
                            showRichText: false
                        })
                    }}
                    footer={null}>
                    {draftjs(this.state.editorContent)}
                </Modal>
            </div>
        )
    }
}