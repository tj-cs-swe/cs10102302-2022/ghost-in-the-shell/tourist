import * as React from 'react';
import { useEffect, useRef } from 'react';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Link } from 'react-router-dom';
import Button from '@material-tailwind/react/Button';
//import Button from '@mui/material/Button';
import { Input } from "@material-tailwind/react";
import { Textarea } from "@material-tailwind/react";
import Editor from "./Editor";

import { Url } from '../../index'
import axios from 'axios';

const flags_false = false;
const flags_true = true;

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


export default class View extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        title : "",
        content : "",
        time : new Date().toLocaleDateString(),
    };
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleContent = this.handleContent.bind(this);
  }

  addPassage = (id, title, content, time) => {
    if(title == "" || content == "") {
      alert("标题和内容不能为空");
      return true;
    }
    console.log(id);
    console.log(title);
    console.log(content);
    console.log(time);
    axios.post(/*"https://mock.apifox.cn/m1/935986-0-default/passage/add"*/  Url + "/passage/add"  ,
        {
            //"id": id,
            "title": title,
            //"content": content.blocks[0].text,
            "content": content,
            //"time": time,
        }, {
        timeout: 1000,
        //*
        headers: {
            Authorization: window.sessionStorage.getItem('user_token'),
            'Content-Type': 'application/json'
        }
        //*/
    })
        .then(function (response) {
            console.log(response)
            if (response.data.data === "Add Success") {
                alert("成功");
            } else {
                alert("失败");
                return;
            }
        })
        .catch(function (error) {
            console.log(error);
            if (error.response&&error.response.status === 402) {
              alert("您的账号已被封禁，无法进行操作");
              return;
            }
            alert("失败");
        });
    return true;
  }

  handleTitleChange(event) {
    this.setState({title: event.target.value});
  }

  handleContentChange(event) {
    this.setState({content: event.target.value});
  }

  handleContent(editorContent){
    this.setState({content: editorContent});
  }

  componentWillMount () { 
    this.setState({
      title : this.props.title,
      content : this.props.content
    })
  }

  render () {
    var title = this.state.title;
    var content = this.state.content;
  return (
    <Box class="w-full">

    <div class="left-0">

      <br></br>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Item>发布者:{this.props.publisher}</Item>
        </Grid>
        <Grid item xs={4} >
          <Item>发布日期:{this.state.time}</Item>
        </Grid>
        <Grid item xs={4} >
          <Item>
            

<Link sx={{ width: '50%', maxWidth: 500 }}
to={{
    pathname: '/forum',
    state: {  // 页面跳转要传递的数据，如下
        data1: {},
        data2: []
    },
}}
>

            放弃编辑
            
</Link>
          </Item>
        </Grid>
      </Grid>
      </div>

      <br></br>

      <div className="items-end gap-1 w-full">
      <div class="relative h-16">
          <div class="h-12 absolute inset-x-0 bottom-0">
              <Input 
                variant="outlined" 
                label="Outlined" 
                placeholder="标题" 
                value={title} 
                onChange={this.handleTitleChange}
              >
              </Input>
          </div>
      </div>

      <div class="relative h-60">
          <div class="h-48 absolute inset-x-0 bottom-0">
              <Textarea 
                variant="outlined" 
                label="Outlined" 
                placeholder="内容" 
                value={content} 
                onChange={this.handleContentChange}
              >
              </Textarea>
          </div>
      </div>

        {/*
      <Card>
        <div class="relative h-16">
          <div class="h-12 absolute inset-x-0 bottom-0">
            <Input variant="outlined" label="Outlined" placeholder="标题" />
          </div>
        </div>
      </Card>
        <br></br>
        <Textarea variant="outlined" label="Outlined" placeholder="内容" />
        */}
      </div>
{/*
      <Editor
        content = {this.state.content}
        handleContent = {this.handleContent}
      ></Editor>
*/}
      <Button
        disabled = {(this.state.title != "" && this.state.content != "" && this.state.title != null && this.state.content != null) ? false : true}
        variant="outlined"
        color="lightBlue"
        size="md"
        ripple="dark"
        onClick={() => this.addPassage(1, this.state.title, this.state.content, this.state.time)}     
        >

{(this.state.title != "" && this.state.content != "" && this.state.title != null && this.state.content != null)  && 
        <Link sx={{ width: '50%', maxWidth: 500 }}
          to={{
              pathname: '/forum',
              state: {  // 页面跳转要传递的数据，如下
                  title : this.state.title,
                  content: this.state.content
              },
          }}
        >
            发布
        
        </Link>
}
{(this.state.title == "" || this.state.content == "" || this.state.title == null || this.state.content == null)  && 
            <div>发布</div>
}
      </Button> 

    <br></br>
      
    </Box>
  );
}
}