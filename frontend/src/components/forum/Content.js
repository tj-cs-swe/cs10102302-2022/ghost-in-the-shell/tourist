import * as React from 'react';
import Title from './Title'
import SearchBox from './Search'
import BrowseTable from './Table'

export default class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            forumList : []
        };
    }

    handlerReceiveForumList () {
        return (data)=>{
            this.setState({
                forumList: data
            })
        }
    }

    render () {
        return (
            <section className="relative py-16 bg-gray-100">

                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <Title/>

                            <br />

                            <BrowseTable />
                            
                            {/*<SearchBox onReceiveForumList = {this.handlerReceiveForumList()} />*/ }


                            <br /><br /><br />
                        </div>
                    </div>
                </div>
            </section >
        ); 
    }
}