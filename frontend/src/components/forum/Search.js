import React from "react";
import Input from '@material-tailwind/react/Input';
import Button from '@material-tailwind/react/Button';


export default class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '', 
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({keyword: event.target.value});
    }

    searchContent = (keyword) => {

        var forumList = [];

        forumList.push({
            "title": "凑数帖", 
            "publisher": "打工仔",
            "time": "2022/05/28",
            "operation": "查看"
        });

        this.props.onReceiveSearchKeyword(keyword);
    }

    render () {
        var keyword = this.state.keyword;
        return (
            <>
                    <div className="mt-10 mb-10 px-4" style={{ height: "20px" }}>
                        <div style={{ float: "left", width: "50%" }}>
                            <Input 
                                type="text" 
                                color="lightBlue" 
                                placeholder="关键字" 
                                id="keyword_title" 
                                value={keyword} 
                                onChange={this.handleChange}
                            />
                        </div>                
                        <Button
                            variant="outlined"
                            color="lightBlue"
                            size="md"
                            ripple="dark"
                                onClick={() => {
                                    this.state.adcode = this.searchContent(keyword)                                                                                                                  
                                    if(this.state.adcode == 'error'){                                           
                                        this.setState({
                                            tag:"查询失败"
                                        })                                           
                                    }
                                    else{
                                        this.state.tag="查询成功"
                                    }                                         
                                }}
                        >
                            查询
                        </Button> 
                    {/*<p className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">{this.state.tag}</p> */}
                </div>
             </>
        );
    }
}
