
/*
export default class BrowseTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lists: []
        }
    }

    componentDidMount () {
        const lists = this.state.lists;
        lists.push({
            "title": "我很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长", 
            "publisher": "汪汪队",
            "time": "2022/05/29",
            "operation": "查看 编辑 删除"
        });
        lists.push({
            "title": "震惊！", 
            "publisher": "热心市民",
            "time": "2022/05/28",
            "operation": "查看"
        });
        lists.push({
            "title": "凑数帖", 
            "publisher": "打工仔",
            "time": "2022/05/28",
            "operation": "查看"
        });
        this.setState({lists:lists})
    }

    render () {
        return (
            <div>
            <table class="text-lg w-2/3 table-fixed border-solid border-2 border-indigo-600 divide-y divide-solid truncate">
            <thead class="text-left">
                <tr class="divide-x divide-dashed">
                <th class="w-2/5 overflow-x-auto">标题</th>
                <th class="w-1/5">发布者</th>
                <th class="w-1/5">发布/修改时间</th>
                <th class="w-1/5">可用操作</th>
                </tr>
            </thead>
            <tbody class="divide-y divide-dashed overflow-y-auto">
                {this.state.lists.map((data)=>{
                    console.log(data);
                    return (
                        <tr class="divide-x divide-dashed">
                            <td class="truncate">{data.title}</td>
                            <td class="truncate">{data.publisher}</td>
                            <td class="truncate">{data.time}</td>
                            <td class="truncate"><button>查看</button></td>
                        </tr>
                    )
                })}
            </tbody>
            </table>

          </div>
        ); 
    }
}
*/
import * as React from 'react';
import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { visuallyHidden } from '@mui/utils';
import Input from '@material-tailwind/react/Input';
import SearchBox from './Search'
import { Link } from 'react-router-dom';
import Button from '@material-tailwind/react/Button';

import { Url } from '../../index'
import axios from 'axios';

let nickname;

function createData(id, title, publisher, time) {
  return {
    id,
    title,
    publisher,
    time,
  };
}

let total_rows = [
  /*
  createData('凑数帖12', '热心市民', '2022/05/17'),
  createData('凑数帖11', '汪汪打工仔队', '2022/05/12'),
  createData('凑数帖1', '汪汪打工仔队', '2022/05/27'),
  createData('凑数帖2', '热心市民', '2022/05/22'),
  createData('震惊！', '打工仔', '2022/05/23'),
  createData('凑数帖8', '热心市民', '2022/05/21'),
  createData('凑数帖9', '打工仔', '2022/05/27'),
  createData('凑数帖10', '热心市民', '2022/05/26'),
  createData('凑数帖3', '打工仔', '2022/05/20'),
  createData('凑数帖4', '打工仔', '2022/05/23'),
  createData('凑数帖5', '汪汪队', '2022/05/22'),
  createData('凑数帖6', '汪汪队', '2022/05/29'),
  createData('凑数帖7', '汪汪队', '2022/05/28'),
  createData('TEST', '汪汪队', '2022/06/01'),
  */
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: 'title',
    numeric: false,
    disablePadding: true,
    label: '标题',
  },
  {
    id: 'publisher',
    numeric: true,
    disablePadding: false,
    label: '发布者',
  },
  {
    id: 'time',
    numeric: true,
    disablePadding: false,
    label: '发布时间',
  },
];


const adminCells = [
  {
    id: 'title',
    numeric: false,
    disablePadding: true,
    label: '系统公告',
  },
  {
    id: 'publisher',
    numeric: true,
    disablePadding: false,
    label: '攻壳机动队',
  },
  {
    id: 'time',
    numeric: true,
    disablePadding: false,
    label: '2022/05/30',
  },
];

function EnhancedTableHead(props) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
        props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            //padding={headCell.disablePadding ? 'none' : 'normal'}
            padding="2rem"
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}

      </TableRow>
{
      <TableRow>
        {adminCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            //padding={headCell.disablePadding ? 'none' : 'normal'}
            padding="2rem"
          >
<Link sx={{ width: '100%', maxWidth: 500 }}
to={{
    pathname: '/passage',
    state: {  // 页面跳转要传递的数据，如下
        title: headCell.label
    },
}}
>
            {headCell.label}
</Link>
          </TableCell>
        ))}

      </TableRow>
}
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const EnhancedTableToolbar = (props) => {
  const { numSelected, searchContent } = props;
  const [key, setKey] = React.useState('');

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
      }}
    >
        <Typography
          sx={{ flex: '1 1 100%' }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          帖子列表
          {/*key*/}
        </Typography>

        <SearchBox onReceiveSearchKeyword = {searchContent} />

    <div style={{ float: "right", width: "10%", height: "20px" }}>
        {window.sessionStorage.getItem('nickname') != null && 
        <Button 
          variant="outlined"
          color="lightBlue"
          size="md"
          ripple="dark" 
        >
<Link sx={{ width: '100%', maxWidth: 500 }}
to={{
    pathname: '/edit',
    state: {  // 页面跳转要传递的数据，如下
        //createData: {createData},
        title: "",
        publisher: nickname
    },
}}
>
          新建
</Link>
      </Button>
      }
      </div>

    {/*
        <div id="pos_choice" style={{ width: '30%'}}>
            <Input 
                type="text" 
                color="lightBlue" 
                placeholder="输入" 
                id="wea_city" 
                value={search_title} 
                //onChange={this.handleChange}
            />
        </div>
    */}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
  searchContent: PropTypes.func.isRequired,
};

function renewNickname(data) {
  console.log(data);
  //setPublisher(data.nickname);
  nickname = data.nickname;
  console.log(nickname);
}

export default function EnhancedTable() {
  const [publisher, setPublisher] = React.useState('');
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('time');
  const [rows, setRows] = React.useState(total_rows);
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  //const [key, setKey] = React.useState('');

  
  const handleRenewData = (key) => {
    let rows = [];
    for (let i = 0; i < total_rows.length; i++) {
        if (total_rows[i].title.includes(key) || total_rows[i].publisher.includes(key) || total_rows[i].time.includes(key)) {
            rows.push(createData(total_rows[i].id, total_rows[i].title, total_rows[i].publisher, total_rows[i].time));
        }
    }
    setRows(rows);
  }


  useEffect(async () => {
    await axios.get(/*"https://mock.apifox.cn/m1/935986-0-default/passage/findAll"*/  Url + "/passage/findAll"  , {
          timeout: 1000,
          headers: {
              Authorization: window.sessionStorage.getItem('user_token'),
              'Content-Type': 'application/json'
          }
      })
          .then(function (response) {
              console.log(response.data.data)
              renewData(response.data.data);
          })
          .catch(function (error) {
              console.log(error);
          });
    await axios.get(Url + "/user" + "/getMessage", {
          timeout: 1000,
          headers: {
              Authorization: window.sessionStorage.getItem('user_token')
          }
      })
          .then(function (response) {
              console.log(response.data.data)
              renewNickname(response.data.data);
          })
          .catch(function (error) {
              console.log(error);
          });
    }, []);
  
  /*
  const renewNickname = (data) => {
      console.log(data);
      setPublisher(data.nickname);
  }
  */

  const renewData = (Pducts) => {
    //let rows = [];
    total_rows = [];
    for (let i = 0; i < Pducts.length; i++) {
      total_rows.push(createData(Pducts[i].id, Pducts[i].title, Pducts[i].nickName, Pducts[i].createTime));
    }
    console.log(total_rows);
    handleRenewData("");
    //setRows(rows);
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.title);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, title) => {
    const selectedIndex = selected.indexOf(title);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, title);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    //window.location.href="./Passage"

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (title) => selected.indexOf(title) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <EnhancedTableToolbar numSelected={selected.length} searchContent={handleRenewData} />
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />


            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                //.slice(1, 5)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.title);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.name)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                    >

                        <TableCell
                          padding="2rem"
                          component="th"
                          id={labelId}
                          scope="row"
                          //padding="none"
                        >

<Link sx={{ width: '100%', maxWidth: 500 }}
to={{
    pathname: '/passage',
    state: {  // 页面跳转要传递的数据，如下
        id: row.id,
        title: row.title,
        publisher: nickname
    },
}}
>
                            {row.title}
</Link>
                        </TableCell>
                        <TableCell align="right">{row.publisher}</TableCell>
                        <TableCell align="right">{row.time}</TableCell>

                        </TableRow>

                  );
                })}
              {emptyRows > 0 && (

                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>

              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          labelRowsPerPage="每页数量"
        />
      </Paper>
    </Box>
  );
}
