import H3 from '@material-tailwind/react/Heading3';

export default function Title() {
    return (

        <div className="text-center my-8">
            <H3 color="gray">旅游论坛</H3>
        </div>
    );
}