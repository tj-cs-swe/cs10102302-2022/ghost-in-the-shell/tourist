import Card from '@material-tailwind/react/Card';
import CardImage from '@material-tailwind/react/CardImage';
import CardBody from '@material-tailwind/react/CardBody';
import Icon from '@material-tailwind/react/Icon';
import H4 from '@material-tailwind/react/Heading4';
import H6 from '@material-tailwind/react/Heading6';
import LeadText from '@material-tailwind/react/LeadText';
import Paragraph from '@material-tailwind/react/Paragraph';
import StatusCard from 'components/landing/StatusCard';
import Teamwork from 'assets/img/teamwork.jpeg';
import { Link } from 'react-router-dom';

export default function WorkingSection() {
    return (
        <section className="pb-20 bg-gray-100 -mt-32">
            <div className="container max-w-7xl mx-auto px-4">
                <div className="flex flex-wrap relative z-50">
                    <StatusCard color="red" icon="check" title="行程规划">
                        查看我的<Link to='/calender'>行程日历</Link>
                    </StatusCard>
                    <StatusCard
                        color="teal"
                        icon="search"
                        title="信息查询"
                    >
                       <Link to='/line'>出行路线？</Link>
                       <Link to='/weather'>天气情况？</Link>
                       <Link to='/spot'>景点百科？</Link>你需要的信息都在这里
                    </StatusCard>
                    <StatusCard
                        color="lightBlue"
                        icon="comment"
                        title="攻略分享"
                    >
                        <Link to='/forum'>分享你的旅行见闻</Link> 10w+旅行笔记帮你避雷防踩坑
                    </StatusCard>
                </div>

                <div className="flex flex-wrap items-center mt-32">
                    <div className="w-full md:w-5/12 px-4 mx-auto">
                        <div className="text-blue-gray-800 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-white">
                            <Icon name="people" size="3xl" />
                        </div>
                        <H4 color="gray">还在为出行规划而烦恼？</H4>
                        <LeadText color="blueGray">
                            制定出行计划要浏览无数个网站？<br></br>
                            害怕被网红营销欺骗？
                        </LeadText>
                        <LeadText color="blueGray">
                            在这里，你可以找到所有旅行信息，<br></br>
                            我们将根据您的喜好匹配适合您的最佳出游线路<br></br>
                            1w+同路er与您分享旅行见闻
                        </LeadText>
                        <a
                            href="/register"
                            className="font-medium text-light-blue-500 mt-2 inline-block"
                        >
                            立即体验
                        </a>
                    </div>

                    <div className="w-full md:w-4/12 px-4 mx-auto flex justify-center mt-24 lg:mt-0">
                        <Card>
                            <CardImage alt="Card Image" src={Teamwork} />
                            <CardBody>
                                <H6 color="gray">最优质的出行体验</H6>
                                <Paragraph color="blueGray">
                                🔒 多重风险控制设计，全方位保障您的行程安全<br></br>
                                👨‍💼 人工审核所有商家信息，出行更放心
                                </Paragraph>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        </section>
    );
}
