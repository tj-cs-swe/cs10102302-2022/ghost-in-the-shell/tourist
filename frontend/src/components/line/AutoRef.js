import { ori_city, des_city } from './Content';
var autoori;
var autodes;
export default function AutoRef(flag,value) {
    if (flag == 0&&autoori==null&&autodes==null) {
        window.AMap.plugin('AMap.Autocomplete', function () {
            // 实例化Autocomplete
            autoori = new window.AMap.Autocomplete({
                input: "ori",
                city:"全国"
            });
            autodes = new window.AMap.Autocomplete({
                input: "des",
                city:"全国"
            });
        })
    }
    else if (flag == 1) {
        autoori.setCity(value);
    }
    else if (flag == 2) {
        autodes.setCity(value);
    }
}