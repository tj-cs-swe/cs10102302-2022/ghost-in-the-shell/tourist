import * as React from 'react';
import H3 from '@material-tailwind/react/Heading3';
import Button from '@material-tailwind/react/Button';
import Input from '@material-tailwind/react/Input'
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import { SelectChangeEvent } from '@mui/material/Select';

import Loadmap from './Loadmap'
import Createroute from './Drawroute';
import { useEffect, useRef } from 'react';
import AutoRef from './AutoRef';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};
const names = [
    '全国','北京市', '天津市', '上海市', '重庆市',
    '河北省', '山西省', '辽宁省', '吉林省', '黑龙江省', '江苏省', '浙江省', '安徽省', '福建省', '江西省', '山东省', '河南省', '湖北省', '湖南省', '广东省', '海南省', '四川省', '贵州省', '云南省', '陕西省', '甘肃省', '青海省', '台湾省',
    '内蒙古自治区', '广西壮族自治区', '西藏自治区', '宁夏回族自治区', '新疆维吾尔自治区',
    '香港特别行政区', '澳门特别行政区',
];
export var ori_city;
export var des_city;
export default function Content() {
    const oriRef = useRef();
    const desRef = useRef();
    const [oriCityName, setOriCityName] = React.useState([]);
    const [desCityName, setDesCityName] = React.useState([]);

    const handleOriCityChange = (event) => {
        const {
            target: { value },
        } = event;
        ori_city = value;
        console.log(value);
        setOriCityName(value);
        AutoRef(1,value);
    };
    const handleDesCityChange = (event) => {
        const {
            target: { value },
        } = event;
        des_city = value;
        console.log(value);
        setDesCityName(value);
        AutoRef(2,value);
    };
    useEffect(async () => {
        Loadmap();
    })
    return (
        <section className="relative py-16 bg-gray-100">
            <div className="container max-w-7xl px-4 mx-auto">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                    <div className="px-6">
                        <div className="text-center my-8">
                            <H3 id="init_title" color="gray" style={{ display: "block" }}>路线查询</H3>
                            <h5 id="title" style={{ size:"50px",display: "block" }}> (若地图不显示，请刷新页面) </h5>
                        </div>
                        <br />
                        <div id="container" style={{ width: '68%', height: '800px', float: 'left', marginBottom: "75px" }}></div>
                        <div id="pos_choice" style={{ width: '30%', height: '350px', float: 'left', marginLeft: '20px' }}>
                            <div className="mb-10 px-4" >
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel id="method-label">出行方式</InputLabel>
                                        <Select
                                            labelId="method-label"
                                            id="method"
                                            label="选择出行方式"
                                        >
                                            <MenuItem value={10}>步行</MenuItem>
                                            <MenuItem value={20}>驾车</MenuItem>
                                            <MenuItem value={30}>公共交通</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                            </div>
                            <div className="mb-10 px-4" style={{ height: "30px" }}>
                                <div style={{ float: "left", width: "30%" }}>
                                    {/* <Input type="text" color="lightBlue" placeholder="出发城市" id="ori_city" /> */}
                                    <FormControl sx={{ m: 1, width: "100%" }}>
                                        <InputLabel id="demo-multiple-name-label">出发城市</InputLabel>
                                        <Select
                                            labelId="demo-multiple-name-label"
                                            id="demo-multiple-name"
                                            value={oriCityName}
                                            onChange={handleOriCityChange}
                                            input={<OutlinedInput label="出发城市" inputRef={oriRef} />}
                                            MenuProps={MenuProps}
                                        >
                                            {names.map((name) => (
                                                <MenuItem
                                                    key={name}
                                                    value={name}
                                                >
                                                    {name}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div>
                                <div style={{ float: "left", width: "60%", marginLeft: "10%", marginTop: "15px" }}>
                                    <Input type="text" color="lightBlue" placeholder="出发地" id="ori" />
                                </div>
                            </div>
                            <div className="mb-10 px-4" style={{ marginTop: "50px", marginBottom: "50px" }}>
                                <div style={{ float: "left", width: "30%" }}>
                                    {/* <Input type="text" color="lightBlue" placeholder="目的城市" id="des_city" /> */}
                                    <FormControl sx={{ m: 1, width: "100%" }} style={{ marginBottom: "20px" }}>
                                        <InputLabel id="demo-multiple-name-label">目的城市</InputLabel>
                                        <Select
                                            labelId="demo-multiple-name-label"
                                            id="demo-multiple-name"
                                            value={desCityName}
                                            onChange={handleDesCityChange}
                                            input={<OutlinedInput label="目的城市" inputRef={desRef} />}
                                            MenuProps={MenuProps}
                                        >
                                            {names.map((name) => (
                                                <MenuItem
                                                    key={name}
                                                    value={name}
                                                >
                                                    {name}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div>
                                <div style={{ float: "left", width: "60%", marginLeft: "10%", marginTop: "15px" }}>
                                    <Input type="text" color="lightBlue" placeholder="目的地" id="des" />
                                </div>
                            </div>
                            <div style={{ marginLeft: "35%", marginTop: "140px" }}>
                                <Button
                                    variant="outlined"
                                    color="lightBlue"
                                    size="lg"
                                    ripple="dark"
                                    onClick={() => {
                                        Createroute();
                                    }}
                                >
                                    查询
                                </Button>
                            </div>
                        </div>
                        <div id="mode" className="mb-10 px-4" style={{ width: "20%", height: "30px", float: "left", marginLeft: "10px", display: "none" }}>
                            <Box sx={{}}>
                                <FormControl fullWidth>
                                    <InputLabel id="method-label">路线偏好</InputLabel>
                                    <Select
                                        labelId="method-label"
                                        id="mo"
                                        label="选择路线偏好"
                                        onChange={() => {
                                            Createroute();
                                        }}
                                    >
                                        <MenuItem value={10}>最短时间</MenuItem>
                                        <MenuItem value={20}>最少费用</MenuItem>
                                        <MenuItem value={30}>最少换乘</MenuItem>
                                    </Select>
                                </FormControl>
                            </Box>
                        </div>
                        <div id="display_route" style={{ width: "30%", height: "450px", float: "right", overflowX: "hidden" }}>

                        </div>
                        <></>

                        <br /><br /><br />
                    </div>
                </div>
            </div>
        </section >
    );
}