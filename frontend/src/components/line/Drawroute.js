import { walking, driving, transfer } from './Loadmap'
import Changemethod from "./Changemethod";
import { ori_city,des_city } from './Content';
const walk = "walking"
const drive = "driving";
const transit = "transit"
//最快捷模式LEAST_TIME
//最经济模式LEAST_FEE
//最少换乘模式LEAST_TRANSFER
function Judgemode(mode) {
    if (mode == "BUS") {
        return "-166px -104px";
    }
    else if (mode == "SUBWAY") {
        return "-245px -101px"
    }
    else if (mode == "RAILWAY") {
        return "-208px -104px"
    }
    else {
        return "";
    }
}
function Judgedir(orientation, action) {
    var dirpos;
    if (action == "直行" || orientation == "北") {
        dirpos = "-45px -23px"
    }
    else if (action == "右转" || orientation == "东") {
        dirpos = "-124px -23px"
    }
    else if (action == "左转" || orientation == "西") {
        dirpos = "-87px -23px"
    }
    else if (action == "向左后方行走") {
        dirpos = "-326px -23px"
    }
    else if (action == "向右后方行走") {
        dirpos = "-366px -23px"
    }
    else if (orientation == "东南") {
        dirpos = "-285px -23px"
    }
    else if (action == "向右前方行走" || orientation == "东北") {
        dirpos = "-206px -23px"
    }
    else if (orientation == "西南") {
        dirpos = "-250px -23px"
    }
    else if (action == "向左前方行走" || orientation == "西北") {
        dirpos = "-165px -23px"
    }
    else if (orientation == "南") {
        dirpos = "-330px -23px"
    }
    return dirpos;
}
function AddWalkingroute(route) {
    document.getElementById("pos_choice").style.height = "350px";
    document.getElementById("mode").style.display = "none";

    var duration = (parseInt(route.routes[0].time) / 60).toFixed(2);
    var distance = (parseInt(route.routes[0].distance) / 1000).toFixed(2);

    document.getElementById("display_route").innerHTML = "<div id=\"display\" style=\"height:450px;margin: 0;padding: 0;color: #565656;line-height: 2;word-wrap: break-word;border: 2px solid silver;border-radius: 10px ;overflow-y: scroll \"></div>"
    document.getElementById("display").innerHTML += "<b style=\"font-family:楷体;font-size:20px\">预计用时" + duration.toString() + "分钟(" + distance.toString() + "公里)</b>";
    document.getElementById("display").innerHTML += "<div id=\"action\" ></div>";
    document.getElementById("action").innerHTML += "<dt id=\"" + id + "\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";

    document.getElementById("action").innerHTML += "<dt id=\"start\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";
    document.getElementById("start").innerHTML += "<div style=\"width: 21px;height: 21px;border: 1px solid #ddd;border-radius:50%;float:left;background-image: url(https://s3.bmp.ovh/imgs/2022/06/01/a241c633dbfaad0e.png);background-position: -47px -104px;\"></div>";
    document.getElementById("start").innerHTML += "<div style=\"background-color: #e5e7e8;left: -22px;top: 20px;width: 4px;height: 100%;float:right;z-index: 9;\"></div>"
    document.getElementById("start").innerHTML += "<b style=\"font-family:楷体;font-size:20px\">起点</b>";
    var i;
    for (i = 0; i < route.routes[0].steps.length; i++) {
        var id = "act" + i.toString();
        var action = route.routes[0].steps[i].action;
        var orientation = route.routes[0].steps[i].orientation;
        document.getElementById("action").innerHTML += "<dt id=\"" + id + "\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";
        document.getElementById(id).innerHTML += "<div style=\"width: 21px;height: 21px;border: 1px solid #ddd;border-radius:50%;float:left;background-image: url(https://s3.bmp.ovh/imgs/2022/06/01/a241c633dbfaad0e.png);background-position: " + Judgedir(orientation, action) + ";\"></div>";
        document.getElementById(id).innerHTML += "<div style=\"background-color: #e5e7e8;left: -22px;top: 20px;width: 4px;height: 100%;float:right;z-index: 9;\"></div>"
        document.getElementById(id).innerHTML += "<p style=\"font-family:楷体;font-size:15px\">" + route.routes[0].steps[i].instruction + "</p>";
    }
    document.getElementById("action").innerHTML += "<dt id=\"end\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";
    document.getElementById("end").innerHTML += "<div style=\"width: 21px;height: 21px;border: 1px solid #ddd;border-radius:50%;float:left;background-image: url(https://s3.bmp.ovh/imgs/2022/06/01/a241c633dbfaad0e.png);background-position: -126px -104px;\"></div>";
    document.getElementById("end").innerHTML += "<div style=\"background-color: #e5e7e8;left: -22px;top: 20px;width: 4px;height: 100%;float:right;z-index: 9;\"></div>"
    document.getElementById("end").innerHTML += "<b style=\"font-family:楷体;font-size:20px\">终点</b>";
}
function AddTransitroute(route) {
    document.getElementById("pos_choice").style.height = "320px";
    document.getElementById("mode").style.display = "block";
    var duration = (parseInt(route.plans[0].time) / 60).toFixed(2);
    var distance = (parseInt(route.plans[0].distance) / 1000).toFixed(2);
    document.getElementById("display_route").innerHTML = "<div id=\"display\" style=\"height:410px;margin: 0;padding: 0;color: #565656;line-height: 2;word-wrap: break-word;border: 2px solid silver;border-radius: 10px ;overflow-y: scroll \"></div>"
    document.getElementById("display").innerHTML += "<b style=\"font-family:楷体;font-size:20px\">预计用时" + duration.toString() + "分钟(" + distance.toString() + "公里)</b>";
    document.getElementById("display").innerHTML += "<div id=\"action\" ></div>";
    document.getElementById("action").innerHTML += "<dt id=\"" + id + "\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";

    document.getElementById("action").innerHTML += "<dt id=\"start\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";
    document.getElementById("start").innerHTML += "<div style=\"width: 21px;height: 21px;border: 1px solid #ddd;border-radius:50%;float:left;background-image: url(https://s3.bmp.ovh/imgs/2022/06/01/a241c633dbfaad0e.png);background-position: -47px -104px;\"></div>";
    document.getElementById("start").innerHTML += "<div style=\"background-color: #e5e7e8;left: -22px;top: 20px;width: 4px;height: 100%;float:right;z-index: 9;\"></div>"
    document.getElementById("start").innerHTML += "<b style=\"font-family:楷体;font-size:20px\">起点</b>";
    var i;
    for (i = 0; i < route.plans[0].segments.length; i++) {
        var id = "act" + i.toString();
        document.getElementById("action").innerHTML += "<dt id=\"" + id + "\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";
        document.getElementById(id).innerHTML += "<div style=\"width: 21px;height: 21px;border: 1px solid #ddd;border-radius:50%;float:left;background-image: url(https://s3.bmp.ovh/imgs/2022/06/01/a241c633dbfaad0e.png);background-position: " + Judgemode(route.plans[0].segments[i].transit_mode) + ";\"></div>";
        document.getElementById(id).innerHTML += "<div style=\"background-color: #e5e7e8;left: -22px;top: 20px;width: 4px;height: 100%;float:right;z-index: 9;\"></div>"
        document.getElementById(id).innerHTML += "<p style=\"font-family:楷体;font-size:15px\">" + route.plans[0].segments[i].instruction + "</p>";
    }
    document.getElementById("action").innerHTML += "<dt id=\"end\" style=\"position: relative;margin-left: 10px;line-height: 20px;border-bottom: 1px solid #f0f0f0;padding-bottom: 5px;padding-top: 5px;\"></dt>";
    document.getElementById("end").innerHTML += "<div style=\"width: 21px;height: 21px;border: 1px solid #ddd;border-radius:50%;float:left;background-image: url(https://s3.bmp.ovh/imgs/2022/06/01/a241c633dbfaad0e.png);background-position: -126px -104px;\"></div>";
    document.getElementById("end").innerHTML += "<div style=\"background-color: #e5e7e8;left: -22px;top: 20px;width: 4px;height: 100%;float:right;z-index: 9;\"></div>"
    document.getElementById("end").innerHTML += "<b style=\"font-family:楷体;font-size:20px\">终点</b>";
}
export default function Createroute() {
    var method = Changemethod();
    var ori = document.getElementById("ori").value
    var des = document.getElementById("des").value
    if (method == walk) {
        //根据起终点坐标规划步行路线
        walking.search([{ keyword: ori_city + ori }, { keyword: des_city + des }], function (status, result) {
            console.log(result);
            if (status === 'complete') {
                if (result.routes && result.routes.length) {
                    AddWalkingroute(result);
                    Drawroute(result.routes[0])
                    alert('绘制步行路线完成')
                    return;
                }
            } else {
                alert('步行路线数据查询失败' + result);
                return;
            }
        });
    }
    else if (method == drive) {
        // 根据起终点名称规划驾车导航路线
        re: driving.search([
            { keyword: ori_city + ori }, { keyword: des_city + des }
        ], function (status, result) {
            console.log(result);
            if (status === 'complete') {
                AddWalkingroute(result);
                Drawroute(result.routes[0])
                alert('绘制驾车路线完成')
                return;
            } else {
                alert('获取驾车数据失败：' + result)
                return;
            }
        });
    }
    else if (method == transit) {
        //根据起、终点名称查询公交换乘路线
        var mo = document.getElementById("mo").textContent;
        if (mo == "最少费用") {
            transfer.setPolicy(window.AMap.TransferPolicy.LEAST_FEE);
        }
        else if (mo == "最少换乘") {
            transfer.setPolicy(window.AMap.TransferPolicy.LEAST_TRANSFER);
        }
        else {//默认为最少时间
            transfer.setPolicy(window.AMap.TransferPolicy.LEAST_TIME);
        }
        transfer.search([
            { keyword: ori, city: ori_city }, { keyword: des, city: des_city }
        ], function (status, result) {
            if (status === 'complete') {
                console.log(result);

                AddTransitroute(result);
                drawTransitRoute(result.plans[0])
                alert('绘制公交路线完成')
                return;
            } else {
                alert('公交路线数据查询失败' + result);
                return;
            }
        });
    }
}
function Drawroute(route) {
    var map = new window.AMap.Map("container", {
        center: [116.397559, 39.89621],
        zoom: 14
    });
    var path = parseRouteToPath(route)

    var startMarker = new window.AMap.Marker({
        position: path[0],
        icon: 'https://webapi.amap.com/theme/v1.3/markers/n/start.png',
        map: map
    })

    var endMarker = new window.AMap.Marker({
        position: path[path.length - 1],
        icon: 'https://webapi.amap.com/theme/v1.3/markers/n/end.png',
        map: map
    })

    var routeLine = new window.AMap.Polyline({
        path: path,
        isOutline: true,
        outlineColor: '#ffeeee',
        borderWeight: 2,
        strokeWeight: 5,
        strokeColor: '#0091ff',
        lineJoin: 'round'
    })

    routeLine.setMap(map)

    // 调整视野达到最佳显示区域
    map.setFitView([startMarker, endMarker, routeLine])
}

// 解析WalkRoute对象，构造成AMap.Polyline的path参数需要的格式
function parseRouteToPath(route) {
    var path = []

    for (var i = 0, l = route.steps.length; i < l; i++) {
        var step = route.steps[i]

        for (var j = 0, n = step.path.length; j < n; j++) {
            path.push(step.path[j])
        }
    }

    return path
}

function drawTransitRoute(route) {
    console.log(route)
    var map = new window.AMap.Map("container", {
        center: [116.397559, 39.89621],
        zoom: 14
    });
    var startMarker = new window.AMap.Marker({
        position: route.segments[0].transit.origin,
        icon: 'https://webapi.amap.com/theme/v1.3/markers/n/start.png',
        map: map
    })

    var endMarker = new window.AMap.Marker({
        position: route.path[route.path.length - 1],
        icon: 'https://webapi.amap.com/theme/v1.3/markers/n/end.png',
        map: map
    })
    var routeLines = []

    for (var i = 0, l = route.segments.length; i < l; i++) {
        var segment = route.segments[i]
        var line = null

        // 绘制步行路线
        if (segment.transit_mode === 'WALK') {
            line = new window.AMap.Polyline({
                path: segment.transit.path,
                isOutline: true,
                outlineColor: '#ffeeee',
                borderWeight: 2,
                strokeWeight: 5,
                strokeColor: 'grey',
                lineJoin: 'round',
                strokeStyle: 'dashed'
            })


            line.setMap(map)
            routeLines.push(line)
        } else if (segment.transit_mode === 'SUBWAY' || segment.transit_mode === 'BUS') {
            line = new window.AMap.Polyline({
                path: segment.transit.path,
                isOutline: true,
                outlineColor: '#ffeeee',
                borderWeight: 2,
                strokeWeight: 5,
                strokeColor: '#0091ff',
                lineJoin: 'round',
                strokeStyle: 'solid'
            })

            line.setMap(map)
            routeLines.push(line)
        } else {
            // 其它transit_mode的情况如RAILWAY、TAXI等，该示例中不做处理
        }
    }

    // 调整视野达到最佳显示区域
    map.setFitView([startMarker, endMarker].concat(routeLines))
}