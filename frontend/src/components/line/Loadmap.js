import AutoRef from './AutoRef';
export var map = "notloading";
// 步行导航
export var walking;
// 驾车导航
export var driving;
// 乘车导航
export var transfer;

window.onload = function () {
    map = new window.AMap.Map("container");
    var walkingOption = {}
    window.AMap.plugin('AMap.Walking', function () {
        walking = new window.AMap.Walking(walkingOption)
    })
    var drivingOption = {}
    window.AMap.plugin('AMap.Driving', function () {
        driving = new window.AMap.Driving(drivingOption)
    })
    var transOptions = {}
    window.AMap.plugin('AMap.Transfer', function () {
        transfer = new window.AMap.Transfer(transOptions)
    })
    AutoRef(0);
    console.log(document.getElementById("title"));
    if (document.getElementById("title") !== null) {
        document.getElementById("title").style.display = "none";
    }
}

export default function Loadmap() {
    if (map == "notloading") {
        var url = 'https://webapi.amap.com/loader.js';
        var jsapi = document.createElement('script');
        jsapi.src = url;
        document.head.appendChild(jsapi);

        // url = 'https://webapi.amap.com/maps?v=1.4.10&key=3716a4a0cb9da00f18d2d976297b5345&plugin=AMap.PolyEditor';
        jsapi = document.createElement('script');
        jsapi.type = "text/javascript";
        jsapi.text = "window._AMapSecurityConfig = {securityJsCode: '26bddd0edf330148d34682786f73cd91',}"
        document.head.appendChild(jsapi);

        url = 'https://webapi.amap.com/maps?v=1.4.15&key=3716a4a0cb9da00f18d2d976297b5345&plugin=AMap.Autocomplete&callback=onload';
        jsapi = document.createElement('script');
        jsapi.src = url;
        document.head.appendChild(jsapi);

        // url = 'https://webapi.amap.com/maps?v=1.4.15&key=3716a4a0cb9da00f18d2d976297b5345&plugin=AMap.Walking&callback=onload';
        // jsapi = document.createElement('script');
        // jsapi.src = url;
        // document.head.appendChild(jsapi);
    }
}
