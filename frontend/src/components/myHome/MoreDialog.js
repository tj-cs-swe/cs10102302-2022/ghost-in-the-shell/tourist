import React, { useRef, useEffect } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { TextField } from '@mui/material';


export default function MoreDialog(props) {


    const handleClose = () => {
        props.setOpen(false);
    };


    return (
        <div>
            <Dialog open={props.open} onClose={handleClose}>
                <DialogTitle>我购买的 {props.Row.name} 详情</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="价格"
                        fullWidth
                        disabled
                        variant="standard"
                        defaultValue={props.Row.price}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="已购数量"
                        fullWidth
                        disabled
                        variant="standard"
                        defaultValue={props.Row.buynum}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="购买状态"
                        fullWidth
                        disabled
                        variant="standard"
                        defaultValue={props.Row.status}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="地址"
                        fullWidth
                        disabled
                        variant="standard"
                        defaultValue={props.Row.address}
                    />
                     <TextField
                        autoFocus
                        margin="dense"
                        label="介绍"
                        fullWidth
                        disabled
                        multiline
                        defaultValue={props.Row.intro==null?'暂无':props.Row.intro}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>关闭</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}