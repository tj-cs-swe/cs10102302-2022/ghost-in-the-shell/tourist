import H5 from '@material-tailwind/react/Heading5';
import TicketPaginationActionsTable from "./table";

export default function MyTicket(){
    return(
        <>
        <div className="my-8">
        <H5>我的票夹</H5>
        </div>
        
        <TicketPaginationActionsTable />
        </>
    )
}