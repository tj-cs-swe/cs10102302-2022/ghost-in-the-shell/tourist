import React, { useRef } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { TextField } from '@mui/material';


export default function ReturnDialog(props) {

  const handleClickOpen = () => {
    props.setOpen(true);
  };

  const handleClose = () => {
    props.setOpen(false);
  };

  const handleSubmit = () => {
   
    if (num.current?.value == '') {
      alert("价格不能为空");
      return;
    }
    if (num.current?.value < 0) {
      alert("价格不能小于0");
      return;
    }
    if(props.returnAproduct(props.returnID,num.current?.value)){
      props.setOpen(false);
    };
   
  }

  const num = useRef();

  return (
    <div>
      <Dialog open={props.open} onClose={handleClose}>
        <DialogTitle>我要退票</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="要退票的数量"
            fullWidth
            type='number'
            variant="standard"
            inputRef={num}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>取消</Button>
          <Button onClick={handleSubmit}>提交</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}