import * as React from 'react';
import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { TableHead } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import { Button } from '@mui/material';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';


import MoreDialog from './MoreDialog';
import ReturnDialog from './ReturnDialog';

import { Url } from '../../index';
import axios from 'axios';

function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

function createData(name, price, buynum, status, address, intro, id) {
    return { name, price, buynum, status, address, intro, id };
}

const columns = [
    { id: 'name', label: '名称', minWidth: 130, align: 'center' },
    { id: 'price', label: '价格', minWidth: 100, align: 'center' },
    { id: 'buynum', label: '数量', minWidth: 100, align: 'center' },
    { id: 'status', label: '当前状态', minWidth: 100, align: 'center' },
    { id: 'operation', label: '操作', minWidth: 170, align: 'center' },
];

const trows = [
    //createData('故宫门票', 100, 100, '北京市', 'dkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdh', '10023')
];

function statusShow(status) {
    if (status == "CHECKING")
        return "正在等待确认";
    else if (status == "NORMAL")
        return "购买完成";
    else if (status == "USED")
        return "已经使用";
    else if (status == "REFUND")
        return "已退款";
    else if (status == "CANCEL")
        return "取消";
}

export default function TicketPaginationActionsTable() {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [moreOpen, setMoreOpen] = React.useState(false);
    const [returnOpen, setReturnOpen] = React.useState(false);
    const [rows, setRows] = React.useState([]);
    const [returnID, setBuyID] = React.useState(trows);
    const [moreRow, setMoreRow] = React.useState({ name: '', price: 0, buynum: 0, address: '', intro: '', id: '' });

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleMore = (row) => {
        setMoreOpen(true);
        setMoreRow(row);
    };

    const handleReturn = (id) => {
        setBuyID(id);
        setReturnOpen(true);
    };

    const returnAproduct = (id, num) => {
        // console.log(id);
        // console.log(num);
        axios.post(Url + "/ticket" + "/refund",
            {
                "id": id,
                "num": num
            }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token'),
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {
                // console.log(response)
                if (response.data.msg == "request success") {
                    alert("退票成功。退款金额:" + response.data.data);
                    axios.get(Url + "/ticket" + "/findAll", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            // console.log(response.data)
                            renewData(response.data.data);
                        })
                        .catch(function (error) {
                            // console.log(error);
                            
                        });
                    return;
                } else {
                   
                    alert("退票失败");
                    return;
                }
            })
            .catch(function (error) {
                // console.log(error);
                if (error.response&&error.response.status === 402) {
                    alert("您的账号已被封禁，无法完成该操作");
                    return;
                }
                alert("退票失败");
            });
        return true;
    }

    const renewData = (Pducts) => {
        let rows = [];
        for (let i = 0; i < Pducts.length; i++) {
            rows.push(createData(Pducts[i].name, Pducts[i].price, Pducts[i].num, Pducts[i].status, Pducts[i].address, Pducts[i].intro, Pducts[i].id));
        }
        setRows(rows);
    }


    useEffect(async () => {
        await axios.get(Url + "/ticket" + "/findAll", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                renewData(response.data.data);
            })
            .catch(function (error) {
                // console.log(error);
            });
    }, []);

    return (
        <>

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 500 }} aria-label="user pagination table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : rows
                        ).map((row) => (
                            <TableRow key={row.id}>
                                <TableCell align="center">
                                    {row.name}
                                </TableCell>
                                <TableCell align="center">
                                    {row.price}
                                </TableCell>
                                <TableCell align="center">
                                    {row.buynum}
                                </TableCell>
                                <TableCell align="center">
                                    {statusShow(row.status)}
                                </TableCell>
                                <TableCell align="center">
                                    <Button onClick={() => { handleMore(row) }}>查看详情</Button>
                                    {row.status=="NORMAL"&&(<Button onClick={() => { handleReturn(row.id) }}>退票</Button>)}
                                </TableCell>
                            </TableRow>
                        ))}

                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={4} />
                            </TableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                colSpan={5}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: {
                                        'aria-label': 'rows per page',
                                    },
                                    native: true,
                                }}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage="每页数量"
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <MoreDialog setOpen={setMoreOpen} open={moreOpen}
                Row={moreRow} />
            <ReturnDialog returnAproduct={returnAproduct} returnID={returnID}
                setOpen={setReturnOpen} open={returnOpen} />
        </>
    );
}
