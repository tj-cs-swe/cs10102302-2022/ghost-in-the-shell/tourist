
import * as React from 'react';
import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { visuallyHidden } from '@mui/utils';
import Input from '@material-tailwind/react/Input';
import { Link } from 'react-router-dom';
import Button from '@material-tailwind/react/Button';

import { Url } from '../../index'
import axios from 'axios';

function createData(id, publisher, content, time) {
  return {
    id,
    publisher,
    content,
    time,
  };
}

let total_rows = [
  //createData('默认评论1', '热心市民', '2022/05/17'),
  //createData('默认评论2', '热心市民', '2022/05/22'),
  //createData('good', '汪汪队', '2022/06/01'),
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: ' content',
    numeric: false,
    disablePadding: true,
    label: '内容',
  },
  {
    id: 'publisher',
    numeric: true,
    disablePadding: false,
    label: '用户',
  },
  {
    id: 'time',
    numeric: true,
    disablePadding: false,
    label: '时间',
  },
];


function EnhancedTableHead(props) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
        props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            //padding={headCell.disablePadding ? 'none' : 'normal'}
            padding="2rem"
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}

      </TableRow>

    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const EnhancedTableToolbar = (props) => {
  const { numSelected, searchContent } = props;
  const [key, setKey] = React.useState('');

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
      }}
    >
        <Typography
          sx={{ flex: '1 1 100%' }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          评论
          {/*key*/}
        </Typography>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
  searchContent: PropTypes.func.isRequired,
};


export default function EnhancedTable(props) {
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('time');
  const [rows, setRows] = React.useState(total_rows);
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  //const [key, setKey] = React.useState('');

  
  const handleRenewData = (key) => {
    let rows = [];
    for (let i = 0; i < total_rows.length; i++) {
        //if (total_rows[i].title.includes(key) || total_rows[i].publisher.includes(key) || total_rows[i].time.includes(key)) {
            rows.push(createData(total_rows[i].id, total_rows[i].publisher, total_rows[i].content, total_rows[i].time));
        //}
    }
    setRows(rows);
  }

  useEffect(async () => {
    await axios.get(/*"https://mock.apifox.cn/m1/935986-0-default/passage/getContent" */  Url + "/passage/getContent"  , {
          timeout: 1000,
          params: {
            id : props.id
          },
          headers: {
              Authorization: window.sessionStorage.getItem('user_token')
          }
      })
          .then(function (response) {
              console.log(response.data.data)
              renewData(response.data.data);
          })
          .catch(function (error) {
              console.log(error);
          });
    }, []);

  const renewData = (Pducts) => {
    //let rows = [];
    total_rows = [];
    for (let i = 0; i < Pducts.commentList.length; i++) {
      total_rows.push(createData(Pducts.commentList[i].id, Pducts.commentList[i].userName, Pducts.commentList[i].cont, Pducts.commentList[i].createTime));
    }
    console.log(total_rows);
    handleRenewData("");
    //setRows(rows);
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.title);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, title) => {
    const selectedIndex = selected.indexOf(title);
    let newSelected = [];

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (title) => selected.indexOf(title) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <EnhancedTableToolbar numSelected={selected.length} searchContent={handleRenewData} />
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />


            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.content);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    
                        <TableRow
                        hover
                        onClick={(event) => handleClick(event, row.content)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.content}
                        selected={isItemSelected}
                        >

                        <TableCell
                            padding="2rem"
                            component="th"
                            id={labelId}
                            scope="row"
                            width="70%"
                            //padding="none"
                        >

                            {row.content}
                        </TableCell>
                        <TableCell align="right" width="10%">{row.publisher}</TableCell>
                        <TableCell align="right" width="10%">{row.time}</TableCell>


                        </TableRow>

                  );
                })}
              {emptyRows > 0 && (

                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>

              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          labelRowsPerPage="每页数量"
        />
      </Paper>
    </Box>
  );
}
