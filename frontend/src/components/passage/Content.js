import * as React from 'react';
import Title from '../forum/Title'
import View from './View'
import CommentTable from './CommentTable'

export default class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            forumList : []
        };
    }

    handlerReceiveForumList () {
        return (data)=>{
            this.setState({
                forumList: data
            })
        }
    }

    render () {
        return (
            <section className="relative py-16 bg-gray-100">

                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <Title/>

                            <br />

                            <View id = {this.props.id}/>
                            
                            <br />
        {(this.props.title != '系统公告') &&
                            <CommentTable id = {this.props.id}/>
        }
                            <br /><br />
                        </div>
                    </div>
                </div>
            </section >
        ); 
    }
}