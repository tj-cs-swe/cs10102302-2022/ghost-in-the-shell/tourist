import * as React from 'react';
import { useEffect, useRef } from 'react';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Link } from 'react-router-dom';
import Button from '@material-tailwind/react/Button';
import Modal from '@mui/material/Modal';
import { Input } from "@material-tailwind/react";
import { Textarea } from "@material-tailwind/react";

import { Url } from '../../index'
import axios from 'axios';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  hight: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};


const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

function createData(title, publisher, time, content, comment) {
  return {
    title,
    publisher,
    time,
    content,
    comment
  };
}


let total_rows = [
  /*
  createData('系统公告', '攻壳机动队', '2022/05/30'),
  createData('凑数帖12', '热心市民', '2022/05/17'),
  createData('凑数帖11', '汪汪打工仔队', '2022/05/12'),
  createData('凑数帖1', '汪汪打工仔队', '2022/05/27'),
  createData('凑数帖2', '热心市民', '2022/05/22'),
  createData('震惊！', '打工仔', '2022/05/23'),
  createData('凑数帖8', '热心市民', '2022/05/21'),
  createData('凑数帖9', '打工仔', '2022/05/27'),
  createData('凑数帖10', '热心市民', '2022/05/26'),
  createData('凑数帖3', '打工仔', '2022/05/20'),
  createData('凑数帖4', '打工仔', '2022/05/23'),
  createData('凑数帖5', '汪汪队', '2022/05/22'),
  createData('凑数帖6', '汪汪队', '2022/05/29'),
  createData('凑数帖7', '汪汪队', '2022/05/28'),
  createData('TEST', '汪汪队', '2022/06/01'),
  */
  createData("系统公告", "攻壳特工队", "2022/06/16", "Pducts.content", "Pducts.comment")
];


export default function View(props) {

  const [str, setStr] = React.useState("");
  const [rows, setRows] = React.useState(total_rows);
  const [open, setOpen] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [publisher, setPublisher] = React.useState("");
  const [time, setTime] = React.useState("");
  const [content, setContent] = React.useState("");
  const [comment, setComment] = React.useState(["评论1", "评论2"]);
  const [textareaValue, setTextareaValue] = React.useState("");

  const renewData = (Pducts) => {
    let rows = [];
    rows.push(createData(Pducts.passage.title, Pducts.passage.nickName, Pducts.passage.createTime, Pducts.content, Pducts.passage.commentList));
    //rows.push(createData("Pducts.title", "Pducts.nickname", "Pducts.create_time", "Pducts.content", "Pducts.comment"));
    setRows(rows);
    console.log(rows);
    setStr(Pducts.content.replace(/\n/g,"<br/>"));
  }
  
  useEffect(async () => {
    console.log(props.id);
    await axios.get(/*"https://mock.apifox.cn/m1/935986-0-default/passage/getContent" */  Url + "/passage/getContent"  , {
          timeout: 1000,
          params: {
            id : props.id
          },
          headers: {
              Authorization: window.sessionStorage.getItem('user_token'),
              'Content-Type': 'application/json'
          }
      })
          .then(function (response) {
              console.log(response.data.data)
              renewData(response.data.data);
              /*
              this.setState({
                title : response.data.data.title,
                publisher : response.data.data.nickname,
                time : response.data.data.create_time,
                content : response.data.data.content
              })
              */
          })
          .catch(function (error) {
              console.log(error);
          });
    }, []);

  const handleOpen = () => {
    setOpen(true);
  }

  const handleClose = () => {
    setOpen(false);
  }

  const handleTextareaChange = (e) => {
    setTextareaValue(e.target.value)
  }


  const delPassage = () => {
     axios.delete(/*"https://mock.apifox.cn/m1/935986-0-default/passage/getContent" */  Url + "/passage/delete"  , {
          timeout: 1000,
          params: {
            id : props.id
          },
          headers: {
              Authorization: window.sessionStorage.getItem('user_token'),
              'Content-Type': 'application/json'
          }
      })
          .then(function (response) {
              console.log(response.data.data)
              //renewData(response.data.data);
          })
          .catch(function (error) {
            if (error.response&&error.response.status === 402) {
              alert("您的账号已被封禁，无法进行操作");
              return;
            }
              console.log(error);
          });
  }

  const addContent = (comment) => {
    handleClose();
    console.log(props.id);
    console.log(comment);
    let time = new Date().toLocaleDateString()
    console.log(time);
    axios.post(/*"https://mock.apifox.cn/m1/935986-0-default/passage/addComment"*/  Url + "/passage/addComment"  ,
        {
            "articleId": props.id,
            "content": comment,
            //"time": time,
        }, {
        timeout: 1000,
        headers: {
            Authorization: window.sessionStorage.getItem('user_token'),
            'Content-Type': 'application/json'
        }
    })
        .then(function (response) {
            console.log(response)
            if (response.data.data === "Add Success") {
                alert("成功");
                console.log("success");
            } else {
                alert("失败");
                return;
            }
        })
        .catch(function (error) {
            console.log(error);
            if (error.response&&error.response.status === 402) {
              alert("您的账号已被封禁，无法进行操作");
              return;
            }
            alert("失败");
        });
    return true;
  }


  const changeAnnouncement = (comment) => {
    handleClose();
    console.log(props.id);
    console.log(comment);
    let time = new Date().toLocaleDateString()
    console.log(time);
    total_rows = [
      createData("系统公告", "攻壳特工队", "2022/06/16", {comment}, "Pducts.comment")
    ];
    return true;
  }
  
  return (
    <Box sx={{ width: '100%' }} searchContent={renewData}>

    <div class="left-0">

      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Item>
            <Typography variant="h4" gutterBottom component="div">
              {/*攻壳机动队*/}
              {rows[0].title} {/*/ {props.id}*/}
            </Typography>
          </Item>
        </Grid>
      </Grid>
      <br></br>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Item>
            {/*发布者:汪汪队*/}
            {rows[0].publisher}
          </Item>
        </Grid>
        <Grid item xs={4} >
          <Item>
            {/*发布日期:2022/05/25*/}
            {rows[0].time}
          </Item>
        </Grid>

        {((window.sessionStorage.getItem('user_type') == "ADMIN" && rows[0].title != '系统公告') || rows[0].publisher == window.sessionStorage.getItem('nickname')) &&

/*
        <Grid item xs={1} >
          <Item> 
<Link sx={{ width: '100%', maxWidth: 500 }}
to={{
    pathname: '/edit',
    state: {  // 页面跳转要传递的数据，如下
        title: rows[0].title,
        content: rows[0].content
    },
}}
>
          删除
</Link>
          </Item>
        </Grid>
*/

        <Grid item xs={1} >
          <Item
            onClick={() => delPassage()}  
          > 
            <Link sx={{ width: '100%', maxWidth: 500 }}
             to={{
              pathname: '/forum',
              state: {  // 页面跳转要传递的数据，如下
                  data1: {},
                  data2: []
              },
            }}
            >
              删除
            </Link>
          </Item>
        </Grid>
}

{(window.sessionStorage.getItem('user_type') == "ADMIN" && rows[0].title == '系统公告') &&
        <Grid item xs={1} >
          <Item onClick={handleOpen}>
            编辑
          </Item>
          <Modal
            open = {open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style} >
              <div className="items-end gap-1 w-full">

                <Textarea 
                variant="outlined" label="Outlined" placeholder="内容" 
                value={textareaValue} onChange={handleTextareaChange}
                />
              </div>

          <div className="hidden sm:block" aria-hidden="true">
            <div className="py-5">
              <div className="border-t border-gray-200" />
            </div>
          </div>


              <Button
                variant="outlined"
                color="lightBlue"
                size="md"
                ripple="dark"
                onClick={() => changeAnnouncement(textareaValue)}     
              >
                  发布
              </Button> 

            </Box>
          </Modal>
        </Grid>
}

{(window.sessionStorage.getItem('nickname') != null && rows[0].title != '系统公告') &&
        <Grid item xs={1} >
          <Item onClick={handleOpen}>
            评论
          </Item>
          <Modal
            open = {open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style} >
              <div className="items-end gap-1 w-full">
              {/*
                <Input variant="outlined" label="Outlined" placeholder="标题" />
                <br></br>
              */}

                <Textarea 
                variant="outlined" label="Outlined" placeholder="内容" 
                value={textareaValue} onChange={handleTextareaChange}
                />
              </div>

          <div className="hidden sm:block" aria-hidden="true">
            <div className="py-5">
              <div className="border-t border-gray-200" />
            </div>
          </div>


              <Button
                variant="outlined"
                color="lightBlue"
                size="md"
                ripple="dark"
                onClick={() => addContent(textareaValue)}
                /*
                onClick={() => {
                    this.state.adcode = this.searchContent(keyword)                                                                                                                  
                    if(this.state.adcode == 'error'){                                           
                        this.setState({
                            tag:"查询失败"
                        })                                           
                    }
                    else{
                        this.state.tag="查询成功"
                    }                                  
                }}
                    */       
              >
                  发布
              </Button> 

            </Box>
          </Modal>
        </Grid>
}
      

        <Grid item xs={2} >
          <Item> 
<Link sx={{ width: '50%', maxWidth: 500 }}
to={{
    pathname: '/forum',
    state: {  // 页面跳转要传递的数据，如下
        data1: {},
        data2: []
    },
}}
>

            返回论坛主页
            
</Link>
          </Item>
        </Grid>
           
        <Grid item xs={12}>
          <Item>
            
          <Typography variant="body1" gutterBottom class="left-0 text-left overflow-auto px-18 text-lg h-96">
            { /* rows[0].content */ }
            <div class='h-96 w-full overflow-auto' dangerouslySetInnerHTML={{ __html: str }}/>
          </Typography>

          </Item>
        </Grid>

      </Grid>
      </div>

      
    </Box>
  );
}
