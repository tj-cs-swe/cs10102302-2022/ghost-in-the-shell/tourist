import Button from '@material-tailwind/react/Button';
import Image from '@material-tailwind/react/Image';
import H3 from '@material-tailwind/react/Heading3';
import Icon from '@material-tailwind/react/Icon';
import ProfilePicture from 'assets/img/scenery2.jpg';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';


import { useEffect, useRef } from 'react';
import { Url } from '../../index'
import axios from 'axios';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};
const ListItem = styled('li')(({ theme }) => ({
    margin: theme.spacing(0.5),
}));
const init_tags = [
    '爱跑步',
    '爱健身',
    '爱人文',
    '爱风景',
    '爱美食',
    '爱睡觉'
];
const profile = {
    nickname: "",
    articles: "",
    comments: "",
    motto: "",
    email: "",
    createTime: ""
};
const my_tags = [
    { label: '爱风景' },
    { label: '爱人文' },
    { label: '健身走' },
    { label: '静态网格' },
    { label: '华山论剑上一届冠军' }
];
function createData( _articles, _comments, _motto, _email, _createTime) {
    return {  articles:_articles, comments:_comments, motto:_motto, email:_email, createTime:_createTime };
}

export default function Message() {
    const [_pro, setProfile] = React.useState(profile);
    const [myTag, setMyTag] = React.useState([]);
    const [tagName, setTagName] = React.useState([]);
    const [tags, setAllTag] = React.useState([]);
    
    const mottoRef = useRef();
    const emailRef = useRef();

    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
         setTagName([]);
        //将value中存储的标签添加到用户的标签池
        let exist = 0;
        for (let i = 0; i < myTag.length; i++) {
            if (myTag[i].label == value) {
                exist = 1;
            }
        }
        if (exist == 0) {
            myTag.push({ label: value })
        }
    };
    const handleDelete = (chipToDelete) => () => {
        setMyTag((chips) => chips.filter((chip) => chip.label !== chipToDelete.label));
    };

    const renewMyTag = (data) => {
        let rows = [];
        for (let i = 0; i < data.length; i++) {
            rows.push(data[i]);
        }
        setMyTag(rows);
    }
    const renewAllTag = (data) => {
        let rows = [];
        for (let i = 0; i < data.length; i++) {
            rows.push(data[i]);
        }
        setAllTag(rows);
    }
    const renewProfile = (data) => {
        let newPro;
        console.log(data);
        newPro = createData(data.articles, data.comments, data.motto, data.email, data.createTime);
        console.log(newPro);
        setProfile(newPro);
        console.log(_pro);
    }
    const saveProfile = () => {
        axios.put(Url + "/user/saveMessage",
            {
                nickname: window.sessionStorage.getItem('nickname'),
                motto: mottoRef.current?.value,
                email: emailRef.current?.value
            }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    alert("信息修改成功");
                    // axios.get(Url + "/profile" + "/getMessage", {
                    //     timeout: 1000,
                    //     headers: {
                    //         Authorization: window.sessionStorage.getItem('user_token')
                    //     }
                    // })
                    //     .then(function (response) {
                    //         console.log(response.data)
                    //         renewProfile(response.data.data);
                    //         alert("信息修改成功");
                    //         return;
                    //     })
                    //     .catch(function (error) {
                    //         console.log(error);
                    //     });

                } else {
                    
                    alert("信息修改失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                if (error.response&&error.response.status === 402) {
                    alert("您的账号已被封禁，无法完成该操作");
                    return;
                }
                alert("信息修改失败");
            });
    }
    const saveLabel = () => {
        axios.put(Url + "/profile/saveLabel",
            myTag, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data)
                if (response.data.data === 0) {
                    axios.get(Url + "/profile" + "/getLabel", {
                        timeout: 1000,
                        headers: {
                            Authorization: window.sessionStorage.getItem('user_token')
                        }
                    })
                        .then(function (response) {
                            console.log(response.data)
                            renewMyTag(response.data.data);
                            alert("标签保存成功");
                            return;
                        })
                        .catch(function (error) {
                            if (error.response&&error.response.status === 402) {
                                alert("您的账号已被封禁，无法完成该操作");
                                return;
                            }
                            console.log(error);
                        });

                } else {
                    alert("标签保存失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("标签保存失败");
            });
    }
    useEffect(async () => {
        await axios.get(Url + "/user" + "/getMessage", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewProfile(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });

        await axios.get(Url + "/profile" + "/getLabel", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewMyTag(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });

        await axios.get(Url + "/profile" + "/getAllUserLabel", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewAllTag(response.data.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);


    return (
        <section>
            <div className="flex flex-wrap justify-center">
                <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                    <div className="relative">
                        <div className="w-40 -mt-20">
                            <Image
                                src={ProfilePicture}
                                alt="Profile picture"
                                raised
                                rounded
                            />
                        </div>
                    </div>
                </div>
                <div className="w-full lg:w-4/12 px-4 lg:order-3 lg:self-center flex justify-center mt-10 lg:justify-end lg:mt-0">
                    <Button color="lightBlue" ripple="light" onClick={saveProfile}>
                        保存基本信息
                    </Button>
                </div>
                <div className="w-full lg:w-4/12 px-4 lg:order-1">
                    <div className="flex justify-center py-4 lg:pt-4 pt-8">
                        {/* <div className="mr-4 p-3 text-center">
                            <span className="text-xl font-bold block uppercase tracking-wide text-gray-900">
                                {_pro.articles}
                            </span>
                            <span className="text-sm text-gray-700">
                                发帖数
                            </span>
                        </div>
                        <div className="lg:mr-4 p-3 text-center">
                            <span className="text-xl font-bold block uppercase tracking-wide text-gray-900">
                                {_pro.comments}
                            </span>
                            <span className="text-sm text-gray-700">
                                评论数
                            </span>
                        </div> */}
                    </div>
                </div>
            </div>

            <div className="text-center my-8">
                <H3 color="gray">{window.sessionStorage.getItem('nickname')}</H3>
                <div className="mt-0 mb-2 text-gray-700 font-medium flex items-center justify-center gap-2">
                    <Icon name="place" size="xl" />
                    {_pro.motto}
                </div>
            </div>

            
                <div className="flex flex-wrap">
                    <div style={{ width: "35%", marginLeft: "20%", float: "left" }}>
                        <div style={{ marginTop: "20px" }}>
                            <TextField
                                fullWidth
                                focused
                                label="邮箱"
                                inputRef={emailRef}
                                value={_pro.email}
                                onChange={(e) => { 
                                    setProfile(createData( _pro.articles, _pro.comments, _pro.motto, e.target.value, _pro.createTime));
                                }}
                            />
                        </div>
                        <div style={{ marginTop: "20px" }}>
                            <TextField
                                fullWidth
                                focused
                                label="座右铭"
                                inputRef={mottoRef}
                                value={_pro.motto}
                                onChange={(e) => { 
                                    setProfile(createData( _pro.articles, _pro.comments, e.target.value, _pro.email, _pro.createTime));
                                }}
                            />
                        </div>
                        <div style={{ marginTop: "20px", marginBottom: "30px" }}>
                            <TextField
                                fullWidth
                                focused
                                label={_pro.createTime}
                                disabled
                            />
                        </div>

                    </div>
                    <div style={{ width: "30%", float: "right", marginLeft: "5%" }}>
                        <div style={{ marginTop: "50px", height: "100px" }}>
                            <FormControl sx={{ m: 1, width: 150 }} style={{ float: "left" }}>
                                <InputLabel id="tag_select_label">选择标签</InputLabel>
                                <Select
                                    labelId="tag_select_label"
                                    id="tag_select"
                                    value={tagName}
                                    onChange={handleChange}
                                    MenuProps={MenuProps}
                                >
                                    {tags.map((name) => (
                                        <MenuItem
                                            key={name}
                                            value={name}
                                        >
                                            {name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <div style={{ float: "right", marginRight: "0px", marginTop: "20px" }}>
                                <Button color="lightBlue" ripple="light" style={{ width: "140px" }} onClick={saveLabel}>
                                    保存当前标签
                                </Button>
                            </div>
                        </div>
                        <div style={{ marginTop: "20px" }}>
                            <Paper
                                elevation={10}
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    flexWrap: 'wrap',
                                    listStyle: 'none',
                                    p: 0.5,
                                    m: 0,
                                }}
                                component="ul"
                            >
                                {myTag.map((data) => {
                                    return (
                                        <ListItem>
                                            <Chip
                                                label={data.label}
                                                onDelete={handleDelete(data)}
                                                color="primary"
                                            />
                                        </ListItem>
                                    );
                                })}
                            </Paper>
                        </div>
                    </div>
                </div>
        </section>
    );
}
