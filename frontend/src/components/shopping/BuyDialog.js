import React, { useRef } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { TextField } from '@mui/material';


export default function BuyDialog(props) {

  const handleClickOpen = () => {
    props.setOpen(true);
  };

  const handleClose = () => {
    props.setOpen(false);
  };

  const handleSubmit = () => {
   
    if (num.current?.value == '') {
      alert("购买数量不能为空");
      return;
    }
    if (num.current?.value < 0) {
      alert("购买数量不能小于0");
      return;
    }
    if(props.buyAproduct(props.buyID,num.current?.value)){
      props.setOpen(false);
    };
   
  }

  const num = useRef();

  return (
    <div>
      <Dialog open={props.open} onClose={handleClose}>
        <DialogTitle>购买 {props.name}</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="数量"
            fullWidth
            variant="standard"
            inputRef={num}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>取消</Button>
          <Button onClick={handleSubmit}>提交</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}