import H3 from '@material-tailwind/react/Heading3';
import OutLink from './OutLink';
import ShoppingPaginationActionsTable from './table.js'


export default function Content() {
    return (
        <section className="relative py-16 bg-gray-100">
            <div className="container max-w-7xl px-4 mx-auto ">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                    <div className="px-6">
                        <div className="text-center my-8">
                            <H3 color="gray">票务服务</H3>
                        </div>

                        <div className="mb-10 py-2 border-t border-gray-200 text-center">
                           <ShoppingPaginationActionsTable/>
                        </div>

                        <div className="mb-10 py-2 border-t border-gray-200 text-center" />

                        <OutLink/>
                        
                    </div>
                </div>
            </div>
        </section>
    );
}
