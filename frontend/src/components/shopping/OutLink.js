import H4 from '@material-tailwind/react/Heading4';
import WebCard from './WebCard';
import qunaImage from '../../assets/img/logo/logo-qunar.png';
import tcImage from '../../assets/img/logo/logo-tc.jpeg'
import ctripImage from '../../assets/img/logo/logo-ctrip.jpg'
import eLongImage from '../../assets/img/logo/logo-eLong.png'
import fliggyImage from '../../assets/img/logo/logo-fliggy.jpg'
import tppImage from '../../assets/img/logo/logo-tpp.png'

export default function OutLink() {
    return (
        <section className="mb-10">
            <div className="container max-w-7xl mx-auto px-4">
            <H4>外部网站</H4>
                <div className="flex flex-wrap mt-5">
                    <WebCard
                        name="去哪儿"
                        link="https://www.qunar.com/"
                        img ={qunaImage}
                    />
                    <WebCard
                        name="同程"
                        link="https://www.ly.com/"
                        img ={tcImage}
                    />
                    <WebCard
                        name="携程"
                        link="https://www.ctrip.com/"
                        img ={ctripImage}
                    />
                    <WebCard
                        name="艺龙"
                        link="https://www.elong.com/"
                        img={eLongImage}
                    />
                    <WebCard
                        name="飞猪旅行"
                        link="https://www.fliggy.com/"
                        img={fliggyImage}
                    />
                    <WebCard
                        name="淘票票"
                        link="https://dianying.taobao.com/"
                        img={tppImage}
                    />
                </div>
            </div>
        </section>
    );
}
