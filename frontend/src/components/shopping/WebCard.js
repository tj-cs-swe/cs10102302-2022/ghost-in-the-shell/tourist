import H6 from '@material-tailwind/react/Heading6';
import Image from '@material-tailwind/react/Image';

export default function WebCard({ img, name, link }) {
    return (
        <div className="w-full md:w-6/12 lg:w-2/12 lg:mb-0 mb-12 px-4">
            <div className="px-6">
                <Image src={img} />
                <div className="pt-6 text-center">
                    <a href={link} target="_blank">
                        <H6 color="gray">{name}</H6>
                    </a>
                </div>
            </div>
        </div>
    );
}
