import * as React from 'react';
import { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { TableHead } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import { Button } from '@mui/material';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import TextField from '@mui/material/TextField';
import Search from '@mui/icons-material/Search'


import MoreDialog from './MoreDialog';
import BuyDialog from './BuyDialog';

import { Url } from '../../index'
import axios from 'axios';

function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

function createData(name, price, remain, address, intro, id) {
    return { name, price, remain, address, intro, id };
}

const columns = [
    { id: 'name', label: '名称', minWidth: 170, align: 'center' },
    { id: 'price', label: '价格', minWidth: 100, align: 'center' },
    { id: 'remain', label: '余量', minWidth: 100, align: 'center' },
    { id: 'operation', label: '操作', minWidth: 170, align: 'center' },
];

const trows = [
    createData('故宫门票', 100, 100, '北京市', 'dkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdhdkhgfkshdhfkuasdgfiuwagfiasdh', '10023')
];

export default function ShoppingPaginationActionsTable() {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [moreOpen, setMoreOpen] = React.useState(false);
    const [buyOpen, setBuyOpen] = React.useState(false);
    const [rows, setRows] = React.useState([]);
    const [buyID, setBuyID] = React.useState([]);
    const [change, setChange] = React.useState(false);
    const [moreRow, setMoreRow] = React.useState({ name: '', address: '', price: 0, remains: 0, intro: '', id: '' });

    const searchRef = useRef();

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleMore = (row) => {
        setMoreOpen(true);
        setMoreRow(row);
    };

    const handleSearch = () => {
        axios.get(Url + "/shopping/search", {
            params: {
                name:searchRef.current?.value,
            }
        }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token'),
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {
                console.log(response)
                if (response.data.msg === "request success") {
                    renewData(response.data.data)
                } else {
                    alert("搜索失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("搜索失败");
            });
    }

    const handleBuy = (id) => {
        setBuyID(id);
        setBuyOpen(true);
    };

    const buyAproduct = (id, num) => {
        console.log(id);
        console.log(num);
        axios.post(Url + "/ticket" + "/buy",
            {
                "id": id,
                "num": num
            }, {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token'),
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {
                console.log(response)
                if (response.data.data === "request success") {
                    alert("购买成功");
                } else {
                    alert("添加失败");
                    return;
                }
            })
            .catch(function (error) {
                console.log(error);
                if (error.response&&error.response.status === 402) {
                    alert("您的账号已被封禁，无法完成该操作");
                    return;
                }
                alert("添加失败");
            });
        return true;
    }

    const renewData = (Pducts) => {
        let rows = [];
        for (let i = 0; i < Pducts.length; i++) {
            rows.push(createData(Pducts[i].name, Pducts[i].price, Pducts[i].remain, Pducts[i].address, Pducts[i].intro, Pducts[i].id));
        }
        setRows(rows);
    }


    useEffect(async () => {
        await axios.get(Url + "/shopping" + "/getproducts", {
            timeout: 1000,
            headers: {
                Authorization: window.sessionStorage.getItem('user_token')
            }
        })
            .then(function (response) {
                console.log(response.data.data)
                renewData(response.data.data);
            })
            .catch(function (error) {
                if (error.response&&error.response.status === 402) {
                alert("您的账号已被封禁，无法查看票务信息");
                return;
            }
                console.log(error);
            });
    }, []);

    return (
        <>
            <Box sx={{ display: 'flex', alignItems: 'flex-end', ml: 5 }}>
                <Search sx={{ color: 'action.active', mr: 1, my: 0.5, mt: 2 }} />
                <TextField inputRef={searchRef} label="票务搜索" variant="standard" />
                <Button onClick={handleSearch} color="secondary" variant="contained">搜索</Button>
            </Box>

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : rows
                        ).map((row) => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell align="center">
                                    {row.price}
                                </TableCell>
                                <TableCell align="center">
                                    {row.remain}
                                </TableCell>
                                <TableCell align="center">
                                    {(window.sessionStorage.getItem('user_type')=='USER') &&(<Button onClick={() => { handleBuy(row.id) }}>购买</Button>)}
                                    <Button onClick={() => { handleMore(row) }}>查看详情</Button>
                                </TableCell>
                            </TableRow>
                        ))}

                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={4} />
                            </TableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                colSpan={4}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: {
                                        'aria-label': 'rows per page',
                                    },
                                    native: true,
                                }}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage="每页数量"
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <MoreDialog setOpen={setMoreOpen} open={moreOpen}
                Row={moreRow} />
            <BuyDialog buyAproduct={buyAproduct} buyID={buyID}
                setOpen={setBuyOpen} open={buyOpen} />
        </>
    );
}
