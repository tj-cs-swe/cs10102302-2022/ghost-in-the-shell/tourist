
import React from "react";

// reactstrap components

import Image from "@material-tailwind/react/Image";
//import 'bootstrap/dist/css/bootstrap.css';
import carouselCss from '../../assets/styles/carousel.css';
// reactstrap components
import {
    Container,
    Row,
    Col,
    Carousel,
    CarouselItem,
    CarouselIndicators,
} from "reactstrap";

// core components

const items = [
    {
        src: require("assets/img/bg1.jpg").default,
        altText: "img1",
        caption: "img1",
    },
    {
        src: require("assets/img/bg3.jpg").default,
        altText: "img2",
        caption: "img2",
    },
    {
        src: require("assets/img/bg4.jpg").default,
        altText: "img3",
        caption: "img3",
    },
    {
        src: require("assets/img/bg5.jpg").default,
        altText: "img4",
        caption: "img4",
    },
    {
        src: require("assets/img/bg6.jpg").default,
        altText: "img5",
        caption: "img5",
    },
];

export default function CarouselSection() {
    const [activeIndex, setActiveIndex] = React.useState(0);
    const [animating, setAnimating] = React.useState(false);
    const onExiting = () => {
      setAnimating(true);
    };
    const onExited = () => {
      setAnimating(false);
    };
    const next = () => {
      if (animating) return;
      const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
      setActiveIndex(nextIndex);
    };
    const previous = () => {
      if (animating) return;
      const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
      setActiveIndex(nextIndex);
    };
    const goToIndex = (newIndex) => {
      if (animating) return;
      setActiveIndex(newIndex);
    };
    return (
      <>
        <div className={"section"} id="carousel" >
          <Container>
            <Row className="justify-content-center">
              <Col lg="12" md="12">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={items}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {items.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.src}
                      >
                        <div class="object-left w-full aspect-[1920/1080] ...">
                            <Image 
                                src={item.src}
                                rounded={false}
                                raised={false}
                                alt={item.altText}
                            />
                        </div>
                        <div className="carousel-caption d-none d-md-block">
                          <h5>{item.caption}</h5>
                        </div>
                      </CarouselItem>
                    );
                  })}

                  <a
                    className="carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <i className="now-ui-icons arrows-1_minimal-left"></i>
                  </a>
                  <a
                    className="carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <i className="now-ui-icons arrows-1_minimal-right"></i>
                  </a>
                </Carousel>
              </Col>
            </Row>
          </Container>
          
        </div>
      </>
    );
}


/*

import React from "react";

// reactstrap components
import { Carousel, CarouselItem, CarouselIndicators } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.css';
import Image from "@material-tailwind/react/Image";

// core components
const items = [
    {
        src: require("assets/img/background-1920x1080.jpg").default,
        altText: "photo1",
        caption: "·—·",
    },
    {
        src: require("assets/img/pexels-stan-swinnen-6465964.jpg").default,
        altText: "photo2",
        caption: "·—·",
    },
    {
        src: require("assets/img/team-3-800x800.jpg").default,
        altText: "photo3",
        caption: "·—·",
    },
    {
        src: require("assets/img/teamwork.jpeg").default,
        altText: "photo4",
        caption: "·—·",
    },
];

export default function CarouselSection(){
    const [activeIndex, setActiveIndex] = React.useState(0);
    const [animating, setAnimating] = React.useState(false);
    const onExiting = () => {
        setAnimating(true);
    };
    const onExited = () => {
        setAnimating(false);
    };
    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    };
    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    };
    const goToIndex = newIndex => {
        if (animating) return;
        setActiveIndex(newIndex);
    };

    return (
        <>
        <Carousel activeIndex={activeIndex} next={next} previous={previous}>
            <div class = "space-x-100 text-white object-bottom">
                <CarouselIndicators
                    items={items}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                />
            </div>

            {items.map(item => {
                return (
                    <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.src}
                    >

                        <div class="object-center bg-white aspect-[1920/1080] ...">
                            <Image
                                src={item.src}
                                rounded={false}
                                raised={false}
                                alt={item.altText}
                            />
                        </div>

                        <div className="carousel-caption d-none d-md-block">
                            <h5>{item.altText}</h5>
                        </div>

                    </CarouselItem>
                );
            })}

            <div>
                <a
                    className="carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={e => {
                        e.preventDefault();
                        previous();
                    }}
                    role="button"
                >
                    <i className="now-ui-icons arrows-1_minimal-left"></i>
                </a>

                <a
                    className="carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={e => {
                        e.preventDefault();
                        next();
                    }}
                    role="button"
                >
                    <i className="now-ui-icons arrows-1_minimal-right"></i>
                </a>
            </div>
            
        </Carousel>
        </>
    );
}

*/