import * as React from 'react';
import Title from './Title'
import CarouselSection from './Carousel'
import SearchBox from './Search'
import InformationBox from './Information'

function createData(name, province, city, content) {
    return {
        name,
        province,
        city,
        content,
    };
}

export default class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spotInformation : {
                name: '', 
                province: '',
                city: '',
                content: '',
                key: '48e395538e06327f01c9365e8eba6bac'
            }
        };
    }

    handlerReceiveSpotInformation () {
        return (data)=>{
            console.log(data);
            this.setState({
                spotInformation: createData(data.name, data.province, data.city, data.content)
            })
        }
    }

    render () {
        return (
            <section className="relative py-16 bg-gray-100">

                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">

                            <Title/>

                            <br />
                            
                            < div id="carousel_section" style={{ width: '68%', height: '800px', float: 'left', marginBottom: "75px" }} >

                                {/*<CarouselSection/>*/}
                                
                                <InformationBox data = {this.state.spotInformation}/>
                        
                            </div>
                            
                            <div id="pos_choice" style={{ width: '28%', height: '800px', float: 'right' }}>
                                
                                <div className="mb-10 px-4" >

                                    <SearchBox onReceiveSpotInformation = {this.handlerReceiveSpotInformation()} />

                                </div>
                                
                            </div>

                            <br /><br /><br />
                        </div>
                    </div>
                </div>
            </section >
        ); 
    }
}