import React from "react";
import {useState} from 'react';
import Tab from "@material-tailwind/react/Tab";
import TabList from "@material-tailwind/react/TabList";
import TabItem from "@material-tailwind/react/TabItem";
import TabContent from "@material-tailwind/react/TabContent";
import TabPane from "@material-tailwind/react/TabPane";
import Icon from "@material-tailwind/react/Icon";
import Card from "@material-tailwind/react/Card";
import CardImage from "@material-tailwind/react/CardImage";
import CardBody from "@material-tailwind/react/CardBody";
import CardFooter from "@material-tailwind/react/CardFooter";
import H6 from "@material-tailwind/react/Heading6";
import Paragraph from "@material-tailwind/react/Paragraph";
import Button from "@material-tailwind/react/Button";



export default class Information extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        openTab: 1,
      };
      
    }
    //const [openTab, setOpenTab] = useState(1);

    setOpenTab (opt) {
      this.setState({openTab: opt});
    }

    render () {
      return (
        <div>
        <Card>
          <CardBody>
              <H6 color="gray">景点名称</H6>
              <Paragraph color="gray" class='h-8'>
                {this.props.data.name}
              </Paragraph>
              <H6 color="gray">所在城市</H6>
              <Paragraph color="gray" class='h-8'>
                {this.props.data.city}
              </Paragraph>
              <H6 color="gray">景点介绍</H6>
              <Paragraph color="gray">
                <div class='h-96 w-full overflow-auto' dangerouslySetInnerHTML={{ __html: this.props.data.content }}/>
              </Paragraph>
              {/* 
              <H6 color="gray">景点评价</H6>
              <Paragraph color="gray">
                <div class='h-28 w-full overflow-auto'/>
              </Paragraph>
              */}
          </CardBody>
        </Card>
        </div>
      );
    }
}

/*
            <Tab>
              <TabList color="lightBlue">
                  <TabItem
                      onClick={(e) => {
                          e.preventDefault();
                          this.setOpenTab(1);
                      }}
                      ripple="light"
                      active={this.state.openTab === 1 ? true : false}
                      href="tabItem"
                  >
                      <Icon name="language" size="lg" />
                      景点名称
                  </TabItem>
                  <TabItem
                      onClick={(e) => {
                          e.preventDefault();
                          this.setOpenTab(2);
                      }}
                      ripple="light"
                      active={this.state.openTab === 2 ? true : false}
                      href="tabItem"
                  >
                      <Icon name="account_circle" size="lg" />
                      景点介绍
                  </TabItem>
                  <TabItem
                      onClick={(e) => {
                          e.preventDefault();
                          this.setOpenTab(3);
                      }}
                      ripple="light"
                      active={this.state.openTab === 3 ? true : false}
                      href="tabItem"
                  >
                      <Icon name="settings" size="lg" />
                      相关评价
                  </TabItem>
              </TabList>

              <TabContent>
                  <TabPane active={this.state.openTab === 1 ? true : false}>
                      <p>
                        name : 
                        <br></br>
                        {this.props.data.name}
                        <br></br>

                        <br></br>

                        city : 
                        <br></br>
                        {this.props.data.city}
                        <br></br>
                      </p>
                  </TabPane>
                  <TabPane active={this.state.openTab === 2 ? true : false}>
                      <p className="text-xs">
                        {this.props.data.content}
                      </p>
                  </TabPane>
                  <TabPane active={this.state.openTab === 3 ? true : false}>
                      <p>
                          I think that’s a responsibility that I have, to push
                          possibilities, to show people, this is the level that things
                          could be at. So when you get something that has the name Kanye
                          West on it, it’s supposed to be pushing the furthest
                          possibilities. I will be the leader of a company that ends up
                          being worth billions of dollars, because I got the answers. I
                          understand culture. I am the nucleus.
                      </p>
                  </TabPane>
              </TabContent>
          </Tab>
*/