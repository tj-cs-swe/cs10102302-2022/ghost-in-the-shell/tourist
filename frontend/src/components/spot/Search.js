import React from "react";
import Input from '@material-tailwind/react/Input';
import Button from '@material-tailwind/react/Button';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';


let spots = [];

function createData(id, name, province, city, content) {
    return {
        id,
        name,
        province,
        city,
        content,
    };
}

export default class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '', 
            province: '',
            city: '',
            content: '',
            key: '48e395538e06327f01c9365e8eba6bac'
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSpotSelect = this.handleSpotSelect.bind(this);
    }

    handleChange(event) {
        this.setState({name: event.target.value});
    }

    searchSpot() {
        
        var url="http://api.tianapi.com/scenic/index?key="+this.state.key+"&word="+this.state.name+"&num=10";
        
        fetch(url)
        .then(res=>res.json())
        .then(data=>{
            console.log(data);
            if(data.newslist) {
                //data.newslist["0"].content.replace('<br>','/n');
                //console.log(data);
                //var str = data.newslist["0"].content;
                //str = str.replace("<br>","\n");
                //str = str.replace(/<br>/g,"\n");
                //console.log(str);
                spots = [];
                for (let i = 0; i < data.newslist.length; i++) {
                    data.newslist[i].name.replace(/\?/g, ' ');
                    let tmp;
                    tmp = createData(i, data.newslist[i].name, data.newslist[i].province, data.newslist[i].city, data.newslist[i].content);
                    /*
                    for (let j = 0; j < tmp.name.length; j++) {
                        if(tmp.name[j] == '?') {
                            tmp.name[j] = ' ';
                        }
                    }
                    */
                    spots.push(tmp);
                    //spots.push(createData(i, "name", "content", "province", "city"));
                }
                /*
                this.setState({
                    name:data.newslist["0"].name,
                    content:data.newslist["0"].content,
                    province:data.newslist["0"].province,
                    city:data.newslist["0"].city
                })
                */
            }
            else {
                this.setState({
                    name:"暂无相关信息",
                    content:"",
                    province:"",
                    city:""
                })
            }
        })

        var spotInformation = {
            name:this.state.name,
            content:this.state.content,
            province:this.state.province,
            city:this.state.city
        };

        this.props.onReceiveSpotInformation(spotInformation);
    }


    handleSpotSelect = (event, name) => {
        //const { options } = event.target;
        for (let i = 0; i < spots.length; i++) {
            if(spots[i].name == name) {
                console.log("success " + spots[i].name + " " + name + " " + i)
                this.setState({
                    name:spots[i].name,
                    content:spots[i].content,
                    province:spots[i].province,
                    city:spots[i].city
                })
                this.props.onReceiveSpotInformation(spots[i]);
                break;
            }
        }
        /*
        this.setState({
            name:options.value.name,
            content:options.value.content,
            province:options.value.province,
            city:options.value.city
        })
        */
    };

    render () {
        var spot = this.state.name;
        return (
            <>

                <div style={{ width: "100%", height: "500px", float: "right", overflowX: "hidden" }}>                                
                    <p></p>
                    <div className="mt-10 mb-10 px-4" style={{ height: "100px" }}>
                        <div style={{ float: "left", width: "100%" }}>
                            <Input 
                                type="text" 
                                color="lightBlue" 
                                placeholder="输入景点名称" 
                                id="wea_city" 
                                value={spot} 
                                onChange={this.handleChange}
                            />
                            <br></br>
                            <FormControl style={{ width: "100%", height: "200px"}}>
                                <InputLabel shrink htmlFor="select-multiple-native">
                                    景点
                                </InputLabel>
                                <Select
                                    multiple
                                    native
                                    value={spot}
                                    // @ts-ignore Typings are not considering `native`
                                    label="spot"
                                    onChange={this.handleChangeMultiple}
                                >
                                    {spots.map((spot) => (
                                        <option 
                                            key={spot.name} 
                                            value={spot.name}
                                            onClick={(event) => this.handleSpotSelect(event, spot.name)}
                                        >
                                            {spot.name}
                                        </option>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>
                    </div>                                
                    <div style={{ marginLeft: "35%", marginTop: "100px" }}>

                    <Button
                        variant="outlined"
                        color="lightBlue"
                        size="lg"
                        ripple="dark"
                            onClick={() => {
                                this.state.adcode = this.searchSpot()                                                                                                                  
                                if(this.state.adcode == 'error'){                                           
                                    this.setState({
                                        tag:"查询失败"
                                    })                                           
                                }
                                else{
                                    this.state.tag="查询成功"
                                }                                         
                            }}
                    >
                        查询景点
                    </Button>                                                             
                    </div>
                    {/*<p className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">{this.state.tag}</p> */}
                </div>
             </>
        );
    }
}


{/*
                <div className="mt-10 sm:mt-0">
                    <div className="md:grid md:grid-cols-1 md:gap-6">

                    <div className="mt-5 md:mt-0 md:col-span-1">
                        <form action="#" method="POST">
                        <div className="shadow overflow-hidden sm:rounded-md">
                            <div className="px-4 py-5 bg-white sm:p-6">
                            <div className="grid grid-cols-1 gap-6 max-w-md">
                                <div className="col-span-6 sm:col-span-6">
                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                    Spot
                                </label>
                                <input
                                    value={spot} onChange={this.handleChange}
                                    type="text"
                                    name="first-name"
                                    id="first-name"
                                    autoComplete="given-name"
                                    className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                />
                                </div>

                                <div className="col-span-6 sm:col-span-3">
                                <label htmlFor="country" className="block text-sm font-medium text-gray-700">
                                    Country
                                </label>
                                <select
                                    id="country"
                                    name="country"
                                    autoComplete="country-name"
                                    className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                >
                                    <option>China</option>
                                    <option>Canada</option>
                                    <option>Mexico</option>
                                </select>
                                </div>

                                <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                                <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                                    City
                                </label>
                                <input
                                    type="text"
                                    name="city"
                                    id="city"
                                    autoComplete="address-level2"
                                    className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                />
                                </div>

                            </div>
                            </div>

                            <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button
                                type="submit"
                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                onClick={() => {
                                    //this.searchSpot();    
                                                                                                                                        
                                    if(this.state.adcode == 'error'){                                           
                                        this.setState({
                                            tag:"查询失败"
                                        })
                                    }
                                    else{
                                        this.componentDidMount();
                                        this.state.tag="查询成功"
                                    }           
                                                               
                                }}
                            >
                                Save
                            </button>
                            </div>
                        </div>
                        </form>
                    </div>
                    </div>
                </div>
*/}
