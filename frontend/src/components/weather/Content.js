import * as React from 'react';
import H3 from '@material-tailwind/react/Heading3';
import Input from '@material-tailwind/react/Input';
import Button from '@material-tailwind/react/Button';
import Searchweather from './Searchweather';
import Background from '../../assets/weaimg/1.jpg'
import {
    HomeWrapper,
    Header,
    Temperature,
    Extra,
    Echartcontaier,
    MoreInfo,
    MoreWrapper,
    MoreDay,
    Loading,
    SpanWrapper
  } from './style'
import WeatherCircle from './cloudy'


var BackStyle={
    width:"100%",
    height:"410px",
    //alignItems:'center',
    backgroundImage:`url(${Background})`
};

export default class Content extends React.Component{
    state={
        province: '',
        city:'',
        weather:'',
        temperature:'',
        winddirection:'',
        windpower:'',
        humidity:'',
        reporttime:'',
        adcode:'310114',
        tag:''
    }
    componentDidMount() {     
        var url="https://restapi.amap.com/v3/weather/weatherInfo?city="+this.state.adcode+"&key=f5c0e0d7cdd994dcc67778aa03872d19"
        
        fetch(url)
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            this.setState({
                province:data.lives["0"].province,
                city:data.lives["0"].city,
                weather:data.lives["0"].weather,
                temperature:data.lives["0"].temperature,
                winddirection:data.lives["0"].winddirection,
                windpower:data.lives["0"].windpower,
                humidity:data.lives["0"].humidity,
                reporttime:data.lives["0"].reporttime
            })
        })
    }

    render() {
        return (
            <>         
                <section className="relative py-16 bg-gray-100"> 
                <div className="container max-w-7xl px-4 mx-auto">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-2xl -mt-64">
                        <div className="px-6">
                            <div className="text-center my-8">
                                <H3 color="gray">天气查询</H3>
                            </div>
                            
                            <div style={{ width: "32%", height: "450px", float: "right", overflowX: "hidden" }}>                                
                                <p></p>
                                <div className="mt-10 mb-10 px-4" style={{ height: "20px" }}>
                                    <div style={{ float: "left", width: "35%" }}>
                                        <Input type="text" color="lightBlue" placeholder="输入城市名称" id="wea_city" />
                                    </div>
                                </div>                                
                                <div style={{ marginLeft: "35%", marginTop: "100px" }}>
                                <Button
                                    variant="outlined"
                                    color="lightBlue"
                                    size="lg"
                                    ripple="dark"
                                     onClick={() => {
                                         this.state.adcode=Searchweather()                                                                                                                  
                                         if(this.state.adcode=='error'){                                           
                                            this.setState({
                                                tag:"查询失败"
                                            })                                           
                                         }
                                         else{
                                            this.componentDidMount();
                                            this.state.tag="查询成功"
                                         }                                         
                                     }}
                                >
                                    查询天气
                                </Button>                                                             
                                </div>
                                <p className="text-gray-700 hover:text-gray-900 block pb-2 text-sm">{this.state.tag}</p> 
                            </div>
                            <div id="show_wea" style={{ width: '68%', height: '500px', float: 'left', marginBottom: "75px",margin:0 }}>                               
                                <div style={BackStyle}>
                                    <Header><span>{this.state.province}|{this.state.city}</span></Header>
                                    <Temperature>
                                        <h2>{this.state.temperature}℃</h2>
                                        <span>{this.state.weather}</span><span>|</span>
                                        <span>更新时间：{this.state.reporttime}</span>
                                        <Extra>
                                            <dl>
                                                <dt>风力:{this.state.windpower} | 风向:{this.state.winddirection} | 湿度:{this.state.humidity}%</dt>
                                            </dl>
                                        </Extra>
                                    </Temperature>
                                    <WeatherCircle />
                                    <MoreInfo>
                                        <MoreWrapper>                                           
                                                <MoreDay>
                                                    <p>明日</p>
                                                    <div>
                                                        <p>12℃-25℃</p>
                                                    </div>
                                                    <p>晴</p>
                                                </MoreDay>
                                                <MoreDay>
                                                    <p>后日</p>
                                                    <div>
                                                        <p>10℃-27℃</p>
                                                    </div>
                                                    <p>雨</p>
                                                </MoreDay>
                                                <MoreDay>
                                                    <p>大后天</p>
                                                    <div>
                                                        <p>20℃-33℃</p>
                                                    </div>
                                                    <p>晴</p>
                                                </MoreDay>                                           
                                        </MoreWrapper>
                                    </MoreInfo>
                                </div>
                            </div>                           
                        </div>
                    </div>  
                </div>
                </section> 
                                   
            </>
        )
    }
}