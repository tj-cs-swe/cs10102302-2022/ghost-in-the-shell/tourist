import React from 'react'
import { Cloudy, Sunny } from './cloudy_style'

class WeatherCircle extends React.Component {
  render() {
    return (
      <div>
        {/* <Cloudy>
          <span className="cloud"></span>
        </Cloudy> */}
        <Sunny>
          <span className="sun"></span>
        </Sunny>
      </div>
        
        
    )
  }
}

export default WeatherCircle