import React from 'react'
import DefaultNavbar from 'components/DefaultNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/forum/Header';
import Content from 'components/forum/Content';

export default class Forum extends React.Component {

    render () {
        return (
            <>
                <div className="absolute w-full z-20">
                    <DefaultNavbar />
                </div>
                <main>
                    <Header />
                    <Content />
                </main>
                <DefaultFooter />
            </>
        );
    }
}