import React, { Component } from 'react'
import DefaultNavbar from 'components/DefaultNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/calendar/Header';
import Content from 'components/line/Content';

export default function Line() {
    return (
        <>
            <div className="absolute w-full z-20">
                <DefaultNavbar />
            </div>
            <main>
                <Header/>
                <Content/>
                
            </main>
            <DefaultFooter />
        </>
    )

}

