import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import CardFooter from '@material-tailwind/react/CardFooter';
import H5 from '@material-tailwind/react/Heading5';
import InputIcon from '@material-tailwind/react/InputIcon';
import Button from '@material-tailwind/react/Button';
import { Link } from 'react-router-dom';
import LoginNavbar from 'components/login/LoginNavbar';
import SimpleFooter from 'components/SimpleFooter';
import Page from 'components/login/Page';
import Container from 'components/login/Container';
import { Redirect } from 'react-router';

import React, { useState } from 'react';

import axios from 'axios';
import { Url } from 'index';

export var nickname = "登录";

export default function Login() {

    const [id, setID] = useState('');
    const [pwd, setPwd] = useState('');
    const [jumptoHome, setJumpToHome] = useState(false);
    const [jumptoBus, setJumpToBus] = useState(false);
    const [jumptoAdmin, setJumpToAdmin] = useState(false);

    const handleSubmit = () => {
        if(id==''||pwd==''){
            alert('输入不能为空');
        return;}
        axios.post(Url + "/signin", {
            name: id,
            password: pwd
        }, {
            timeout: 10000
        })
            .then(function (response) {
                 console.log(response.data);
                //  if (response.data.code === 402) {
                //     alert("您的账号已经被冻结");
                //     return;
                // }
                // console.log(response.data.data.roles);

                window.sessionStorage.setItem('user_token', response.headers.authorization)
                window.sessionStorage.setItem('nickname', response.data.data.name)
                window.sessionStorage.setItem('user_type', response.data.data.roles)
                if (response.data.data.roles == 'USER')
                    setJumpToHome(true);
                else if (response.data.data.roles == 'FIRM')
                    setJumpToBus(true);
                else if (response.data.data.roles == 'ADMIN')
                    setJumpToAdmin(true);
                alert("登录成功，准备跳转");
                return;

            })
            .catch(function (error) {
                // alert("登录失败");
                console.log(error);

                if (error.response&&error.response.status === 401) {
                    alert("账号密码不符，请重新输入");
                } else if (error.response&&error.response.status === 402) {
                    alert("您的账号已经被冻结");
                } else {
                    alert("登录失败");
                }
            });
    };

    const changeID = (e) => {
        setID(e.target.value);
    }

    const changePwd = (e) => {
        setPwd(e.target.value);
    }


    if (jumptoHome === true) {
        return <Redirect from="*" to="/" />
    }
    if (jumptoBus === true) {
        return <Redirect from="*" to="/busCredit" />
    }
    if (jumptoAdmin === true) {
        return <Redirect from="*" to="/adminMain" />
    }

    return (
        <Page>
            <LoginNavbar />
            <Container>
                <Card>
                    <CardHeader color="lightBlue">
                        <H5 color="white" style={{ marginBottom: 0 }}>
                            登 录
                        </H5>
                    </CardHeader>

                    <CardBody>
                        <div className="mb-12 px-4 bg-bb">
                            <InputIcon
                                type="rext"
                                color="lightBlue"
                                placeholder="昵称"
                                iconName="person"
                                value={id}
                                onChange={changeID}
                            />
                        </div>
                        <div className="mb-8 px-4">
                            <InputIcon
                                type="password"
                                color="lightBlue"
                                placeholder="密码"
                                iconName="lock"
                                value={pwd}
                                onChange={changePwd}
                            />
                        </div>
                        <Link to="/register">
                            <p class="text-sm text-center underline decoration-solid">没有账号？现在注册</p>
                        </Link>
                    </CardBody>
                    <CardFooter>
                        <div className="flex justify-center bg-bb">
                            <Button
                                color="lightBlue"
                                buttonType="link"
                                size="lg"
                                ripple="dark"
                                onClick={handleSubmit}
                            >
                                登 录
                            </Button>
                        </div>
                    </CardFooter>
                </Card>
            </Container>
            <SimpleFooter />
        </Page>
    );
}
