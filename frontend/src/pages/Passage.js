import React from 'react'
import DefaultNavbar from 'components/DefaultNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/forum/Header';
import Content from 'components/passage/Content';

export default class Passage extends React.Component {

    render () {
        return (
            <>
                <div className="absolute w-full z-20">
                    <DefaultNavbar />
                </div>
                <main>
                    <Header />
                    <Content id = {this.props.location.state.id} title = {this.props.location.state.title}/>
                </main>
                <DefaultFooter />
            </>
        );
    }
}