import DefaultNavbar from 'components/DefaultNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/profile/Header';
import Content from 'components/profile/Content';
import NullContent from 'components/NullContent';

export default function Profile() {
    if(window.sessionStorage.getItem('user_type')!='USER'){
        return (
            <>
                <div className="absolute w-full z-20">
                    <DefaultNavbar />
                </div>
                <main>
                    <Header />
                    <NullContent msg="请先登录 只有注册旅行者才能查看此页哦"/>
                </main>
                <DefaultFooter />
            </>
        );
    }
    return (
        <>
            <div className="absolute w-full z-20">
                <DefaultNavbar />
            </div>
            <main>
                <Header />
                <Content />
            </main>
            <DefaultFooter />
        </>
    );
}
