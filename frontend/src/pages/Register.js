import Card from '@material-tailwind/react/Card';
import CardHeader from '@material-tailwind/react/CardHeader';
import CardBody from '@material-tailwind/react/CardBody';
import CardFooter from '@material-tailwind/react/CardFooter';
import H5 from '@material-tailwind/react/Heading5';
import InputIcon from '@material-tailwind/react/InputIcon';
import Button from '@material-tailwind/react/Button';
import RegisterNavbar from 'components/login/RegisterNavbar';
import SimpleFooter from 'components/SimpleFooter';
import Page from 'components/login/Page';
import Container from 'components/login/Container';

import { FormControl } from '@mui/material';
import { FormControlLabel } from '@mui/material';
import { RadioGroup } from '@mui/material';
import { Radio } from '@mui/material';

import React, { useState } from 'react';
import { Redirect } from 'react-router';

import { Url } from 'index';
import axios from 'axios';

export default function Register() {

    const [id, setID] = useState('');
    const [pwd, setPwd] = useState('');
    const [jumpto, setJumpTo] = useState(false);
    const [type, setType] = useState('USER');

    const handleSubmit = () => {
        if(id==''||pwd==''){
            alert('输入不能为空');
            return;
        }
        
        axios.post(Url + "/signup",
            { name: id, password: pwd, role: type })
            .then(function (response) {
                alert("注册成功，准备跳转至登录界面");
                console.log(response);
                setJumpTo(true);
            })
            .catch(function (error) {
                if(error.request.status==402){
                    alert("该昵称已被占用，换个名字试试吧");
                }
                else{
                    alert("注册失败");
                }
                console.log(error);
                
            });
    };

    if (jumpto === true) {
        return <Redirect from="*" to="/login" />
    }

    const changeID = (e) => {
        setID(e.target.value);
    }

    const changePwd = (e) => {
        setPwd(e.target.value);
    }

    const changeType = (e) => {
        setType(e.target.value);
    }

    return (
        <Page>
            <RegisterNavbar />
            <Container>
                <Card>
                    <CardHeader color="lightBlue">
                        <H5 color="white" style={{ marginBottom: 0 }}>
                            注 册
                        </H5>
                    </CardHeader>

                    <CardBody>
                        <div className="mb-10 px-4">
                            <InputIcon
                                type="text"
                                color="lightBlue"
                                placeholder="昵称"
                                iconName="account_circle"
                                value={id}
                                onChange={changeID}
                            />
                        </div>
                        <div className="mb-4 px-4">
                            <InputIcon
                                type="password"
                                color="lightBlue"
                                placeholder="密码"
                                iconName="lock"
                                value={pwd}
                                onChange={changePwd}
                            />
                        </div>
                        <div className="ml-5 ">
                            <FormControl>
                                <RadioGroup
                                    row
                                    aria-labelledby="type-group-label"
                                    name="type-group"
                                    value={type}
                                    onChange={changeType}
                                >
                                    <FormControlLabel value="USER" control={<Radio />} label="旅行者" />
                                    <FormControlLabel value="FIRM" control={<Radio />} label="商家" />
                                </RadioGroup>
                            </FormControl>
                        </div>
                    </CardBody>
                    <CardFooter>
                        <div className="flex justify-center">
                            <Button
                                color="lightBlue"
                                buttonType="link"
                                size="lg"
                                ripple="dark"
                                onClick={handleSubmit}
                            >
                                注 册
                            </Button>
                        </div>
                    </CardFooter>
                </Card>
            </Container>
            <SimpleFooter />
        </Page>
    );
}
