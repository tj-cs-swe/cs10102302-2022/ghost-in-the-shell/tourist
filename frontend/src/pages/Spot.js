import React from 'react'
import DefaultNavbar from 'components/DefaultNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/spot/Header';
import Content from 'components/spot/Content';

export default class Spot extends React.Component {
    
    spotInformation = {
        name: '',
        content:'',
        province:'',
        city:'',
        key:'48e395538e06327f01c9365e8eba6bac'
    }

    

    render () {
        return (
            <>
                <div className="absolute w-full z-20">
                    <DefaultNavbar />
                </div>
                <main>
                    <Header />
                    <Content />
                </main>
                <DefaultFooter />
            </>
        );
    }
}