import React from 'react';
import DefaultNavbar from 'components/DefaultNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/calendar/Header';
import Content from 'components/weather/Content';

export default class Weather extends React.Component {
    // constructor(){
    //     super();
    //     this.state={
    //         banners:'hhhhh'
    //     }
    // }  
    state={
        province: '',
        city:'',
        weather:'',
        temperature:'',
        winddirection:'',
        windpower:'',
        humidity:'',
        adcode:'330100'
    }

    componentDidMount() {
        //var adcode='110101'
        var url="https://restapi.amap.com/v3/weather/weatherInfo?city="+this.state.adcode+"&key=f5c0e0d7cdd994dcc67778aa03872d19"
        
        fetch(url)
        .then(res=>res.json())
        .then(data=>{
            // console.log(data.lives["0"].province)
            // console.log(data.lives["0"].city)
            // console.log(data.lives["0"].weather)
            // console.log(data.lives["0"].temperature)
            // console.log(data.lives["0"].winddirection)
            // console.log(data.lives["0"].windpower)
            // console.log(data.lives["0"].humidity)
            this.setState({
                province:data.lives["0"].province,
                city:data.lives["0"].city,
                weather:data.lives["0"].weather,
                temperature:data.lives["0"].temperature,
                winddirection:data.lives["0"].winddirection,
                windpower:data.lives["0"].windpower,
                humidity:data.lives["0"].humidity
            })
        })
    }

    render() {
        return (
            <>     
                <div className="absolute w-full z-20">
                    <DefaultNavbar />
                </div>
                <main>
                    <Header/>   
                    <Content />
                </main>
                <DefaultFooter />             
            </>
        )
    }

}


