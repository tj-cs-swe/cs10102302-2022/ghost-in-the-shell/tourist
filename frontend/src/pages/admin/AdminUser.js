import Footer from 'components/admin/Footer';
import Header from 'components/admin/Header';
import AdminNavbar from 'components/Navbars/AdminNavbar';
import Content from 'components/admin/user/Content';
import NullContent from 'components/NullContent';

export default function AdminUser() {
    if (window.sessionStorage.getItem('user_type') != 'ADMIN') {
        return (
            <>
                <div className="absolute w-full z-20">
                    <AdminNavbar />
                </div>
                <main>
                    <Header />
                    <NullContent msg="请先登录 只有管理员才能查看此页哦" />
                </main>
                <Footer />
            </>
        );
    }
    return (
        <>
            <div className="absolute w-full z-20">
                <AdminNavbar />
            </div>

            <main>
                <Header />
                <Content />
            </main>
            <Footer />
        </>
    );
}