import BusinessNavbar from 'components/Navbars/BusinessNavbar';
import DefaultFooter from 'components/DefaultFooter';
import Header from 'components/business/profile/Header';
import Content from 'components/business/profile/Content';
import DefaultNavbar from 'components/DefaultNavbar';
import NullContent from 'components/NullContent';

export default function BusProfile() {
    if(window.sessionStorage.getItem('user_type')!='FIRM'){
        return (
            <>
                <div className="absolute w-full z-20">
                    <DefaultNavbar />
                </div>
                <main>
                    <Header />
                    <NullContent msg="请先登录 只有注册商家才能查看此页哦"/>
                </main>
                <DefaultFooter />
            </>
        );
    }
    return (
        <>
            <div className="absolute w-full z-20">
                <BusinessNavbar />
            </div>
            <main>
                <Header />
                <Content />
            </main>
            <DefaultFooter />
        </>
    );
}
